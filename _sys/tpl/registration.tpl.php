<!DOCTYPE html>
<html class="page-index" lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <title><?= $F_PAGE_GEN['title'] ?></title>
        <meta name="description" content="<?= $F_PAGE_GEN['description'] ?>" />
        <meta name="keywords" content="<?= $F_PAGE_GEN['keywords'] ?>" />
        <meta name="robots" content="<?= $F_PAGE_GEN['robots'] ?>" />
        <meta name="f_t" id="f_token" content="<?= $_SESSION["token"] ?>" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        <script type="text/javascript" src="/js/ajax.js"></script>
        <script type="text/javascript" src="/js/registration.js?v=1553704826368"></script>
        <link rel="stylesheet" href="css/main.css?v=1553704826368">
        <link rel="stylesheet" href="/js/sweetmodal/jquery.sweet-modal.min.css" />
    </head>
    <body>
        <div class="b-page">
            <? include_once(F_PATH_SYS."tpl/inc/new_header.tpl.php") ?>
            <main class="b-body b-page__body">
                <div id="js-fullpage">
                    <div class="b-login-new-bg b-screen" data-anchor="main">
                        <div class="b-center">
                            <h1 class="b-tit">Регистрация</h1>
                            <form id="registration" method="post">
                                <div class="b-login__form">
                                    <div class="b-form-line">
                                            <label class="b-label">e-mail *</label>
                                            <div class="b-fild">
                                                <input class="b-input" id="email" value="<?= $email ?>" name="email" type="email" placeholder="mymail@example.com">
                                            </div>
                                    </div>
                                    <div class="b-form-line b-form-line--row">
                                        <div class="b-form-line__col">
                                            <label class="b-label">Пароль *</label>
                                            <div class="b-fild">
                                                <input class="b-input" type="password" name="pass">
                                            </div>
                                        </div>
                                        <div class="b-form-line__col">
                                            <label class="b-label">Повторите пароль *</label>
                                            <div class="b-fild">
                                                <input class="b-input" type="password" name="pass2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="b-form-line b-form-line--center">
                                        <label class="b-ld-input">
                                            <input class="js-input" type="checkbox" name="premissons">Я даю согласие на обработку песональных данных
                                        </label>
                                    </div>
                                    <div class="b-form-btn">
                                        <button class="b-btn" id="register">Регистрация
                                            <div class="arrow">
                                                <svg>
                                                <use xlink:href="images/sprites.svg#arrow"></use>
                                                </svg>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <script src="js/vendor.js?v=1553704826376"></script>
        <script src="js/main.js?v=1553704826376"></script>
    </body>
</html>