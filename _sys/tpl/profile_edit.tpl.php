<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php')); ?>
<!DOCTYPE html>
<html class="page-index" lang="ru">
    <head>
        <? include_once(F_PATH_SYS."tpl/inc/html_head_tags.tpl.php") ?> 
        <link rel="stylesheet" href="/css/interface.css?v=1557472175815">
        <script type="text/javascript" src="/js/common.js"></script>
        <script type="text/javascript" src="/js/_admin/mask/jquery.maskedinput.min.js"></script>
    </head>
    <body>
        <div class="a-page">
            <nav class="a-main-nav a-page__menu">
                <div class="a-main-nav__tit">Confpulse</div>
                <div class="a-main-nav__menu">
                    <? require_once(F_PATH_SYS."tpl/inc/v_nav_menu.tpl.php") ?>
                    <ul class="a-main-nav__list">
                        <li class="a-main-nav__list-item"><a href="/#examples">как это работает</a></li>
                        <li class="a-main-nav__list-item"><a href="/#about">о проекте </a></li>
                        <li class="a-main-nav__list-item"><a href="/#questions">контакты </a></li>
                    </ul>
                </div>
                <ul class="a-share-nav">
                    <li class="a-share-nav__item"><a class="icon-facebook" href="#"></a></li>
                    <li class="a-share-nav__item"><a class="icon-youtubel" href="#"></a></li>
                    <li class="a-share-nav__item"><a class="icon-instagram" href="#"></a></li>
                </ul>
            </nav>
            <? include_once(F_PATH_SYS."tpl/inc/header_interface.tpl.php") ?>
            <main class="a-body a-page__main">
                <div class="a-center">
                    <div class="a-crumbs"><a href="/groups">Inspiratura<span class="icon"><span></span></span></a><span>Профиль</span></div>

                    <? include_once(F_PATH_SYS.'tpl/inc/profile_alert.tpl.php'); ?>

                    <form id="profile_edit" method="post">
                        <div class="a-form">
                            <h2 class="a-tit">Редактирование профиля</h2>
                            <div class="a-form-row">
                                <div class="a-form-row__lb">
                                    <label class="a-form-lb">Имя *</label>
                                </div>
                                <div class="a-form-row__fild">
                                    <input id="name" value="<?= $fio ?>" type="text" name="name" class="a-input" />
                                </div>
                                <div class="a-form-row__valid a-form-row__valid-name">

                                </div>
                            </div>
                            <div class="a-form-row">
                                <div class="a-form-row__lb">
                                    <label class="a-form-lb">Телефон * </label>
                                </div>
                                <div class="a-form-row__fild">
                                    <input class="a-input js-mask-phone" id="phone" value="<?= substr($phone, 3) ?>" type="text" name="phone">
                                </div>
                                <div class="a-form-row__valid a-form-row__valid-phone">

                                </div>
                            </div>
                            <div class="a-form-row">
                                <div class="a-form-row__lb">
                                    <label class="a-form-lb">E-mail *  </label>
                                </div>
                                <div class="a-form-row__fild">
                                    <input class="a-input" id="email" value="<?= $email ?>" type="email" name="email">
                                </div>
                                <div class="a-form-row__valid a-form-row__valid-email">

                                </div>
                            </div>
                            <div class="a-form-row">
                                <div class="a-form-row__lb">
                                    <label class="a-form-lb">Мессенджеры</label>
                                </div>
                                <div class="a-form-row__fild">
                                    <? if (!empty($t_admin)): ?>
                                        <button class="a-btn unauth">Отвязать Telegram</button>
                                    <? else: ?>
                                        <script async src="https://telegram.org/js/telegram-widget.js?5" data-telegram-login="inspiratura_bot" data-userpic="false" data-size="medium" data-radius="1" data-onauth="onTelegramAuth(user)" data-request-access="write"></script>
                                    <? endif; ?>
                                </div>
                            </div>
                            <div class="a-form-row a-form-row--bot">
                                <div class="a-form-row__btn">
                                    <button class="a-btn a-btn--green" id="save_profile">Сохранить</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form id="profile_edit_pass" method="post">
                        <div class="a-form">
                            <h2 class="a-tit">Изменить пароль</h2>
                            <div class="a-form-row">
                                <div class="a-form-row__lb">
                                    <label class="a-form-lb">Старый пароль *</label>
                                </div>
                                <div class="a-form-row__fild">
                                    <input class="a-input" type="password" name="oldpass">
                                </div>

                            </div>
                            <div class="a-form-row">
                                <div class="a-form-row__lb">
                                    <label class="a-form-lb">Новый пароль *</label>
                                </div>
                                <div class="a-form-row__fild">
                                    <input class="a-input" type="password" name="pass">
                                </div>

                            </div>
                            <div class="a-form-row">
                                <div class="a-form-row__lb">
                                    <label class="a-form-lb">Подтвердите пароль * </label>
                                </div>
                                <div class="a-form-row__fild">
                                    <input class="a-input" type="password" name="pass2"/>
                                </div>

                            </div>
                            <div class="a-form-row a-form-row--bot">
                                <div class="a-form-row__btn">
                                    <button id="save_profile_pass" class="a-btn a-btn--green">Сохранить</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </main>
        </div>
        <script src="js/vendor.js?v=1557472175815"></script>
        <script src="js/main.js?v=1557472175815"></script>
    </body>
</html>