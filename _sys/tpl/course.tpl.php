<!DOCTYPE html>
<html class="page-index" lang="ru">
<head>
    <? include_once(F_PATH_SYS."tpl/inc/html_head_tags.tpl.php") ?>
    <link rel="stylesheet" href="/css/interface.css?v=1557472175815">
    <link rel="stylesheet" href="/css/bootstrap/bootstrap.min.css">
    <script type="text/javascript" src="/css//bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/course_learn.js"></script>
    <style>
        table {
            width: 100% !important;
        }

        h2 {
            display: inline-block;
            word-break: break-word;
        }

        iframe {
            border: 0;
            border-radius: 6px;
            width: 100% !important;
        }

        label {
            font-weight: 400;
        }

        right_side_menu {
            position: absolute;
            top: 92px;
            right: 0;
            width: 30%;
            z-index: 9999;
            transition: width 0.5s linear;
        }

        .close-menu right_side_menu{
            width: 35%;
        }

        right_side_menu .a-control, .a-entry{
            width: inherit;
        }

        right_side_menu .a-entry{
            position: fixed;
            border-left: 1px rgb(232, 233, 235) solid;
            height: 100%;
            background: #fff;
            padding-right: 5px;
        }

        .a-main-nav__list-item a:hover {
            color: #fff;
        }

        .green-link {
            border-bottom: 1px dashed #5bb646;
            color: #5bb646;
            text-decoration: none;
        }

        .green-link:hover {
            border: none;
            color: #4da339;
            text-decoration: none;
        }

        .a-signals-nav__item .link {
            height: 230px;
        }

        .a-user-name a:hover {
            color: #8e9ef5 !important;
        }

        .a-header__login a:hover {
            color: #fff !important;
        }

        .btn_teacher a:hover {
            color: #fff !important;
        }

        .course-image {
            position: relative;
            border: 1px #656565 solid;
            background: #bdbdbd;
            width: 300px;
        }

        .course-image-style {
            background-repeat: no-repeat !important;
            background-size: cover !important;
            color: #6c7289;
            height: 150px;
        }

        .course-image-text-style {
            color: #909090;
        }

        .tit-content-right {
            margin-top: 20px;
            margin-bottom: 10px;
            margin-left: 20px;
            float: right;
        }

        .glyphicon-ok {
            margin: 5px;
            color: #5a5a5a;
        }

        .a-channel-row {
            padding: 0 0 10px;
        }

        .a-channel__item {
            padding: 0 0 25px;
        }

        .a-channel-row__col2-child {
            width: -webkit-calc(20.61% + -35px);
            width: -moz-calc(20.61% + -35px);
            width: calc(20.61% + -35px);
        }

        .col1 {
            border-left: none;
            width: 100%;
        }

        .col2 {
            border-left: none;
            width: -webkit-calc(20.61% + 526px);
            width: -moz-calc(20.61% + 526px);
            width: calc(20.61% + 526px);
        }

        .row__block {
            background: #ffffff;
            border: solid 1px #e8e9eb;
            padding: 10px 30px 10px 10px;
        }

        .row__block__dark {
            background: #f9f9f9;
            border: solid 1px #e8e9eb;
            padding: 10px 30px 10px 10px;
            width: auto;
        }

        .row__block__dark div label {
            cursor: pointer;
        }

        .glyphicon-right-side {
            top: 45%;
            right: 3%;
            transform: translate(-3%, -50%);
        }

        .a-text-center {
            text-align: center;
            top: 50%;
            left: 50%;
            position: absolute;
            transform: translate(-50%, -50%);
            margin: 0;
        }

        .clearfix {
            overflow: hidden;
        }


        .course-image-text-style .course-label {
            position: absolute;
            bottom: 0;
            height: 30px;
            background-color: rgba(1.0, 1.0, 1.0, 0.3);
            color: #fff;
            width: 100%;
            text-align: center;
        }

        .course-label label {
            margin-top: 2px;
            margin-bottom: 1px;
            margin-left: 5px;
            font-size: 1.4em;
        }

        .slide {
            cursor: pointer;
        }

        .pin-header-show {
            visibility: visible;
            opacity: 1.0;
            transition: opacity 0.5s;
        }

        .pin-header-hide {
            opacity: 0.0;
            visibility: hidden;
            transition: visibility 0.5s, opacity 0.5s linear;
        }

        .a-selected {
            background: #6ab9ff;
        }

        .a-selected .a-lb-input {
            color: #fff !important;
        }

        .child-container .a-channel-row {
            cursor: pointer;
        }

        .row__block_hover:hover {
            background: #97d9ff;
        }

        .content {
            min-height: 400px;
        }
        .content, .a-crumbs {
            width: 80%;
            transition: width 0.5s linear;
        }

        .close-menu .content, .a-crumbs{
            width: 84%;
        }

        .right-side {
            width: 21%;
            top: 0;
            padding-bottom: 100%;
            float: right;
            margin-bottom: 10px;
            margin-left: 20px;
        }

        .center-abs{
            position:absolute;
            right: 0;
            top: 50%;
            transform: translate(-50%,-50%);
        }

        .glyphicon-refresh-animate {
            -animation: spin .7s infinite linear;
            -webkit-animation: spin2 .7s infinite linear;
        }

        @-webkit-keyframes spin2 {
            from { -webkit-transform: rotate(0deg);}
            to { -webkit-transform: rotate(360deg);}
        }

        @keyframes spin {
            from { transform: scale(1) rotate(0deg);}
            to { transform: scale(1) rotate(360deg);}
        }

    </style>
</head>
<body>
<div class="a-page">
    <nav class="a-main-nav a-page__menu">
        <div class="a-main-nav__tit">Confpulse</div>
        <div class="a-main-nav__menu">
            <ul class="a-main-nav__list">
                <li class="a-main-nav__list-item"><a href="/welcome">Главная</a></li>
            </ul>
            <ul class="a-main-nav__list">
                <li class="a-main-nav__list-item"><a href="/#examples">как это работает</a></li>
                <li class="a-main-nav__list-item"><a href="/#about">о проекте </a></li>
                <li class="a-main-nav__list-item"><a href="/#questions">контакты </a></li>
                <li class="a-main-nav__list-item"><a href="/registration">регистрация</a></li>
            </ul>
        </div>
        <ul class="a-share-nav">
            <li class="a-share-nav__item"><a class="icon-facebook" href="#"></a></li>
            <li class="a-share-nav__item"><a class="icon-youtubel" href="#"></a></li>
            <li class="a-share-nav__item"><a class="icon-instagram" href="#"></a></li>
        </ul>
    </nav>
    <? include_once(F_PATH_SYS."tpl/inc/header_interface.tpl.php") ?>
    <main class="a-body a-page__main">
        <div class="a-center">
            <div class="clearfix">
                <div style="float: left;width: 75%;">
                    <div class="a-crumbs"><a href="/groups">Inspiratura<span class="icon"><span></span></span></a><span><?= $course['name'] ?></span></div>
                    <div class="content">
                    </div>
                </div>
            </div>
        </div>
        <input id="id" type="hidden" value="<?= $id ?>">
    </main>
    <right_side_menu>
            <div class="a-control">
                <div class="a-entry">
                    <div class="a-channel">
                        <div style="padding-top: 25px;padding-left: 30px;padding-bottom: 5px;">
                            <div style="width: auto;padding-right: 14px;">
                                <?= $course['name'] ?>
                            </div>
                            <div>
                                <?= $total_sections ?> разделов,
                                <?= $total_blocks ?> лекций
                            </div>
                        </div>
                        <? foreach ($course['sections'] as $i => $section): ?>
                            <div class="container-main">
                                <div class="container-middleware">
                                    <div class="a-channel__item" style="border-bottom: none; padding: 0 0 0;">
                                        <div class="a-channel-row row__block__dark slide">
                                            <div class="a-channel-row__col2 col1">
                                                <label><b>Раздел <?= $i + 1 ?>: </b><?= $section['name'] ?>
                                                </label>
                                            </div>
                                            <div class="a-channel-row__col2"
                                                 style="text-align: -webkit-right;text-align: -moz-right;text-align: right; border: 0;">
                                                <label style="margin-right: 10%;">
                                                    <span id="count"><?= $progressCount[$i]['current'] ?></span>
                                                    /<b id="total_count"><?= $progressCount[$i]['total'] ?></b> </label>
                                                <i class="glyphicon glyphicon-chevron-down glyphicon-right-side clickable"
                                                   style="position: absolute;"></i>
                                            </div>
                                            <input id="section_id" type="hidden" value="<?= $section['id'] ?>">
                                        </div>
                                        <div class="block_content"
                                             style="background: #ffffff; border: solid 1px #e8e9eb; display: none;">
                                            <? foreach ($section['blocks'] as $ch_i => $block): ?>
                                                <div class="child-container">
                                                    <div class="a-channel-row row__block row__block_hover <?= isset($last_view) && $last_view['section_id'] === $section['id'] && $last_view['block_id'] === $block['id'] ? 'last_view' : ''?>">
                                                        <div class="a-channel-row__col2 col1">
                                                            <div class="a-lb-input">
                                                                <input type="checkbox" class="a-checkbox js-input"
                                                                       style="float: left;" <?= isset($progress[$section['id']]) && in_array($block['id'], $progress[$section['id']]) ? 'checked' : '' ?>/>
                                                                <?= ++$block_count.'. '.$block['name'] ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input id="block_id" type="hidden" value="<?= $block['id'] ?>">
                                                </div>
                                            <? endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
    </right_side_menu>
</div>
<script src="js/vendor.js?v=1557472175815"></script>
<script src="js/main.js?v=1557472175815"></script>
</body>
</html>
