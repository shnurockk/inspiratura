<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php')); ?>
<!DOCTYPE html>
<html class="page-index" lang="ru">
<head>
    <? include_once(F_PATH_SYS."tpl/inc/html_head_tags.tpl.php") ?>
    <link rel="stylesheet" href="/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/css/interface.css?v=1557472175816">
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/course.js"></script>
    <? include_once(F_PATH_SYS."tpl/inc/prevent_access.tpl.php") ?>
    <style>
        .a-btn-head {
            color:#fff !important;
        }
        .input-error {
            border-color: #ff4e4e;
            border-style: solid;
        }
    </style>
</head>
<body>
<div class="a-page">
    <nav class="a-main-nav a-page__menu">
        <div class="a-main-nav__tit">Confpulse</div>
        <div class="a-main-nav__menu">
            <ul id="main_sections" class="a-main-nav__list">
                <li class="a-main-nav__list-item"><a href="/course_edit?stage=info<? if(!empty($group_id)) { echo '&group_id='.$group_id;} ?>">Информация о курсе</a></li>
                <li class="a-main-nav__list-item is-active"><a href="/course_edit?stage=cost<? if(!empty($group_id)) { echo '&group_id='.$group_id;}?>">Стоимость курса</a></li>
                <li class="a-main-nav__list-item"><a href="/course_edit?stage=section<? if(!empty($group_id)) { echo '&group_id='.$group_id;} ?>">План курса</a></li>
            </ul>
            <ul class="a-main-nav__list">
                <li class="a-main-nav__list-item">
                    <button class="a-btn a-btn--green curse-info-save" style="width: 100%;">Сохранить</button>
                </li>
            </ul>
        </div>
        <ul class="a-share-nav">
            <li class="a-share-nav__item"><a class="icon-facebook" href="#"></a></li>
            <li class="a-share-nav__item"><a class="icon-youtubel" href="#"></a></li>
            <li class="a-share-nav__item"><a class="icon-instagram" href="#"></a></li>
        </ul>
    </nav>
    <? include_once(F_PATH_SYS."tpl/inc/header_interface.tpl.php") ?>
    <main class="a-body a-page__main">
        <div class="a-center">
            <div class="a-crumbs"><a href="/">Inspiratura <span class="icon"><span></span></span></a><span class="page">Стоимость курса</span></div>
            <form id="curse-cost" method="post">
                <div class="a-form">
                    <h2 class="a-tit">Стоимость курса</h2>
                    <div id="cost_0" class="a-form-row">
                        <div class="a-form-row__lb">
                            <label class="a-form-lb">Стоимость курса</label>
                        </div>
                        <div class="a-form-row__fild">
                            <input id="cost" value="<?= isset($course['cost']) ? $course['cost'] : ''?>" type="text" name="cost" class="a-input" maxlength="60"/>
                            <label style="position: absolute; margin: 15px;top:0;left:100%;">ГРН</label>
                        </div>
                        <div class="a-form-row__valid a-form-row__valid-name">

                        </div>
                    </div>
                    <div class="a-form-row a-form-row--bot">
                        <div class="a-form-row__btn">
                            <button class="a-btn a-btn--green curse-info-save-session" data-target="curse-cost">Сохранить</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div style="display: none;">
            <div id="block_learn">
                <div class="item-drop">
                    <div class="a-form-row">
                        <div class="a-form-row__lb"><label class="a-form-lb"></label></div>
                        <div class="a-form-row__fild input-with-counter">
                            <input class="a-input" id="learn" value="" type="text" name="learn" maxlength="">
                            <label class="input-counter">∞</label>
                        </div>
                        <div class="a-form-row__valid" style="cursor: pointer">
                            <div class="a-form-valid a-form-valid--error delete">Удалить</div>
                        </div>
                        <label class="draggable" style="background: #D3D3D3;width: 53px;height:53px;padding: 5px 12px;position: absolute;"></label>
                    </div>
                </div>
            </div>
            <div id="block_requirements">
                <div class="item-drop">
                    <div class="a-form-row">
                        <div class="a-form-row__lb"><label class="a-form-lb"></label></div>
                        <div class="a-form-row__fild input-with-counter">
                            <input class="a-input" id="requirements" value="" type="text" name="requirements" maxlength="">
                            <label class="input-counter">∞</label>
                        </div>
                        <div class="a-form-row__valid" style="cursor: pointer">
                            <div class="a-form-valid a-form-valid--error delete">Удалить</div>
                            <div class="a-form-row__valid a-form-row__valid-learn">
                            </div>
                        </div>
                        <label class="draggable" style="background: #D3D3D3;width: 53px;height:53px;padding: 5px 12px;position: absolute;"></label>

                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" id="group_id" value="<?= $group_id ?>">
        <input type="hidden" id="stage" value="cost">
</div>
</main>
</div>
<script src="js/vendor.js?v=1557472175815"></script>
<script src="js/main.js?v=1557472175815"></script>
</body>
</html>

