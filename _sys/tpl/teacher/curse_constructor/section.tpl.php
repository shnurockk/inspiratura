<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php')); ?>
<!DOCTYPE html>
<html class="page-index" lang="ru">
<head>
    <? include_once(F_PATH_SYS."tpl/inc/html_head_tags.tpl.php") ?>
    <link rel="stylesheet" href="/css/interface.css?v=1557472175816">
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/course.js"></script>
    <script src='https://cdn.tiny.cloud/1/vyjvz9zafasmf90uecaz5qmyctc3gunl3ojt2w4yemvxdjw9/tinymce/5/tinymce.min.js'
            referrerpolicy="origin"></script>
    <? include_once(F_PATH_SYS."tpl/inc/prevent_access.tpl.php") ?>
    <script>
        $(function () {
            group_id = $('input#group_id').val();
            stage = $('#stage').val();

            $(".edit_block").on("click", function (e) {
                e.preventDefault();
                message = $(`<? include(F_PATH_SYS.'tpl/teacher/curse_constructor/modal/edit_block.tpl.php') ?>`);

                parent = $(this).attr('data-parent-target');
                parent_index = $(this).closest('.' + parent).find('input.index').attr('value');
                item = $(this).closest('.a-channel-row');
                //Определяем attachtype и attach для message

                $.post("/_ajax.php", {
                    token: $('#f_token').attr("content"),
                    file: 'course_get_session',
                    group_id: group_id,
                    key: "blocks",
                    index: $('input.index', item).val(),
                    stage_index: parent_index,
                    stage: 'sections',
                }, function (r) {
                    try {
                        result = jQuery.parseJSON(r);
                    } catch (err) {
                        $.sweetModal({
                            content: r,
                            icon: $.sweetModal.ICON_ERROR,
                        });
                    }
                    if (result.status == "success") {
                        $('form', message).attr('id', 'edit-block');
                        $('.a-tit', message).text('Изменение блока');
                        $('#basic_text', message).text(result.data['attach']);
                        $('#name', message).attr('value', result.data['name']);
                        $('#index', message).attr('value', $('input.index', item).val());
                        $('#section_index', message).attr('value', parent_index);

                        $.sweetModal.defaultSettings.confirm.yes.label = 'Изменить';
                        $.sweetModal.defaultSettings.confirm.cancel.label = 'Отмена';
                        $.sweetModal.confirm(message.html(),
                            function () {
                                data = $("form#edit-block").serialize();
                                save();
                            }, function () {
                                tinymce.remove('#basic_text');
                            });
                    } else {
                        $.sweetModal({
                            content: result.mes,
                            icon: $.sweetModal.ICON_ERROR,
                        });
                    }
                });

            });

            $(".add-content").on("click", function (e) {
                e.preventDefault();
                message = $(`<? include(F_PATH_SYS.'tpl/teacher/curse_constructor/modal/edit_block.tpl.php') ?>`);

                item = $(this).closest('.container-middleware');
                index = $('input.index', item).val();

                $('form', message).attr('id', 'create-block');
                $('#section_index', message).attr('value', index);

                $.sweetModal.defaultSettings.confirm.yes.label = 'Добавить';
                $.sweetModal.defaultSettings.confirm.cancel.label = 'Отмена';
                $.sweetModal.confirm(message.html(),
                    function () {
                        data = $("form#create-block").serialize();
                        save();
                    }, function () {
                        tinymce.remove('#basic_text');
                    });
            });

            $(".edit").on("click", function (e) {
                e.preventDefault();
                item = $(this).closest('.a-channel-row');
                index = $('input.index', item).val();
                message = $(`<? include(F_PATH_SYS.'tpl/teacher/curse_constructor/modal/edit_section.tpl.php') ?>`);

                $.post("/_ajax.php", {
                    token: $('#f_token').attr("content"),
                    file: 'course_get_session',
                    group_id: group_id,
                    stage_index: index,
                    stage: 'sections',
                }, function (r) {
                    try {
                        result = jQuery.parseJSON(r);
                    } catch (err) {
                        $.sweetModal({
                            content: r,
                            icon: $.sweetModal.ICON_ERROR,
                        });
                    }
                    if (result.status == "success") {
                        $('form', message).attr('id', 'edit-section');
                        $('.a-tit', message).text('Изменение раздела');
                        $('#name', message).attr('value', result.data['name']);
                        $('#desc', message).text(result.data['desc']);
                        $('#index', message).attr('value', index);

                        $.sweetModal.defaultSettings.confirm.yes.label = 'Изменить';
                        $.sweetModal.defaultSettings.confirm.cancel.label = 'Отмена';
                        $.sweetModal.confirm(message.html(),
                            function () {
                                data = $('form#edit-section').serialize();
                                save();
                            }, function () {
                                tinymce.remove('#basic_text');
                            });
                    } else {
                        $.sweetModal({
                            content: result.mes,
                            icon: $.sweetModal.ICON_ERROR,
                        });
                    }
                })
            });

            $(".a-btn--add").on("click", function (e) {
                e.preventDefault();
                message = $(`<? include(F_PATH_SYS.'tpl/teacher/curse_constructor/modal/edit_section.tpl.php') ?>`);
                $('form', message).attr('id', 'create-section');
                $.sweetModal.defaultSettings.confirm.yes.label = 'Создать';
                $.sweetModal.defaultSettings.confirm.cancel.label = 'Отмена';
                $.sweetModal.confirm(message.html(),
                    function () {
                        data = $('form#create-section').serialize();
                        save();
                    });

                $(".input-with-counter").on("keydown keyup paste", ".a-input", function () {
                    $(this).parent().children('.input-counter').text($(this).attr('maxlength') - $(this).val().length);
                });

                $(".input-with-counter .a-input").each(function () {
                    $(this).parent().children('.input-counter').text($(this).attr('maxlength'));
                });
            });

            function save() {
                $.post("/_ajax.php", {
                    token: $('#f_token').attr("content"),
                    file: 'course_save_session',
                    group_id: group_id,
                    data: data,
                    stage: stage,
                }, function (r) {
                    try {
                        result = jQuery.parseJSON(r);
                    } catch (err) {
                        $.sweetModal({
                            content: r,
                            icon: $.sweetModal.ICON_ERROR,
                        });
                    }
                    if (result.status == "success") {
                        $.sweetModal({
                            timeout: 1500,
                            content: result.mes,
                            icon: $.sweetModal.ICON_SUCCESS,
                            onClose: function () {
                                sessionStorage.setItem('course_changed_prevent_alert', 'true');
                                window.location.reload();
                            }
                        });
                    } else {
                        $.sweetModal({
                            content: result.mes,
                            icon: $.sweetModal.ICON_ERROR,
                        });
                    }
                });
            }

            $(".delete").on("click", function (e) {
                e.preventDefault();
                item = $(this).closest('.a-channel-row');
                index = $('input.index', item).attr('value');
                parent = $(this).attr('data-parent-target');
                index_parent = item.closest('.' + parent).find('input.index').first().attr('value');
                el_name = $('#name', item).html();
                $.sweetModal.defaultSettings.confirm.yes.label = 'Да';
                $.sweetModal.defaultSettings.confirm.cancel.label = 'Нет';
                $.sweetModal.confirm('Вы действительно хотите удалить \'<b>' + el_name + '</b>\'?',
                    function () {
                        $.post("/_ajax.php", {
                            token: $('#f_token').attr("content"),
                            file: "course_delete_section",
                            index: index,
                            section_index: index_parent,
                            group_id: group_id
                        }, function (r) {
                            try {
                                result = jQuery.parseJSON(r);
                            } catch (err) {
                                $.sweetModal({
                                    content: r,
                                    icon: $.sweetModal.ICON_ERROR,
                                });
                            }

                            if (result.status == "success") {
                                $.sweetModal({
                                    timeout: 1500,
                                    content: result.mes,
                                    icon: $.sweetModal.ICON_SUCCESS,
                                    onClose: function () {
                                        sessionStorage.setItem('course_changed_prevent_alert', 'true');
                                        window.location.reload();
                                    }
                                });
                            } else {
                                $.sweetModal({
                                    content: result.mes,
                                    icon: $.sweetModal.ICON_ERROR,
                                });
                            }
                        });
                    });
            });

            $(".input-with-counter").on("keydown keyup paste", ".a-input", function () {
                $(this).parent().children('.input-counter').text($(this).attr('maxlength') - $(this).val().length);
            });

            $(".input-with-counter .a-input").each(function () {
                $(this).parent().children('.input-counter').text($(this).attr('maxlength'));
            });

            /** DRAGGING **/
            preventDropParent = false;
            preventDropChild = false;
            $(".child-swap").parent().each(function () {
                $(this).draggable({
                    revert: true,
                    zIndex: 10,
                    containment: $(this).parent().parent(),
                    snap: $(this).parent(),
                    snapMode: "inner",
                    snapTolerance: 5,
                    axis: "y",
                    start: function (event, ui) {
                        lastPlace = $(this).parent();
                        preventDropParent = true;
                        preventDropChild = false
                    }
                });

                $(this).parent().droppable({
                    drop: function (event, ui) {
                        if (preventDropChild) return;
                        var dropped = $(ui.draggable);
                        console.log(lastPlace);
                        console.log(dropped);
                        var droppedOn = $(this);
                        console.log(droppedOn);

                        from_index = $('input.index', lastPlace);
                        to_index = $('input.index', droppedOn);
                        section_index = $('input.index', lastPlace.parent().parent()).attr('value');
                        console.log(from_index);
                        console.log(to_index);
                        console.log(section_index);
                        if (droppedOn.children().length > 0) {
                            droppedOn.children().detach().prependTo(lastPlace);
                        }

                        dropped.detach().css({
                            top: 0,
                            left: 0
                        }).prependTo(droppedOn);


                        $.post("/_ajax.php", {
                            token: $('#f_token').attr("content"),
                            file: "course_swap_section",
                            from_index: from_index.val(),
                            to_index: to_index.val(),
                            section_index: section_index,
                            group_id: group_id
                        }, function (r) {
                            try {
                                result = jQuery.parseJSON(r);
                            } catch (err) {
                                $.sweetModal({
                                    content: r,
                                    icon: $.sweetModal.ICON_ERROR,
                                });
                            }
                            if (result.status == "success") {
                                console.log('changed');
                                old_from = from_index.val();
                                old_to = to_index.val();
                                from_index.val(old_to);
                                to_index.val(old_from);
                            } else {
                                $.sweetModal({
                                    content: result.mes,
                                    icon: $.sweetModal.ICON_ERROR,
                                });
                            }
                        });
                    },
                });
            });


            $(".parent-swap").parent().draggable({
                revert: true,
                zIndex: 10,
                containment: ".a-channel__list",
                snap: ".draggable-parent",
                snapMode: "inner",
                snapTolerance: 5,
                axis: "y",
                start: function (event, ui) {
                    lastPlace = $(this).parent().parent().parent();
                    preventDropParent = false;
                    preventDropChild = true;
                }
            });

            $(".container-main").droppable({
                drop: function (event, ui) {
                    if (preventDropParent) {
                        return;
                    }

                    var dropped = $(ui.draggable).parent().parent();
                    console.log(lastPlace);
                    console.log(dropped);
                    var droppedOn = $(this);
                    console.log(droppedOn);

                    from_index = $('input.index', lastPlace).first();
                    to_index = $('input.index', droppedOn).first();

                    if (droppedOn.children().length > 0) {
                        droppedOn.children().detach().prependTo(lastPlace);
                    }

                    dropped.detach().css({
                        top: 0,
                        left: 0
                    }).prependTo(droppedOn);

                    $.post("/_ajax.php", {
                        token: $('#f_token').attr("content"),
                        file: "course_swap_section",
                        from_index: from_index.val(),
                        to_index: to_index.val(),
                        group_id: group_id
                    }, function (r) {
                        try {
                            result = jQuery.parseJSON(r);
                        } catch (err) {
                            $.sweetModal({
                                content: r,
                                icon: $.sweetModal.ICON_ERROR,
                            });
                        }
                        if (result.status == "success") {
                            old_from = from_index.val();
                            old_to = to_index.val();
                            from_index.val(old_to);
                            to_index.val(old_from);
                        } else {
                            $.sweetModal({
                                content: result.mes,
                                icon: $.sweetModal.ICON_ERROR,
                            });
                        }
                    });
                },
            });
        });
    </script>
    <style>
        .a-form-lb {
            text-align: left
        }

        .a-form-valid {
            width: 16px;
            height: 16px;
            padding-left: 0;
            text-indent: -9999em;
            overflow: hidden;
            z-index: 5;
        }

        .a-form-valid:before {
            text-indent: 0
        }

        .a-form, .a-form .a-tit {
            padding: 0 0 30px
        }

        .a-form-row {
            margin: 0 0 25px;
            padding: 0;
            display: block
        }

        .a-form-row__lb {
            width: auto;
            padding: 0 0 10px
        }

        .a-form-row__fild {
            width: 100%;
            margin: 0;
            display: inline-block
        }

        .a-form-row__fild--sm {
            width: 200px
        }

        .a-form-row__fild .a-input {
            padding-right: 0px
        }

        .a-form-row__valid {
            width: 25px;
            margin: 0;
            display: block;
            position: absolute;
            bottom: 18px;
            right: 15px
        }

        .a-user-name a:hover {
            color: #8e9ef5 !important;
        }

        .a-header__login a:hover {
            color: #fff !important;
        }

        .a-btn-head {
            color: #fff !important;
        }

        .input-counter {
            position: absolute;
            width: 53px;
            text-align: center;
            left: calc(100% - 66px);
            z-index: 999999;
            padding: 5px 12px;
            margin: 4px;
            background: #D3D3D3;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            font-family: Roboto, sans-serif;
            font-size: 20px;
            font-weight: 400;
            line-height: 32px;
            color: #222;
            top: 0;
        }

        .input-counter-bottom {
            top: calc(100% + 5px);
        }

        .input-with-counter input {
            padding-right: 65px;
        }

        .a-btn:hover {
            color: #fff;
        }

        .col1 {
            border-left: none;
            width: 100%;
            padding-left: 30px;
        }

        .col2 {
            border-left: none;
            width: -webkit-calc(20.61% + 526px);
            width: -moz-calc(20.61% + 526px);
            width: calc(20.61% + 526px);
        }

        .col_action {
            border-left: none;
            width: -webkit-calc(20.61% + 0px);
            width: -moz-calc(20.61% + 0px);
            width: calc(20.61% + 0px);
        }

        .swap-corner {
            position: absolute;
            font-weight: 200;
            font-size: 16px;
            top: calc(50% - 26px);
            padding: 10px 0;
            color: #337ab7;
        }

        .red {
            color: #e84752
        }

        .a-channel-row {
            padding: 0 0 10px;
        }

        .a-channel__item {
            padding: 0 0 25px;
        }

        .a-channel-row__col2-child {
            width: -webkit-calc(20.61% + -35px);
            width: -moz-calc(20.61% + -35px);
            width: calc(20.61% + -35px);
        }

        label {
            font-weight: 400;
        }

        .row__block {
            background: #ffffff;
            border: solid 1px #e8e9eb;
            padding: 10px 30px 10px 22px;
        }

        .row__block__dark {
            background: #f9f9f9;
            border: solid 1px #e8e9eb;
            padding: 10px 30px 10px 22px;
            width: auto;
        }
    </style>
</head>
<body>
<div class="a-page">
    <nav class="a-main-nav a-page__menu">
        <div class="a-main-nav__tit">Confpulse</div>
        <div class="a-main-nav__menu">
            <ul id="main_sections" class="a-main-nav__list">
                <li class="a-main-nav__list-item"><a href="/course_edit?stage=info<? if (!empty($group_id)) {
                        echo '&group_id='.$group_id;
                    } ?>">Информация о курсе</a></li>
                <li class="a-main-nav__list-item"><a href="/course_edit?stage=cost<? if (!empty($group_id)) {
                        echo '&group_id='.$group_id;
                    } ?>">Стоимость курса</a></li>
                <li class="a-main-nav__list-item is-active"><a
                            href="/course_edit?stage=section<? if (!empty($group_id)) {
                                echo '&group_id='.$group_id;
                            } ?>">План курса</a></li>
            </ul>
            <ul class="a-main-nav__list">
                <li class="a-main-nav__list-item">
                    <button class="a-btn a-btn--green curse-info-save" style="width: 100%;">Сохранить</button>
                </li>
            </ul>
        </div>
        <ul class="a-share-nav">
            <li class="a-share-nav__item"><a class="icon-facebook" href="#"></a></li>
            <li class="a-share-nav__item"><a class="icon-youtubel" href="#"></a></li>
            <li class="a-share-nav__item"><a class="icon-instagram" href="#"></a></li>
        </ul>
    </nav>
    <? include_once(F_PATH_SYS."tpl/inc/header_interface.tpl.php") ?>
    <main class="a-body a-page__main">
        <div class="a-center">
            <div class="a-crumbs"><a href="/">Inspiratura <span class="icon"><span></span></span></a><span class="page">План курса</span>
            </div>
            <div class="a-channel">
                <div class="a-entry">
                    <h2 class="a-tit">План курса</h2>
                    <button class="a-btn a-btn--green a-btn--add" style="width: 195px;" data-id="section">Создать
                        раздел
                    </button>
                </div>
                <div class="a-channel__wrapping">
                    <div class="a-channel__wrapping-scroll">
                        <div class="a-channel__list" style="border-bottom: 2px solid #f0f0f0;">
                            <? if (isset($course['sections'])): ?>
                                <? foreach ($course['sections'] as $i => $section): ?>
                                    <? if (isset($section['name']) && $section['deleted'] == 0): ?>
                                        <div class="container-main">
                                            <div class="container-middleware">
                                                <div class="a-channel__item" style="border-bottom: none;">
                                                    <div class="a-channel-row row__block__dark">
                                                    <span class="swap-corner parent-swap"><i
                                                                class="glyphicon glyphicon-align-justify"
                                                                style="top: 6px;"></i></span>

                                                        <div class="a-channel-row__col2 col1">
                                                            <label id="name"><b>Раздел: </b><?= $section['name'] ?>
                                                            </label>
                                                        </div>
                                                        <div class="a-channel-row__col2 col2">
                                                            <input type="hidden" class="index" value="<?= $i ?>">
                                                        </div>
                                                        <div class="a-channel-row__col2 col_action">
                                                            <div class="a-channel__btns" style="margin: 5px;">
                                                                <div class="a-channel__btns-col-lg">
                                                                    <a href="#" title="Добавить контент"
                                                                       class="add-content"><i
                                                                                class="glyphicon glyphicon-file"></i></a>
                                                                </div>
                                                                <div class="a-channel__btns-col-lg">
                                                                    <a href="#" title="Редактировать" class="edit"><i
                                                                                class="glyphicon glyphicon-pencil"></i></a>
                                                                </div>
                                                                <div class="a-channel__btns-col-lg"
                                                                     style="left: 100%; line-height: 14px; color: #e84752;">
                                                                    <a href="#" title="Удалить" class="delete"
                                                                       data-index="<?= $i ?>"><i
                                                                                class="glyphicon glyphicon-remove red"></i></a>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div id="child-container" style="background: #ffffff;
                                                border: solid 1px #e8e9eb;">
                                                        <? if (isset($section['blocks'])): ?>
                                                            <? foreach ($section['blocks'] as $ch_i => $block): ?>
                                                                <? if ($block['deleted'] == 0): ?>
                                                                    <div class="child-container">
                                                                        <div class="a-channel-row row__block">
                                                                <span class="swap-corner child-swap"><i
                                                                            class="glyphicon glyphicon-align-justify"
                                                                            style="top: 6px;"></i></span>

                                                                            <div class="a-channel-row__col2 col1">
                                                                                <label id="name"><?= $block['name'] ?></label>
                                                                                <input type="hidden" class="index"
                                                                                       value="<?= $ch_i ?>">
                                                                            </div>
                                                                            <div class="a-channel-row__col2 col2">

                                                                            </div>
                                                                            <div class="a-channel-row__col2 a-channel-row__col2-child col_action">
                                                                                <div class="a-channel__btns"
                                                                                     style="margin: 5px;">
                                                                                    <div class="a-channel__btns-col-lg">
                                                                                        <a href="#"
                                                                                           title="Редактировать"
                                                                                           class="edit_block"
                                                                                           data-parent-target="container-middleware"><i
                                                                                                    class="glyphicon glyphicon-pencil"></i></a>
                                                                                    </div>
                                                                                    <div class="a-channel__btns-col-lg"
                                                                                         style="left: 100%; line-height: 14px; color: #e84752;">
                                                                                        <a href="#" title="Удалить"
                                                                                           class="delete delete_block"
                                                                                           data-parent-target="container-middleware"><i
                                                                                                    class="glyphicon glyphicon-remove red"></i></a>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <? endif; ?>
                                                            <? endforeach; ?>
                                                        <? else: ?>
                                                            <div id="index_<?= $i ?>" class="a-channel-row row__block">
                                                                <div class="a-channel-row__col2 col2">
                                                                    <label>Нету блоков!!</label>
                                                                </div>
                                                            </div>
                                                        <? endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                <? endforeach; ?>
                            <? else: ?>
                                <div class="a-channel__item">
                                    <div class="a-channel-row">
                                        Пусто
                                    </div>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="group_id" value="<?= $group_id ?>">
        <input type="hidden" id="stage" value="section">
    </main>
</div>
<script src="js/vendor.js?v=1557472175815"></script>
<script src="js/main.js?v=1557472175815"></script>
</body>
</html>

