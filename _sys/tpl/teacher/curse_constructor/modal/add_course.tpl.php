<form id="add-course">
    <div class="a-form" style="padding: 0 !important;">
        <h2 class="a-tit">Добавление нового курса</h2>
        <div class="a-form-row">
            <div class="a-form-row__lb">
                <label class="a-form-lb">Название курса</label>
            </div>
            <div class="a-form-row__fild input-with-counter">
                <input id="name" value="" type="text" name="name" class="a-input" maxlength="60" style="padding-right: 65px;"/>
                <label class="input-counter">∞</label>
            </div>
            <div class="a-form-row__valid a-form-row__valid-name">

            </div>
        </div>
    </div>
</form>
