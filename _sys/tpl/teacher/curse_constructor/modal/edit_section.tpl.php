<div>
    <?=
    "<script>
$(function() {
                $(\".input-with-counter\").on(\"keydown keyup paste\", \".a-input\", function () {
                    $(this).parent().children('.input-counter').text($(this).attr('maxlength') - $(this).val().length);
                });

                $(\".input-with-counter .a-input\").each(function () {
                    $(this).parent().children('.input-counter').text($(this).attr('maxlength') - $(this).val().length);
                });
});
<\/script>"
    ?>
    <form>
        <div class="a-form" style="padding: 0 !important;">
            <h2 class="a-tit">Создание раздела</h2>
            <div class="a-form-row">
                <div class="a-form-row__lb">
                    <label class="a-form-lb">Название раздела</label>
                </div>
                <div class="a-form-row__fild input-with-counter">
                    <input id="name" value="" type="text" name="name" class="a-input" maxlength="60"/>
                    <label class="input-counter">∞</label>
                </div>
                <div class="a-form-row__valid a-form-row__valid-name">

                </div>
            </div>
            <div class="a-form-row">
                <div class="a-form-row__lb">
                    <label class="a-form-lb">Описание раздела</label>
                </div>
                <div class="a-form-row__fild input-with-counter">
                    <label for="description"></label>
                    <textarea class="a-input" id="desc" name="desc" maxlength="200" style="height: 182px;"></textarea>
                    <label class="input-counter input-counter-bottom">∞</label>

                </div>
                <div class="a-form-row__valid a-form-row__valid-learn">

                </div>
            </div>
        </div>

        <input id='index' type="hidden" name="index" value="">
    </form>
</div>