<?php
 /*
  * должны быть определенны в вызываемом компоненте
  */
 $SSL_URL = F_SSL_URL;
?>
<div>
    <?= "<script>$(function () {                       
            $(\".input-with-counter\").on(\"keydown keyup paste\", \".a-input\", function () {
                $(this).parent().children('.input-counter').text($(this).attr('maxlength') - $(this).val().length);
            });

            $(\".input-with-counter .a-input\").each(function () {
                $(this).parent().children('.input-counter').text($(this).attr('maxlength') - $(this).val().length);
            });
            
             tinymce.init({
                    selector: 'textarea#basic_text',
                    height: 500,
                    menubar: false,
                    plugins: [
                        'advlist autolink lists link image charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table code help wordcount'
                    ],
                    toolbar: 'undo redo | formatselect | ' +
                        ' bold italic forecolor backcolor | alignleft aligncenter ' +
                        ' alignright alignjustify | bullist numlist outdent indent |' +
                        ' removeformat | help | image code media',
                    content_css: [
                        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                        '//www.tiny.cloud/css/codepen.min.css'
                    ],
                    setup: function (editor) {
                        editor.on('change', function () {
                            editor.save();
                        });
                    },
                    images_upload_url: '$SSL_URL/_ajax.php',

                    /* we override default upload handler to simulate successful upload*/
                    images_upload_handler: function (blobInfo, success, failure) {

                        var xhr, formData;
                        xhr = new XMLHttpRequest();
                        xhr.withCredentials = false;
                        xhr.open('POST', '$SSL_URL/_ajax.php');
                        xhr.onload = function () {
                            var json;

                            if (xhr.status != 200) {
                                failure('HTTP Error: ' + xhr.status);
                                return;
                            }
                            json = JSON.parse(xhr.responseText);

                            if (!json || typeof json.location != 'string') {
                                failure('Invalid JSON: ' + xhr.responseText);
                                return;
                            }
                            success(json.location);
                        };
                        formData = new FormData();
                        formData.append('token', $('#f_token').attr(\"content\"));
                        formData.append('upload', blobInfo.blob(), blobInfo.filename());
                        formData.append('file', \"upload_image\");
                        xhr.send(formData);
                    }
                });
        });
            
            
        <\/script>" ?>

    <form id="" style="margin-bottom: -49px;" enctype="multipart/form-data">
        <div class="a-form" style="padding: 0 !important;">
            <h2 class="a-tit">Добавление блока</h2>
            <div class="a-form-row">
                <div class="a-form-row__lb">
                    <label class="a-form-lb">Название блока</label>
                </div>
                <div class="a-form-row__fild input-with-counter">
                    <input id="name" value="" type="text" name="name" class="a-input" maxlength="60"/>
                    <label class="input-counter">∞</label>
                </div>
                <div class="a-form-row__valid a-form-row__valid-name">

                </div>
            </div>

            <div class="a-form-row" style="padding: 0;">
                <div id="text-info" style="width: 100%;">
                        <div class="a-form-row__fild" style="width: 100%;" >
                            <textarea id="basic_text" name="attach" class="a-input" style="height: 300px;" ></textarea>
                        </div>
                    <div class="a-form-row">
                        <div class="a-form-row__valid a-form-row__valid-attach">

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <input id="section_index" name="section_index" type="hidden" value="">
        <input id="block_id" name="id" type="hidden" value="">
        <input id="index" name="index" type="hidden" value="">
    </form>
</div>