<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php')); ?>
<!DOCTYPE html>
<html class="page-index" lang="ru">
<head>
    <? include_once(F_PATH_SYS."tpl/inc/html_head_tags.tpl.php") ?>
    <link rel="stylesheet" href="/css/interface.css?v=1557472175816">
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/js/course.js"></script>
    <? include_once(F_PATH_SYS."tpl/inc/prevent_access.tpl.php") ?>
    <script>
        $(function () {

            $('input').change(function () {
                sessionStorage.setItem('course_changed', 'true');
            });

            $(".add_more_blocks").on("click", function (e) {
                e.preventDefault();

                target = $(this).attr('data-target');
                parent = $(this).attr('data-parent');
                length = $(this).parent().parent().find('.a-input').attr('maxlength');

                html = $(target).html();
                html = $(parent).append(html);

                input = $(html).find('.a-input').last();
                input.attr('maxlength', length);
                input.on('keydown keyup paste', function () {
                    $(this).parent().children('.input-counter').text($(this).attr('maxlength') - $(this).val().length);
                });
                input.parent().children('.input-counter').text(input.attr('maxlength'));

                dataTarget = $(".draggable", html).attr('data-target');
                $(".draggable", html).parent().parent().draggable({
                    revert: true,
                    zIndex: 999,
                    containment: $(this).closest('.a-form-multi-inputs'),
                    snap: dataTarget,
                    snapMode: "inner",
                    snapTolerance: 40,
                    axis: "y",
                    start: function (event, ui) {
                        lastPlace = $(this).parent();
                    }
                });
                $(dataTarget, html).droppable({
                    drop: function (event, ui) {
                        var dropped = ui.draggable;
                        var droppedOn = this;

                        if ($(droppedOn).children().length > 0) {
                            $(droppedOn).children().detach().prependTo($(lastPlace));
                        }

                        $(dropped).detach().css({
                            top: 0,
                            left: 0
                        }).prependTo($(droppedOn));
                    },
                });

            });

            $('body').on("click", ".delete", function (e) {
                e.preventDefault();
                parent = $(this).parent().parent().parent();
                container = parent.closest('.a-form');
                deleted = $('input.deleted', parent);
                total = container.find('.delete').filter(function() { return $(this).parent().parent().parent().css("display") !== "none" }).length;

                if (total === 1) {
                    $('input.a-input', parent).val('');
                    $('.input-counter', parent).text($('input.a-input', parent).attr('maxlength'));
                } else if (deleted.length === 0) {
                    parent.remove();
                } else {
                    parent.attr('style', 'display: none;');
                    deleted.attr('value', 1);
                }
            });

            $(".input-with-counter").on("keydown keyup paste", ".a-input", function () {
                $(this).parent().children('.input-counter').text($(this).attr('maxlength') - $(this).val().length);
            });

            $(".input-with-counter .a-input").each(function () {
                $(this).parent().children('.input-counter').text($(this).attr('maxlength') - $(this).val().length);
            });

            $(".draggable").each(function () {
                dataTarget = $(this).attr('data-target');
                $(this).parent().parent().draggable({
                    revert: true,
                    zIndex: 999,
                    containment: $(this).closest('.a-form-multi-inputs'),
                    snap: dataTarget,
                    snapMode: "inner",
                    snapTolerance: 40,
                    axis: "y",
                    start: function (event, ui) {
                        lastPlace = $(this).parent();
                    }
                });
                $(dataTarget).droppable({
                    drop: function (event, ui) {
                        var dropped = ui.draggable;
                        var droppedOn = this;

                        if ($(droppedOn).children().length > 0) {
                            $(droppedOn).children().detach().prependTo($(lastPlace));
                        }

                        $(dropped).detach().css({
                            top: 0,
                            left: 0
                        }).prependTo($(droppedOn));
                    },
                });
            });

            $('#upload_image_trigger').on('click', function(e){
                e.preventDefault();
                $('#upload_image')[0].click();
            });

            $('#upload_image').on('change', function(e){
                var form_file = new FormData();
                form_file.append('token', $('#f_token').attr("content"));
                form_file.append('upload', $(this)[0].files[0]);
                form_file.append('file', "upload_image");

                $.ajax({
                    url: "/_ajax.php",
                    type: "POST",
                    data: form_file,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (js) {
                        result = JSON.parse(js);
                        $('#uploaded_image_name').val(result.location);

                        data = $('form').serialize();
                        stage = $('input#stage').val();
                        group_id = $('input#group_id').val();

                        save_data_to_session(true);
                    },
                    error: function () {
                        $.sweetModal({
                            content: 'Не удалось загрузить файл!',
                            icon: $.sweetModal.ICON_ERROR
                        });
                    }
                });
            });

            $('.img-remove').on('click', function (e) {
                e.preventDefault();
                $('#uploaded_image_name').val('');

                data = $('form').serialize();
                stage = $('input#stage').val();
                group_id = $('input#group_id').val();

                save_data_to_session(true);
            })

        });
    </script>
    <style>
        .input-counter {
            position: absolute;
            width: 53px;
            text-align: center;
            left: calc(100% - 66px);
            z-index: 5;
            padding: 5px 12px;
            margin: 4px;
            background: #D3D3D3;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            font-family: Roboto, sans-serif;
            font-size: 20px;
            font-weight: 400;
            line-height: 32px;
            color: #222;
            top: 0;
        }

        .input-with-counter input {
            padding-right: 65px;
        }

        .input-with-draggable input{
            padding-left: 49px;
        }

        .input-error {
            border-color: #ff4e4e;
            border-style: solid;
        }

        .a-form-multi-inputs-row{
            padding: 0 0 0;
            -webkit-align-items: start;
            -moz-box-align: start;
            -ms-flex-align: start;
            align-items: start;
        }

        .a-form-multi-inputs{
            width: -webkit-calc(100% - 33%);
            width: -moz-calc(100% - 33%);
            width: calc(100% - 33%);
            position: relative;
        }

        .a-form-multi-input_field {
            width: 100%;
        }

        .draggable {
            line-height: 17px; color: #6c7289; z-index: 6; position: absolute;
        }

        .a-glyphicon {
            font-weight: 200;font-size: 25px;margin: 13px;display: block;
        }

        .glyphicon-remove {
            position: absolute;
            background-color: rgba(0, 0, 0, 0.6);
            padding: 6px;
            border-radius: 50%;
            color: #f93838;
        }

        .img-remove {
            display: none;
            position: relative;
        }

        .course-image {
            position:relative;
            border: 1px #656565 solid;
            background: #bdbdbd;
            width: 300px;
        }

        .course-image-is-set:hover .img-remove{
            display: block;
        }

        .course-image-style{
            background-repeat: no-repeat !important;
            background-size:cover !important;
            height: 150px;
            color: #6c7289;
        }

        .course-image-text-style{
            color: #909090;
        }

        .a-text-center{
            text-align: center;
            top: 50%;left: 50%;
            position: absolute;
            transform: translate(-50%, -50%);
            margin: 0;
        }
    </style>
</head>
<body>
<div class="a-page">
    <nav class="a-main-nav a-page__menu">
        <div class="a-main-nav__tit">Confpulse</div>
        <div class="a-main-nav__menu">
            <ul id="main_sections" class="a-main-nav__list">
                <li class="a-main-nav__list-item is-active"><a href="/course_edit?stage=info<? if (!empty($group_id)) {
                        echo '&group_id='.$group_id;
                    } ?>">Информация о курсе</a></li>
                <li class="a-main-nav__list-item"><a href="/course_edit?stage=cost<? if (!empty($group_id)) {
                        echo '&group_id='.$group_id;
                    } ?>">Стоимость курса</a></li>
                <li class="a-main-nav__list-item"><a href="/course_edit?stage=section<? if (!empty($group_id)) {
                        echo '&group_id='.$group_id;
                    } ?>">План курса</a></li>
            </ul>
            <ul class="a-main-nav__list">
                <li class="a-main-nav__list-item">
                    <button class="a-btn a-btn--green curse-info-save" style="width: 100%;">Сохранить</button>
                </li>
            </ul>
        </div>
        <ul class="a-share-nav">
            <li class="a-share-nav__item"><a class="icon-facebook" href="#"></a></li>
            <li class="a-share-nav__item"><a class="icon-youtubel" href="#"></a></li>
            <li class="a-share-nav__item"><a class="icon-instagram" href="#"></a></li>
        </ul>
    </nav>
    <? include_once(F_PATH_SYS."tpl/inc/header_interface.tpl.php") ?>
    <main class="a-body a-page__main">
        <div class="a-center">
            <div class="a-crumbs"><a href="/">Inspiratura <span class="icon"><span></span></span></a><span class="page">Информация о курсе</span>
            </div>
            <form id="curse-info" method="post">
                <div class="a-form">
                    <h2 class="a-tit">Информация о курсе</h2>
                    <div class="a-form-row">
                        <div class="a-form-row__lb">
                            <label class="a-form-lb">Обложка курса</label>
                        </div>
                        <div class="a-form-row__fild">
                            <div class="course-image <?= !empty($course['img']) ? 'course-image-is-set' : '' ?>">
                                <div class="img-remove" title="Удалить изображение..."><i class="a-glyphicon glyphicon glyphicon-remove"></i></div>
                                <div class="course-image-text-style">
                                     <a id="upload_image_trigger" href="#" title="Загрузить изображение...">
                                        <div class="course-image-style" style="background: url('<?=$course['img']?>');">
                                             <div class="a-text-center" style="<?= !empty($course['img']) ? 'display:none;' : '' ?> ">
                                                  <i class="a-glyphicon glyphicon glyphicon-picture"></i>
                                                  <label style="cursor: pointer;">300x200</label>
                                             </div>
                                        </div>
                                     </a>
                                </div>
                                <input id="uploaded_image_name" type="hidden" name="image_name" value="<?= $course['img']?>">
                                <input id="upload_image" type="file" name="image" style="display: none;">
                            </div>
                        </div>
                        <div class="a-form-row__valid a-form-row__valid-image">

                        </div>
                    </div>
                    <div class="a-form-row">
                        <div class="a-form-row__lb">
                            <label class="a-form-lb">Название курса</label>
                        </div>
                        <div class="a-form-row__fild input-with-counter">
                            <input id="name" value="<?= isset($course['name']) ? $course['name'] : '' ?>" type="text" name="name" class="a-input" maxlength="60"/>
                            <label class="input-counter">∞</label>
                        </div>
                        <div class="a-form-row__valid a-form-row__valid-name">

                        </div>
                    </div>
                    <div id="blocks">
                            <div class="a-form" style="padding: 0 0 30px;">
                                <div class="a-form-row a-form-multi-inputs-row">
                                    <div class="a-form-row__lb" style="top: 10px;">
                                        <label class="a-form-lb">Что студенты будут изучать на вашем курсе? </label>
                                    </div>
                                    <div class="a-form-multi-inputs">
                                        <? if (!empty($course['learns'])): ?>
                                            <? foreach ($course['learns'] as $k => $l): ?>
                                                <div id="learns_<?= $k ?>" class="item-drop-a"
                                                     style="<?= $l['deleted'] == 1 ? 'display: none;' : '' ?>">
                                                    <div class="a-form-row">
                                                        <div class="a-form-row__fild a-form-multi-input_field input-with-draggable input-with-counter">
                                                            <label class="draggable" data-target=".item-drop-a"><i class="a-glyphicon glyphicon glyphicon-align-justify"></i></label>
                                                            <input class="a-input" id="learns" value="<?= $l['desc'] ?>" type="text" name="learns[]" maxlength="200">
                                                            <label class="input-counter">∞</label>
                                                        </div>
                                                        <div class="a-form-row__valid" style="cursor: pointer;">
                                                            <div class="a-form-valid a-form-valid--error delete">Удалить</div>
                                                            <div class="a-form-row__valid a-form-row__valid-learn"></div>
                                                        </div>

                                                        <input type="hidden" class="deleted" value="<?= $l['deleted'] ?>" name="learn_deletes[]">
                                                    </div>
                                                </div>
                                            <? endforeach; ?>
                                        <? endif; ?>
                                        <div id="block_learn_main">

                                        </div>
                                        <div class="a-form-row" style="top: 15px;">
                                            <div class="a-form-row__btn" style="padding: 6px 151px 0 4px;">
                                                <a class="green-link add_more_blocks" href="#js" data-target="#block_learn"
                                                   data-parent="#block_learn_main">Добавить поле</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <div class="a-form" style="padding: 0 0 30px;">
                            <div class="a-form-row a-form-multi-inputs-row">
                                <div class="a-form-row__lb" style="top: 10px;">
                                    <label class="a-form-lb">Существуют ли у курса предварительные требования?</label>
                                </div>
                                <div class="a-form-multi-inputs">
                                    <? if (!empty($course['requirements'])): ?>
                                        <? foreach ($course['requirements'] as $k => $l): ?>
                                            <div id="requirements_<?= $k ?>" class="item-drop-b"
                                                 style="<?= $l['deleted'] == 1 ? 'display: none;' : '' ?>">
                                                <div class="a-form-row">
                                                    <div class="a-form-row__fild a-form-multi-input_field input-with-draggable input-with-counter">
                                                        <label class="draggable" data-target=".item-drop-b"><i class="a-glyphicon glyphicon glyphicon-align-justify"></i></label>
                                                        <input class="a-input" id="requirements" value="<?= $l['desc'] ?>" type="text" name="requirements[]" maxlength="200">
                                                        <label class="input-counter">∞</label>
                                                    </div>
                                                    <div class="a-form-row__valid" style="cursor: pointer;">
                                                        <div class="a-form-valid a-form-valid--error delete">Удалить</div>
                                                        <div class="a-form-row__valid a-form-row__valid-requirements"></div>
                                                    </div>

                                                    <input type="hidden" class="deleted" value="<?= $l['deleted'] ?>"
                                                           name="requirement_deletes[]">
                                                </div>
                                            </div>
                                        <? endforeach; ?>
                                    <? endif; ?>
                                    <div id="block_requirements_main">

                                    </div>
                                    <div class="a-form-row" style="top: 15px;">
                                        <div class="a-form-row__btn" style="padding: 6px 151px 0 4px;">
                                            <a class="green-link add_more_blocks" href="#js" data-target="#block_requirements"
                                               data-parent="#block_requirements_main">Добавить поле</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="a-form" style="padding: 0 0 30px;">
                            <div class="a-form-row a-form-multi-inputs-row">
                                <div class="a-form-row__lb" style="top: 10px;">
                                    <label class="a-form-lb">Какова ваша целевая аудитория?</label>
                                </div>
                                <div class="a-form-multi-inputs">
                                    <? if (!empty($course['audiences'])): ?>
                                        <? foreach ($course['audiences'] as $k => $l): ?>
                                            <div id="audiences_<?= $k ?>" class="item-drop-c"
                                                 style="<?= $l['deleted'] == 1 ? 'display: none;' : '' ?>">
                                                <div class="a-form-row">
                                                    <div class="a-form-row__fild a-form-multi-input_field input-with-draggable input-with-counter">
                                                        <label class="draggable" data-target=".item-drop-c"><i class="a-glyphicon glyphicon glyphicon-align-justify"></i></label>
                                                        <input class="a-input" id="audiences" value="<?= $l['desc'] ?>" type="text" name="audiences[]" maxlength="200">
                                                        <label class="input-counter">∞</label>
                                                    </div>
                                                    <div class="a-form-row__valid" style="cursor: pointer;">
                                                        <div class="a-form-valid a-form-valid--error delete">Удалить</div>
                                                        <div class="a-form-row__valid a-form-row__valid-audiences"></div>
                                                    </div>

                                                    <input type="hidden" class="deleted" value="<?= $l['deleted'] ?>" name="audience_deletes[]">
                                                </div>
                                            </div>
                                        <? endforeach; ?>
                                    <? endif; ?>
                                    <div id="block_audinces_main">

                                    </div>
                                    <div class="a-form-row" style="top: 15px;">
                                        <div class="a-form-row__btn" style="padding: 6px 151px 0 4px;">
                                            <a class="green-link add_more_blocks" href="#js" data-target="#block_audiences"
                                               data-parent="#block_audinces_main">Добавить поле</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="a-form-row a-form-row--bot">
                            <div class="a-form-row__btn">
                                <button class="a-btn a-btn--green curse-info-save-session" data-target="curse-info">
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div style="display: none;">
            <div id="block_learn">
                <div class="item-drop-a">
                    <div class="a-form-row">
                        <div class="a-form-row__fild a-form-multi-input_field input-with-draggable input-with-counter">
                            <label class="draggable" data-target=".item-drop-a"><i class="a-glyphicon glyphicon glyphicon-align-justify"></i></label>
                            <input class="a-input" id="learns" value="" type="text" name="learns[]" maxlength="200">
                            <label class="input-counter">∞</label>
                        </div>
                        <div class="a-form-row__valid" style="cursor: pointer">
                            <div class="a-form-valid a-form-valid--error delete">Удалить</div>
                            <div class="a-form-row__valid a-form-row__valid-learn"></div>
                        </div>

                    </div>
                </div>
            </div>
            <div id="block_requirements">
                <div class="item-drop-b">
                    <div class="a-form-row">
                        <div class="a-form-row__fild a-form-multi-input_field input-with-draggable input-with-counter">
                            <label class="draggable" data-target=".item-drop-b"><i class="a-glyphicon glyphicon glyphicon-align-justify"></i></label>
                            <input class="a-input" id="requirements" value="" type="text" name="requirements[]"
                                   maxlength="200">
                            <label class="input-counter">∞</label>
                        </div>
                        <div class="a-form-row__valid" style="cursor: pointer">
                            <div class="a-form-valid a-form-valid--error delete">Удалить</div>
                            <div class="a-form-row__valid a-form-row__valid-requirements">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="block_audiences">
                <div class="item-drop-c">
                    <div class="a-form-row">
                        <div class="a-form-row__fild a-form-multi-input_field input-with-draggable input-with-counter">
                            <label class="draggable" data-target=".item-drop-c"><i class="a-glyphicon glyphicon glyphicon-align-justify"></i></label>
                            <input class="a-input" id="audiences" value="" type="text" name="audiences[]" maxlength="200">
                            <label class="input-counter">∞</label>
                        </div>
                        <div class="a-form-row__valid" style="cursor: pointer">
                            <div class="a-form-valid a-form-valid--error delete">Удалить</div>
                            <div class="a-form-row__valid a-form-row__valid-audiences">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<input type="hidden" id="group_id" value="<?= $group_id ?>">
<input type="hidden" id="stage" value="info">
</main>
</div>
<script src="js/vendor.js?v=1557472175815"></script>
<script src="js/main.js?v=1557472175815"></script>
</body>
</html>

