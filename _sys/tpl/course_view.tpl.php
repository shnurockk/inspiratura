<!DOCTYPE html>
<html class="page-index" lang="ru">
    <head>
        <? include_once(F_PATH_SYS."tpl/inc/html_head_tags.tpl.php") ?>
        <link rel="stylesheet" href="/css/interface.css?v=1557472175815">
        <link rel="stylesheet" href="/css/bootstrap/bootstrap.min.css">
        <script type="text/javascript" src="/css//bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="/js/common.js"></script>
        <script type="text/javascript" src="/js/course_view.js"></script>
        <script>
            $(document).ready(function () {
<? if (!is_null($buy_result) and ! empty($buy_result)): ?>
    <? if ($buy_result == "failed"): ?>
                        $.sweetModal({
                            content: "Простите, покупка не удалась. Попробуйте еще раз.",
                            icon: $.sweetModal.ICON_ERROR,
                            onClose: function () {
                                window.open("/course_view?group_id=" + $('#group_id').val(), '_self');
                            }
                        });
    <? else: ?>
                        $.sweetModal({
                            content: "Покупка успешна! После подтверждения оплаты курс станет доступен!",
                            icon: $.sweetModal.ICON_SUCCESS,
                            onClose: function () {
                                window.open("/course_view?group_id=" + $('#group_id').val(), '_self');
                            }
                        });
    <? endif; ?>
<? endif; ?>
                $('.slide').on('click', function () {
                    glyphicon = $(this).parent().find('.glyphicon');
                    block = $(this).parent().find('.block_content');
                    if (block.first().is(":hidden")) {
                        block.slideDown();
                        glyphicon.removeClass('glyphicon-chevron-down');
                        glyphicon.addClass('glyphicon-chevron-up');
                    } else {
                        block.slideUp();
                        glyphicon.removeClass('glyphicon-chevron-up');
                        glyphicon.addClass('glyphicon-chevron-down');
                    }
                });

                $('.buy').on('click', function (e) {
                    e.preventDefault();
                    group_id = $('#group_id').val();
                    $.sweetModal({
                        timeout: 3000,
                        content: "Вы будете отправлены на страницу оплаты",
                        icon: $.sweetModal.ICON_SUCCESS
                    });
                    //Эмуляция покупки
                    $.post("/_ajax.php", {
                        token: $('#f_token').attr("content"),
                        file: 'course_buy',
                        group_id: group_id
                    }, function (r) {
                        try {
                            result = jQuery.parseJSON(r);
                        } catch (err) {
                            $.sweetModal({
                                content: 'Unexpected errors: ' + r,
                                icon: $.sweetModal.ICON_ERROR
                            });
                            return false;
                        }
                        if (result.status == "success") {
                            window.open(result.mes, '_self');

                        } else {
                            $.sweetModal({
                                content: result.mes,
                                icon: $.sweetModal.ICON_ERROR
                            });
                        }
                    });
                });
            });
        </script>
        <style>
            h2{
                display: inline-block;
                word-break: break-word;
            }
            .a-main-nav__list-item a:hover{
                color: #fff;
            }

            .green-link{
                border-bottom: 1px dashed #5bb646;
                color: #5bb646;
                text-decoration: none;
            }
            .green-link:hover{
                border: none;
                color: #4da339;
                text-decoration: none;
            }

            .a-signals-nav__item .link{
                height: 230px;
            }

            .a-user-name a:hover {
                color: #8e9ef5 !important;
            }

            .a-header__login a:hover{
                color: #fff !important;
            }

            .btn_teacher a:hover {
                color: #fff !important;
            }

            .course-image {
                position: relative;
                border: 1px #656565 solid;
                background: #bdbdbd;
                width: 300px;
            }

            .course-image-style{
                background-repeat: no-repeat !important;
                background-size:cover !important;
                color: #6c7289;
                height: 150px;
            }

            .course-image-text-style{
                color: #909090;
            }

            .tit-content-right{
                margin-top: 20px;
                margin-bottom: 10px;
                margin-left: 20px;
                float: right;
            }

            .glyphicon-ok {
                margin: 5px;
                color: #5a5a5a;
            }

            .a-channel-row {
                padding: 0 0 10px;
            }

            .a-channel__item {
                padding: 0 0 25px;
            }

            .a-channel-row__col2-child {
                width: -webkit-calc(20.61% + -35px);
                width: -moz-calc(20.61% + -35px);
                width: calc(20.61% + -35px);
            }

            .col1 {
                border-left: none;
                width: 100%;
            }

            .col2 {
                border-left: none;
                width: -webkit-calc(20.61% + 526px);
                width: -moz-calc(20.61% + 526px);
                width: calc(20.61% + 526px);
            }

            label {
                font-weight: 400;
            }

            .row__block {
                background: #ffffff;
                border: solid 1px #e8e9eb;
                padding: 10px 30px 10px 22px;
            }

            .row__block__dark {
                background: #f9f9f9;
                border: solid 1px #e8e9eb;
                padding: 10px 30px 10px 22px;
                width: auto;
            }

            .row__block__dark div label {
                cursor: pointer;
            }

            .glyphicon-right-side {
                top: 45%;
                right: 3%;
                transform: translate(-3%, -50%);
            }

            .a-text-center {
                text-align: center;
                top: 50%;left: 50%;
                position: absolute;
                transform: translate(-50%, -50%);
                margin: 0;
            }

            .clearfix {
                overflow: auto;
            }


            #wrap {
                position: fixed;
                z-index: 9;
                margin-top: -15px;
                padding-left: 0;
                padding-right: 0;
                background-color:#fff;
                -webkit-transition: all .6s ease;
                -o-transition: all .6s ease;
                -moz-transition: all .6s ease;
                transition: all .6s ease;
                left: 0;
                width: 100%;
                height: 101px;
            }

            .pinned-tit {
                z-index: 9;
            }

            .pinned-tit .a-tit{
                width: 300px;
            }

            .pinned-tit .pinned-tit-content{
                width: 100%;
                height: 78px;
            }

            .pinned-tit div {
                display: inline-block;
                vertical-align: middle;
            }

            .pinned-tit .pinned-tit-label{
                font-size: 1em;
            }

            .pinned-tit .pinned-tit-right{
                -webkit-transition: all .6s ease;
                -o-transition: all .6s ease;
                -moz-transition: all .6s ease;
                transition: all .6s ease;
                height: 64px;
                width: auto;
                position: relative;
                top: 0;
                margin-top: 0;
                left: 45%;
                transform: translateX(-45%);
            }

            .pinned-tit .pinned-tit-horizontal{
                text-align: center;
                margin-bottom: 0;
                margin-right: 5px;
                font-size: 1.2em;
            }

            .pinned-tit .pinned-tit-btn{
                position: relative;
                margin-top: 0;
                left: 0;
                -moz-transform: none;
                -webkit-transform: none;
                transform: none;
            }

            .pinned-tit .pinned-tit-name {
                position: relative;
                word-wrap: break-word;
                word-break: break-word;
                margin-top: 0;
                -webkit-transition: all .6s ease;
                -o-transition: all .6s ease;
                -moz-transition: all .6s ease;
                transition: all .6s ease;
            }

            .close-menu .pinned-tit .pinned-tit-right{
                left: 63%;
                transform: translateX(-63%);
            }

            @media (max-width: 1135px) {
                .close-menu .pinned-tit .pinned-tit-right{
                    left: 53%;
                    transform: translateX(-53%);
                }
            }

            @media (max-width: 990px) {
                .close-menu .pinned-tit .pinned-tit-name{
                    padding-left: 250px;
                }

                .close-menu .pinned-tit .pinned-tit-right {
                    width: 260px;
                    margin-left: -15px;
                    left: 63%;
                    transform: translateX(-63%);
                }
            }

            @media (max-width: 857px) {
                .pinned-tit .pinned-tit-content{
                    margin-top: -15px;
                }

                .pinned-tit .pinned-tit-btn{
                    position: relative;
                    left: 50%;
                    -moz-transform: translateX(-50%);
                    -webkit-transform: translateX(-50%);
                    transform: translateX(-50%);
                }

                .pinned-tit .pinned-tit-right{
                    width: 155px;
                    left: 50%;
                    -moz-transform: translateX(-50%);
                    -webkit-transform: translateX(-50%);
                    transform: translateX(-50%);
                }

                .pinned-tit .pinned-tit-horizontal{
                    left: 50%;
                    -moz-transform: translateX(-50%);
                    -webkit-transform: translateX(-50%);
                    transform: translateX(-50%);
                    position: relative;
                }

                .pinned-tit .pinned-tit-name{
                    margin-top: 10px;
                    width: 285px;
                }
            }

            @media (max-width: 750px) {
                #wrap {
                    margin-top: -5px;
                }

                .pinned-tit .pinned-tit-content{
                    margin-top: 0;
                }

                .pinned-tit .pinned-tit-name{
                    margin-top: 10px;
                    width: 285px;
                }

                .pinned-tit .pinned-tit-name .a-tit{
                    padding: 0 0 0 10px !important;
                }

                .pinned-tit .pinned-tit-right{
                    width: 155px !important;
                    left: 0;
                    -moz-transform: none;
                    -webkit-transform: none;
                    transform: none;
                }

                .close-menu .pinned-tit .pinned-tit-right {
                    margin-left: 0;
                }
            }

            @media (max-width: 600px) {
                .close-menu .pinned-tit .pinned-tit-right {
                    opacity: 0;
                }
            }



            .course-image-text-style .course-label {
                position:absolute;
                bottom: 0;
                height: 30px;
                background-color: rgba(1.0,1.0,1.0,0.3);
                color: #fff;
                width: 100%;
                text-align: center;
            }

            .course-label label {
                margin-top: 2px;
                margin-bottom: 1px;
                margin-left: 5px;
                font-size: 1.4em;
            }

            .slide {
                cursor: pointer;
            }

            .pin-header-show {
                visibility: visible;
                opacity: 1.0;
                transition: opacity 0.5s;
            }

            .pin-header-hide {
                opacity: 0.0;
                visibility: hidden;
                transition: visibility 0.5s, opacity 0.5s linear;
            }

        </style>
    </head>
    <body>
        <div class="a-page">
            <nav class="a-main-nav a-page__menu">
                <div class="a-main-nav__tit">Confpulse</div>
                <div class="a-main-nav__menu">
                    <ul class="a-main-nav__list">
                        <li class="a-main-nav__list-item"><a href="/welcome">Главная</a></li>
                    </ul>
                    <ul class="a-main-nav__list">
                        <li class="a-main-nav__list-item"><a href="/#examples">как это работает</a></li>
                        <li class="a-main-nav__list-item"><a href="/#about">о проекте </a></li>
                        <li class="a-main-nav__list-item"><a href="/#questions">контакты </a></li>
                        <li class="a-main-nav__list-item"><a href="/registration">регистрация</a></li>
                    </ul>
                </div>
                <ul class="a-share-nav">
                    <li class="a-share-nav__item"><a class="icon-facebook" href="#"></a></li>
                    <li class="a-share-nav__item"><a class="icon-youtubel" href="#"></a></li>
                    <li class="a-share-nav__item"><a class="icon-instagram" href="#"></a></li>
                </ul>
            </nav>
            <? include_once(F_PATH_SYS."tpl/inc/header_interface.tpl.php") ?>
            <main class="a-body a-page__main">
                <div class="a-center">
                    <div id="wrap" class="pin-header-hide js-pin-header a-header">
                        <div class="pinned-tit a-center">
                            <div class="pinned-tit-content">
                                <div class="pinned-tit-name">
                                    <h2 class="a-tit js-resize-text" style="padding: 0 0 0; margin-top: 9px;"><?= $course['name'] ?></h2>
                                </div>
                                <div class="pinned-tit-right">
                                    <div class="pinned-tit-horizontal">
                                        <label class="pinned-tit-label"><?= intval($course['cost']) ?> ГРН</label>
                                    </div>
                                    <div class="pinned-tit-btn">
                                        <? if ($boughtCourse === false): ?>
                                            <a href="#" class="a-btn a-btn--green buy" style="margin: 5px;left: 25%;">Купить</a>
                                        <? else: ?>
                                            <a href="/course?id=<?= $boughtCourse['id'] ?>" class="a-btn a-btn--green" style="margin: 5px;left: 25%;">Перейти к курсу</a>
                                        <? endif; ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="a-crumbs"><a href="/groups">Inspiratura<span class="icon"><span></span></span></a><span><?= $course['name'] ?></span></div>
                    <div class="content">
                        <div class="a-channel clearfix">
                            <div class="tit-content-right">
                                <div class="course-image">
                                    <div class="course-image-text-style">
                                        <a href="#" title="Просмотр изображения">
                                            <div class="course-image-style" style="background: url('<?= $course['img'] ?>');">
                                                <div class="a-text-center" style="<?= !empty($course['img']) ? 'display:none;' : '' ?> ">
                                                    <i class="glyphicon glyphicon-picture" style="font-size: 5em;"></i>
                                                </div>
                                                <div class="course-label">
                                                    <label><?= intval($course['cost']) ?> ГРН</label>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <? if ($boughtCourse === false): ?>
                                    <a href="#" class="a-btn a-btn--green buy" style="margin: 5px;left: 25%;">Купить</a>
                                <? else: ?>
                                    <a href="/course?id=<?= $boughtCourse['id'] ?>" class="a-btn a-btn--green" style="margin: 5px;left: 25%;">Перейти к курсу</a>
                                <? endif; ?>
                            </div>
                            <div class="a-entry" style="padding: 0 0 0;">
                                <h2 class="a-tit" style="padding: 0 0 0;width: 50%;"><?= $course['name'] ?></h2>
                            </div>
                            <div class="a-entry" style="padding: 0 0 0;">
                                <label>
                                    Автор курса:
                                    <a href="/find_course?author=id:<?= $author->id ?>"><?= $author->fio ?></a>
                                </label>
                            </div>
                            <div class="a-entry" style="padding: 0 0 15px; border-bottom: 2px solid #f0f0f0;">
                                <h3>Что вы будете изучать на курсе:</h3>
                                <ul>
                                    <? foreach ($course['learns'] as $k => $l): ?>
                                        <li><i class="glyphicon glyphicon-ok"></i><?= $l['desc'] ?></li>
                                    <? endforeach; ?>
                                </ul>
                            </div>
                            <div class="a-entry" style="padding: 0 0 5px; border-bottom: 2px solid #f0f0f0;">
                                <div class="a-channel__wrapping">
                                    <div class="a-channel__wrapping-scroll">
                                        <div class="a-channel__list">
                                            <h3>План курса:</h3>
                                            Разделов: <b><?= $total_sections ?></b>, Лекций: <b><?= $total_blocks ?></b>
                                            <? foreach ($course['sections'] as $i => $section): ?>
                                                <div class="container-main">
                                                    <div class="container-middleware">
                                                        <div class="a-channel__item" style="border-bottom: none; padding: 0 0 0;">
                                                            <div class="a-channel-row row__block__dark slide">
                                                                <div class="a-channel-row__col2 col1">
                                                                    <label><b>Раздел: </b><?= $section['name'] ?>
                                                                    </label>
                                                                </div>
                                                                <div class="a-channel-row__col2 col2" style="text-align: -webkit-right;text-align: -moz-right;text-align: right;">
                                                                    <label style="margin-right: 10%;">Лекций: <b><?= count($section['blocks']) ?></b> </label>
                                                                    <i class="glyphicon glyphicon-chevron-down glyphicon-right-side clickable" style="position: absolute;"></i>
                                                                </div>
                                                            </div>
                                                            <div class="block_content" style="background: #ffffff; border: solid 1px #e8e9eb; display: none;">
                                                                <? foreach ($section['blocks'] as $ch_i => $block): ?>
                                                                    <div class="child-container">
                                                                        <div class="a-channel-row row__block">
                                                                            <div class="a-channel-row__col2 col1">
                                                                                <label><?= $block['name'] ?></label>
                                                                            </div>
                                                                            <div class="a-channel-row__col2 col2">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <? endforeach; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <? endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input id="group_id" type="hidden" value="<?= $course['id'] ?>">
            </main>
        </div>
        <script src="js/vendor.js?v=1557472175815"></script>
        <script src="js/main.js?v=1557472175815"></script>
    </body>
</html>
