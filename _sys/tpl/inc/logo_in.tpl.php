<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 82 82" style="enable-background:new 0 0 82 82;" xml:space="preserve">

                                    <g>
                                    <circle class="st0" cx="22.4" cy="12.4" r="4.9"/>
                                    <circle class="st0" cx="22.4" cy="69.4" r="4.9"/>
                                    <circle class="st0" cx="13.6" cy="41" r="4.9"/>
                                    <circle class="st0" cx="18.5" cy="29.2" r="4"/>
                                    <circle class="st0" cx="18.5" cy="52.8" r="4"/>
                                    <circle class="st0" cx="51.1" cy="6.6" r="4"/>
                                    <circle class="st0" cx="51.1" cy="75.2" r="4"/>
                                    <circle class="st0" cx="34.6" cy="12.4" r="3.7"/>
                                    <circle class="st0" cx="34.6" cy="69.4" r="3.7"/>
                                    <circle class="st0" cx="11.9" cy="19.1" r="3.7"/>
                                    <circle class="st0" cx="11.9" cy="62.9" r="3.7"/>
                                    <circle class="st0" cx="62" cy="15.4" r="6.3"/>
                                    <circle class="st0" cx="62" cy="66.5" r="6.3"/>
                                    <circle class="st0" cx="43.2" cy="14.5" r="2.6"/>
                                    <circle class="st0" cx="43.2" cy="67.1" r="2.6"/>
                                    <circle class="st0" cx="6.2" cy="26" r="2.6"/>
                                    <circle class="st0" cx="6.2" cy="55.9" r="2.6"/>
                                    <circle class="st0" cx="42.7" cy="4" r="2.6"/>
                                    <circle class="st0" cx="42.7" cy="77.8" r="2.6"/>
                                    <circle class="st0" cx="25.5" cy="23.5" r="1.8"/>
                                    <circle class="st0" cx="25.5" cy="58.5" r="1.8"/>
                                    <circle class="st0" cx="2.9" cy="32" r="1.8"/>
                                    <circle class="st0" cx="2.9" cy="50" r="1.8"/>
                                    <circle class="st0" cx="35.7" cy="2.6" r="1.8"/>
                                    <circle class="st0" cx="35.7" cy="79.2" r="1.8"/>
                                    <circle class="st0" cx="49" cy="18.4" r="1.8"/>
                                    <circle class="st0" cx="49" cy="62.9" r="1.8"/>
                                    </g>
                                    
                                    </svg>