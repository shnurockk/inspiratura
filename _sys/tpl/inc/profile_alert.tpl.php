<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));
$confirm_email = $uinfo->confrim_email;

$alert_profile_fields = '';
if(empty($fio)) {
    $alert_profile_fields .= 'Имя';
}
if(empty($phone)) {
    if(!empty($alert_profile_fields)){
        $alert_profile_fields .= ', ';
    }
    $alert_profile_fields .= 'Телефон';
}

$time_email_expired = F_U_EMAIL_TIME_EXPIRE;
$time_fill_expired = F_U_FILL_TIME_EXPIRE;

?>

<div class="a-entry">
    <? if(!empty($confirm_email) || !empty($alert_profile_fields)): ?>
        <div class="a-important">
            <h2 class="a-tit">Внимание!</h2>
            <? if(!empty($confirm_email)): ?>
                <p>Вы всё ещё не потдвердили почтовый адресс <b><?= $email ?></b>, пожалуйста подтвердите, проверьте не находиться ли письмо в <b>Спаме</b>.</p>
                <p>Аккаунт будет деактивирован через: <b><?= \Fianta\Core\Sys::convert_date_time($time_email_expired)?></b></p><br/>
            <? endif; ?>
            <? if(!empty($alert_profile_fields)): ?>
                <p>Необходимо заполнить поля: <b><?= $alert_profile_fields?></b></p>
                <? if($time_fill_expired >= 0): ?>
                    <p>В течении <b><?= \Fianta\Core\Sys::convert_date_time($time_fill_expired)?></b>, иначе Вы не сможете пользоваться данным сервисом.</p><br/>
                <? else: ?>
                    <b>Заполните профиль что бы дальше использовать сервис!</b>
                <? endif; ?>
            <? endif; ?>

        </div>
    <? endif; ?>
</div>
