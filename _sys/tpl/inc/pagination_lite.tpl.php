<?

use Fianta\Core\Editor;

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'))
?>

<? if (!empty($pagination) and count($pagination) > 1): ?>
    <div class="pagination_wrapper">
        <? foreach ($pagination as $p): ?>
            <a <?= Editor::getVal($p["class"]) ?> <?= Editor::getVal($p["href"]) ?>><?= Editor::getVal($p["num"]) ?></a>
        <? endforeach; ?>
    </div>
<? endif; ?>
<script>
    $('#pagination_orders_selector').select2();
</script>
<div class="pagination_wrapper_orders_selector">
    Показывать сделок: <select id="pagination_orders_selector">
        <? foreach ($orders_per_page as $opp): ?>
            <? if ($opp == 100000): ?>
                <? if ($opp == $limit): ?>
                    <option selected value="<?= $opp ?>">Все</option>
                <? else: ?>
                    <option value="<?= $opp ?>">Все</option>
                <? endif; ?>
            <? elseif ($opp == $limit): ?>
                <option selected value="<?= $opp ?>"><?= $opp ?></option>
            <? else: ?>
                <option value="<?= $opp ?>"><?= $opp ?></option>
            <? endif; ?>
        <? endforeach; ?>
    </select>
</div>
