<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php')); ?>
<? if(F_U_EMAIL_TIME_EXPIRE !== false && F_U_EMAIL_TIME_EXPIRE <= 0): ?>
    <script>
        $(function () {
            $.sweetModal.defaultSettings.confirm.yes.label = 'Отправить письмо';
            $.sweetModal.defaultSettings.confirm.cancel.label = 'Не нужно';
            $.sweetModal.confirm('Нам очень жаль :( <br \>Вы до сих пор не подтвердили свой почтовый адресс,' +
                '<br \>Eсли Вам не пришло письмо Вы можете по пробывать отправить повторно. ' +
                '<br \>Однако, доступ к аккаунт будет ограничен пока Вы не подтвердите почтовый адресс.' +
                ' <br \>По остальным вопросам обратитесь в тех. поддержку.',
                function () {
                    $.post("/_ajax.php", {
                        token: $('#f_token').attr("content"),
                        file: "confrim_email",
                        uid: "<?= \Fianta\Core\User::get()->id ?>"
                    }, function (r) {
                        try {
                            result = jQuery.parseJSON(r);
                        } catch (err) {
                            $.sweetModal({
                                content: r,
                                icon: $.sweetModal.ICON_ERROR,
                                onClose: logout
                            });
                        }
                        if (result.status == "success") {
                            $.sweetModal({
                                content: result.mes,
                                icon: $.sweetModal.ICON_SUCCESS,
                                onClose: logout
                            });
                        } else {
                            $.sweetModal({
                                content: result.mes,
                                icon: $.sweetModal.ICON_ERROR,
                                onClose: logout
                            });
                        }
                    });
                },
            logout);

            function logout() {
                $.post("/_ajax.php", {
                    token: $('#f_token').attr("content"),
                    file: 'auth_logout'
                }, function (r) {
                    try {
                        result = jQuery.parseJSON(r);
                    } catch (err) {
                        $.sweetModal({
                            content: 'Unexpected errors',
                            icon: $.sweetModal.ICON_ERROR
                        });
                        return false;
                    }
                    if (result.status == "success") {
                        window.location.reload();
                    } else {
                        $.sweetModal({
                            content: result.mes,
                            icon: $.sweetModal.ICON_WARNING
                        });
                    }
                });
            }
        });
    </script>
<? elseif(F_U_FILL_TIME_EXPIRE !== false && F_U_FILL_TIME_EXPIRE <= 0): ?>
    <script>
        $(function () {
            if(location.pathname === '/profile_edit'){
                $(".a-form-row__valid").html('');
                <? if(empty($fio)): ?>
                    $(".a-form-row__valid-name").html('<div class="a-form-valid a-form-valid--error">Необходимо заполнить!</div>');
                <? endif; ?>
                <? if(empty($phone)): ?>
                    $(".a-form-row__valid-phone").html('<div class="a-form-valid a-form-valid--error">Необходимо заполнить!</div>');
                <? endif; ?>
                return;
            }

            $.sweetModal({
                content: "Нам очень жаль :( <br \>Но Вы до cих пор не заполнили свой профиль. <br \>Что бы дальше пользоваться сервисом необходимо заполнить профиль. <br \>Сейчас вы будете перенаправленны на страницу редактирования вашего профиля.",
                icon: $.sweetModal.ICON_WARNING,
                onClose: function () {
                    location.href = '/profile_edit'
                }
            });
        })
    </script>
<? endif; ?>