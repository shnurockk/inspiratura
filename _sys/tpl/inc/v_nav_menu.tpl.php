<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

$route = $_SERVER['REQUEST_URI'];
// Убераем слеш
$key = substr($route, 1);

$p_menu_active = '';

// Инициализируем меню
$item_menu['profile_edit']['is_active'] = '';
$item_menu['profile_edit']['link'] = '/profile_edit';
// Меню обучения
$item_menu['groups']['is_active'] = '';
$item_menu['groups']['link'] = '/groups';
$item_menu['subscribed_groups']['is_active'] = '';
$item_menu['subscribed_groups']['link'] = '/subscribed_groups';
$item_menu['all_groups']['is_active'] = '';
$item_menu['all_groups']['link'] = '/all_groups';

// Меняем активность (подсветка ссылки)
$item_menu[$key]['is_active'] = 'is-active';
$item_menu[$key]['link'] = '#';

//Всё что пренадлежит p_menu назначаем активным
if($key === 'groups'
    || $key === 'subscribed_groups'
    || $key === 'all_groups'){
    $p_menu_active = 'p_menu_active';
}
?>
<ul class="a-main-nav__list">
    <li class="a-main-nav__list-item <?= $item_menu['profile_edit']['is_active']?>"><a href="<?= $item_menu['profile_edit']['link']?>">Профиль</a></li>
    <li class="a-main-nav__list-item p_menu <?= $p_menu_active?>">
        <a href="#">Обучение</a>
        <ul class="v_menu">
            <li class="a-main-nav__list-item <?= $item_menu['groups']['is_active'] ?>"><a href="<?= $item_menu['groups']['link'] ?>">Мои Уроки</a></li>
            <li class="a-main-nav__list-item <?= $item_menu['all_groups']['is_active'] ?>"><a href="<?= $item_menu['all_groups']['link'] ?>">Все уроки</a></li>
            <li class="a-main-nav__list-item <?= $item_menu['subscribed_groups']['is_active'] ?>"><a href="<?= $item_menu['subscribed_groups']['link'] ?>">Интересующие уроки</a></li>
        </ul>
    </li>
</ul>
