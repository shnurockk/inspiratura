<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php')); ?>
<!DOCTYPE html>
<html class="page-index" lang="ru">
    <head>
        <? include_once(F_PATH_SYS."tpl/inc/html_head_tags.tpl.php") ?>
        <link rel="stylesheet" href="/css/bootstrap/bootstrap.min.css">
        <script type="text/javascript" src="/css//bootstrap/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/css/interface.css?v=1557472175816">
        <script type="text/javascript" src="/js/common.js"></script>
        <script type="text/javascript" src="/js/_admin/mask/jquery.maskedinput.min.js"></script>
        <script>
            $(function () {
                $('.btn-course').on('click', function () {
                    $.post("/_ajax.php", {
                        token: $('#f_token').attr("content"),
                        file: 'user_mode',
                        user_mode: 1
                    }, function (r) {
                        try {
                            result = jQuery.parseJSON(r);
                        } catch (err) {
                            $.sweetModal({
                                content: 'Unexpected errors: ' + r,
                                icon: $.sweetModal.ICON_ERROR
                            });
                            return false;
                        }
                        if (result.status == "success") {
                            $.sweetModal({
                                timeout: 1500,
                                content: result.mes,
                                icon: $.sweetModal.ICON_SUCCESS,
                                onClose: function () {
                                    window.location.href = '/groups';
                                }
                            });
                        } else {
                            $.sweetModal({
                                content: result.mes,
                                icon: $.sweetModal.ICON_ERROR
                            });
                        }
                    });
                });
            });
        </script>
        <style>
            .vyrovnyat {
                text-align: center;
            }
            .vyrovnyat div {
                display: inline-block;
                vertical-align: middle;
                color: #0b3e69 !important;
                font-size: 16px;
                line-height: 17px;
                font-weight: 700;
                word-break: break-word;
                word-wrap: break-word;
            }
            .vyrovnyat_description div {
                line-height: normal !important;
                color: #b3b8ca !important;
                font-size: 12px;
                margin: 5px;
                word-break: break-word;
                word-wrap: break-word;
            }
            .vyrovnyat:before { /* для IE8+ */
                content: "";
                display: inline-block;
                min-height: inherit;
                height: 100%;
                vertical-align: middle;
            }
            .a-signals-nav__item .link{
                height: 230px;
            }
            .bg-circle i {
                text-align: center;
                color: #fff;
                font-size: 30px;
                padding: 15px;
                border-radius: 30px;
            }
            .bg-green i {
                background: #43f590;
            }
            .bg-yellow i {
                background: #f5cb00;
            }
            .bg-red i {
                background: #f54347;
            }
            .a-user-name a:hover {
                color: #8e9ef5 !important;
            }
            .a-header__login a:hover{
                color:#fff !important;
            }
            .btn_teacher a:hover {
                color: #fff !important;
            }

            @media (max-width: 530px) {
                .a-signals-nav__item {
                    width: 50% !important;
                }
            }

            @media (max-width: 750px) {
                .a-signals-nav__item {
                    width: 37%;
                }
            }
        </style>
    </head>
    <body>
        <div class="a-page">
            <nav class="a-main-nav a-page__menu">
                <div class="a-main-nav__tit">Confpulse</div>
                <div class="a-main-nav__menu">
                    <? require_once(F_PATH_SYS."tpl/inc/v_nav_menu.tpl.php") ?>
                    <ul class="a-main-nav__list">
                        <li class="a-main-nav__list-item"><a href="/#examples">как это работает</a></li>
                        <li class="a-main-nav__list-item"><a href="/#about">о проекте </a></li>
                        <li class="a-main-nav__list-item"><a href="/#questions">контакты </a></li>
                    </ul>
                </div>
                <ul class="a-share-nav">
                    <li class="a-share-nav__item"><a class="icon-facebook" href="#"></a></li>
                    <li class="a-share-nav__item"><a class="icon-youtubel" href="#"></a></li>
                    <li class="a-share-nav__item"><a class="icon-instagram" href="#"></a></li>
                </ul>
            </nav>
            <? include_once(F_PATH_SYS."tpl/inc/header_interface.tpl.php") ?>
            <main class="a-body a-page__main">
                <div class="a-center">
                    <div class="a-crumbs"><span></span><span></span></div>
                    <div class="a-entry" style="to">
                        <h2 class="a-tit">С чего начнём?</h2>
                        <nav class="a-signals-nav cf">
                            <ul>
                                <li class="a-signals-nav__item">
                                    <a class="link link--bitnex" style="text-decoration: none;" href="/groups">
                                        <div class="vyrovnyat" style="width:100%;height:35%;">
                                            <div class="bg-circle bg-green">
                                                <i class="glyphicon glyphicon-random"></i>
                                            </div>
                                        </div>
                                        <div class="vyrovnyat" style="width:100%;height:27%;">
                                            <div>Создать упражнение</div>
                                        </div>
                                        <div class="vyrovnyat_description" style="width:100%;height:35%;">
                                            <div>Легко создавайте упражнения и<br />совершенствуйте свои знания прямо в<br />боте.</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="a-signals-nav__item">

                                    <a class="link link--bitnex btn-course" style="text-decoration: none;" href="#">

                                        <div class="vyrovnyat" style="width:100%;height:35%;">
                                            <div class="bg-circle bg-red">
                                                <i class="glyphicon glyphicon-list"></i>
                                            </div>
                                        </div>
                                        <div class="vyrovnyat" style="width:100%;height:27%;">
                                            <div>Создать свой курс</div>
                                        </div>
                                        <div class="vyrovnyat_description" style="width:100%;height:35%;">
                                            <div>Вне зависимости от вашего опыта в преподавании вы можете сделать свой курс увлекательным, используя простой конструктор.</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="a-signals-nav__item">
                                    <a class="link link--bitnex" style="text-decoration: none;" href="/find_course">
                                        <div class="vyrovnyat" style="width:100%;height:35%;">
                                            <div class="bg-circle bg-yellow">
                                                <i class="glyphicon glyphicon-search"></i>
                                            </div>
                                        </div>
                                        <div class="vyrovnyat" style="width:100%;height:27%;">
                                            <div>Найти курс</div>
                                        </div>
                                        <div class="vyrovnyat_description" style="width:100%;height:35%;">
                                            <div>Изучайте подходящие вам темы в любое время.</div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>

                    <? require_once(F_PATH_SYS.'tpl/inc/profile_alert.tpl.php'); ?>

                    <? if (!empty($alert_profile_fields)): ?>
                        <form id="profile_edit" method="post">
                            <div class="a-form">
                                <h2 class="a-tit">Заполните профиль</h2>
                                <div class="a-form-row">
                                    <div class="a-form-row__lb">
                                        <label class="a-form-lb">Имя *</label>
                                    </div>
                                    <div class="a-form-row__fild">
                                        <input id="name" value="<?= $fio ?>" type="text" name="name" class="a-input" />
                                    </div>
                                    <div class="a-form-row__valid a-form-row__valid-name">

                                    </div>
                                </div>
                                <div class="a-form-row">
                                    <div class="a-form-row__lb">
                                        <label class="a-form-lb">Телефон * </label>
                                    </div>
                                    <div class="a-form-row__fild">
                                        <input class="a-input js-mask-phone" id="phone" value="<?= substr($phone, 3) ?>" type="text" name="phone">
                                    </div>
                                    <div class="a-form-row__valid a-form-row__valid-phone">

                                    </div>
                                </div>
                                <div class="a-form-row">
                                    <div class="a-form-row__lb">
                                        <label class="a-form-lb">E-mail *  </label>
                                    </div>
                                    <div class="a-form-row__fild">
                                        <input class="a-input" id="email" value="<?= $email ?>" type="email" name="email">
                                    </div>
                                    <div class="a-form-row__valid a-form-row__valid-email">

                                    </div>
                                </div>
                                <div class="a-form-row">
                                    <div class="a-form-row__lb">
                                        <label class="a-form-lb">Мессенджеры (Опционально)</label>
                                    </div>
                                    <div class="a-form-row__fild">
                                        <? if (!empty($t_admin)): ?>
                                            <button class="a-btn unauth">Отвязать Telegram</button>
                                        <? else: ?>
                                            <script async src="https://telegram.org/js/telegram-widget.js?5" data-telegram-login="inspiratura_bot" data-userpic="false" data-size="medium" data-radius="1" data-onauth="onTelegramAuth(user)" data-request-access="write"></script>
                                        <? endif; ?>
                                    </div>
                                </div>
                                <div class="a-form-row a-form-row--bot">
                                    <div class="a-form-row__btn">
                                        <button class="a-btn a-btn--green" id="save_profile">Сохранить</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    <? endif; ?>
                </div>
            </main>
        </div>
        <script src="js/vendor.js?v=1557472175815"></script>
        <script src="js/main.js?v=1557472175815"></script>
    </body>
</html>
