<!DOCTYPE html>
<html class="page-index" lang="ru">
    <head>
        <? include_once(F_PATH_SYS."tpl/inc/html_head_tags.tpl.php") ?> 
        <link rel="stylesheet" href="/css/interface.css?v=1557472175815">
        <link rel="stylesheet" href="/js/datetimepicker/jquery.datetimepicker.css" type="text/css" />
        <script type="text/javascript" src="/js/common.js"></script>
        <script>
            $(document).ready(function () {
                $("#start_date").datetimepicker({
                    i18n: {
                        en: {
                            months: [
                                'Январь', 'Февраль', 'Март', 'Апрель',
                                'Май', 'Июнь', 'Июль', 'Август',
                                'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь',
                            ],
                            dayOfWeek: [
                                "Вс", "Пн", "Вт", "Ср",
                                "Чт", "Пт", "Сб",
                            ]
                        }
                    },
                    timepicker: true,
                    format: "d.m.Y H:i"
                });
                $("body").on("click", "#translate", function (e) {
                    data = $("#group").serialize();
                    $.post("/_ajax.php", {
                        token: $('#f_token').attr("content"),
                        file: 'translate',
                        data: data
                    }, function (r) {
                        try {
                            result = jQuery.parseJSON(r);
                        } catch (err) {
                            $.sweetModal({
                                content: 'Unexpected errors: ' + r,
                                icon: $.sweetModal.ICON_ERROR
                            });
                            return false;
                        }
                        if (result.status == "success") {
                            $('.eng_text').each(function (i, elem) {
                                if (!$(this).parent().parent().parent().parent().hasClass("block_html")) {
                                    //alert(i + ': FOUND' + $(elem).val());
                                    $(elem).val(result.data.text[i]);
                                }
                            });
                        } else {
                            $.sweetModal({
                                content: result.mes,
                                icon: $.sweetModal.ICON_ERROR
                            });
                        }
                    });
                });
                $("#add_more_blocks").on("click", function (e) {
                    e.preventDefault();
                    html = $(".block_html").html();
                    $("#blocks").append(html);
                });
                $("#blocks").on("click", ".a-form-valid--error", function (e) {
                    e.preventDefault();
                    $(this).parent().parent().parent().remove();
                });
            });
        </script>
        <style>
            .green-link{
                border-bottom: 1px dashed #5bb646;
                color: #5bb646;
                text-decoration: none;
            }
            .green-link:hover{
                border: none;
                color: #4da339;
                text-decoration: none;
            }
            .rus_text{
                width: 49%;
            }
            .eng_text {
                width: 50%;
            }
        </style>
    </head>
    <body>
        <div class="a-page">
            <nav class="a-main-nav a-page__menu">
                <div class="a-main-nav__tit">Confpulse</div>
                <div class="a-main-nav__menu">
                    <ul class="a-main-nav__list">
                        <li class="a-main-nav__list-item"><a href="/groups">Главная</a></li>
                        <li class="a-main-nav__list-item is-active"><a href="#">Новая группа</a></li>
                    </ul>
                    <ul class="a-main-nav__list">
                        <li class="a-main-nav__list-item"><a href="/#examples">как это работает</a></li>
                        <li class="a-main-nav__list-item"><a href="/#about">о проекте </a></li>
                        <li class="a-main-nav__list-item"><a href="/#questions">контакты </a></li>
                    </ul>
                </div>
                <ul class="a-share-nav">
                    <li class="a-share-nav__item"><a class="icon-facebook" href="#"></a></li>
                    <li class="a-share-nav__item"><a class="icon-youtubel" href="#"></a></li>
                    <li class="a-share-nav__item"><a class="icon-instagram" href="#"></a></li>
                </ul>
            </nav>
            <? include_once(F_PATH_SYS."tpl/inc/header_interface.tpl.php") ?>
            <main class="a-body a-page__main">
                <div class="a-center">
                    <div class="a-crumbs"><a href="/groups">Inspiratura<span class="icon"><span></span></span></a><span>Новая группа</span></div>
                    <form id="group" method="post">
                        <div class="a-form">
                            <h2 class="a-tit">Новая группа</h2>
                            <div class="a-form-row">
                                <div class="a-form-row__btn">
                                    <a id="translate" class="green-link" href="#js">Перевести</a>
                                </div>
                            </div>
                            <div class="a-form-row">
                                <div class="a-form-row__lb">
                                    <label class="a-form-lb">Название группы</label>
                                </div>
                                <div class="a-form-row__fild">
                                    <input id="name" value="" type="text" name="name" class="a-input" />
                                </div>
                            </div>
                            <div class="a-form-row">
                                <div class="a-form-row__lb">
                                    <label class="a-form-lb">Доступен всем для копирования</label>
                                </div>
                                <label class="a-form-lb">
                                    <div class="jq-checkbox a-checkbox js-input"><input class="a-checkbox js-input" type="checkbox" name="opened" value="1"><div class="jq-checkbox__div"></div></div>
                                </label>
                            </div>
                            <div class="a-form-row">
                                <div class="a-form-row__lb">
                                    <label class="a-form-lb">Включить по расписанию</label>
                                </div>
                                <div class="a-form-row__fild">
                                    <input class="a-input" id="start_date" value="" type="text" name="start_date" autocomplete="off">
                                </div>
                            </div>
                            <div id="blocks">
                                <div>
                                    <div class="a-form-row a-form-row--va-top">
                                        <div class="a-form-row__lb">
                                            <label class="a-form-lb">Слово/Фраза(рус-англ) * </label>
                                        </div>
                                        <div class="a-form-row__fild">
                                            <textarea class="a-textarea rus_text" name="words[]"></textarea>
                                            <textarea class="a-textarea eng_text" name="translate[]"></textarea>
                                        </div>
                                    </div>
                                    <hr/>
                                </div>
                            </div>
                            <div class="a-form-row">
                                <div class="a-form-row__btn">
                                    <a id="add_more_blocks" class="green-link" href="#js">Добавить еще</a>
                                </div>
                            </div>
                            <div class="a-form-row a-form-row--bot">
                                <div class="a-form-row__btn">
                                    <input type="button" class="a-btn a-btn--green add_group" value="Сохранить"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="block_html" style="display:none">
                    <div>
                        <div class="a-form-row a-form-row--va-top">
                            <div class="a-form-row__lb">
                                <label class="a-form-lb">Слово/Фраза(рус-англ) * </label>
                            </div>
                            <div class="a-form-row__fild">
                                <textarea class="a-textarea rus_text" name="words[]"></textarea>
                                <textarea class="a-textarea eng_text" name="translate[]"></textarea>
                            </div>
                            <div class="a-form-row__valid" style="cursor: pointer">
                                <div class="a-form-valid a-form-valid--error">Удалить</div>
                            </div>
                        </div>

                        <hr/>
                    </div>
                </div>
            </main>
        </div>
        <script src="js/vendor.js?v=1557472175815"></script>
        <script src="js/main.js?v=1557472175815"></script>
    </body>
</html>