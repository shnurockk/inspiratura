<!DOCTYPE html>
<html class="page-index" lang="ru">
    <head>
        <? include_once(F_PATH_SYS."tpl/inc/html_head_tags.tpl.php") ?> 
        <link rel="stylesheet" href="/css/interface.css?v=1557472175815">
        <script type="text/javascript" src="/css//bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="/js/common.js"></script>
        <script>
            $(document).ready(function () {
                $("#name").focus();
                $(".find_course").on("click", function (e) {
                    e.preventDefault();
                    find = $("#name").val();
                    author = $("#author").val();
                    cost_filter = $("#cost_filter").children("option:selected").val();
                    bought_filter = $("#bought_filter").children("option:selected").val();
                    var obj = {"find": find, "author": author, "cost_filter":cost_filter, "bought_filter":bought_filter};
                    var str = jQuery.param(obj);
                    var url = "/find_course?" + str;
                        window.open(url, '_self');

                });

                $('.slide').on('click', function () {
                    glyphicon = $(this).find('.glyphicon');
                    block = $(this).closest('.a-form').find('.block_content');
                    if(block.first().is(":hidden")){
                        block.slideDown();
                        glyphicon.removeClass('glyphicon-chevron-down');
                        glyphicon.addClass('glyphicon-chevron-up');
                    } else {
                        block.slideUp();
                        glyphicon.removeClass('glyphicon-chevron-up');
                        glyphicon.addClass('glyphicon-chevron-down');
                    }
                });
            });
        </script>
        <style>
            @media (min-width: 750px)
            {
                .a-signals-nav__item {
                    width: 25%;
                }

                .a-signals-nav__item .link{
                    height: 280px !important;
                }
            }

            @media (max-width: 750px) {
                .a-signals-nav__item {
                    width: 50%;

                }
                .a-signals-nav__item .link{
                    height: 300px;
                }
            }
            @media (max-width: 798px) {
                .a-btn {
                    min-width: 84px;
                }
            }

            .green-link{
                border-bottom: 1px dashed #5bb646;
                color: #5bb646;
                text-decoration: none;
            }
            .green-link:hover{
                border: none;
                color: #4da339;
                text-decoration: none;
            }
            .vyrovnyat {
                text-align: center;
            }
            .vyrovnyat div {
                display: inline-block;
                vertical-align: middle;
                color: #0b3e69 !important;
                font-size: 16px;
                line-height: 17px;
                font-weight: 700;
                word-wrap: break-word; /* Для остальных браузеров */
                word-break: break-word;
                width: 100%;
            }
            .vyrovnyat_description div {
                line-height: normal !important;
                color: #b3b8ca !important;
                font-size: 12px;
                margin: 5px;
            }
/*            .vyrovnyat:before {  для IE8+ 
                content: "";
                min-height: inherit;
                height: 100%;
                vertical-align: middle;
            }*/
            .a-signals-nav__item {
                width: 33%;
            }

            .a-signals-nav__item .link{
                height: 230px;
            }
            .bg-circle i {
                text-align: center;
                color: #fff;
                font-size: 30px;
                padding: 15px;
                border-radius: 30px;
            }
            .bg-green i {
                background: #43f590;
            }
            .bg-yellow i {
                background: #f5cb00;
            }
            .bg-red i {
                background: #f54347;
            }
            .course-image-style{
                background-repeat: no-repeat !important;
                background-size:cover !important;
                color: #6c7289;
                height: 100%;
                width: 100%;
            }

            .course-image-text-style{
                color: #909090;
            }

            .a-form-lb2{
                display: block;
                font-size: 15px;
                line-height: 17px;
                color: #272727;
                text-align: start;
            }

            .slide{
                cursor: pointer;
            }

            .input-tool{
                display: inline-block;
                position: absolute;
                width: 53px;
                text-align: center;
                left: calc(100% - 66px);
                z-index: 5;
                padding: 5px 12px;
                margin: 4px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                font-family: Roboto, sans-serif;
                font-size: 20px;
                font-weight: 400;
                line-height: 32px;
                color: #949494;
                top: 0;
            }
        </style>
    </head>
    <body>
        <div class="a-page">
            <nav class="a-main-nav a-page__menu">
                <div class="a-main-nav__tit">Confpulse</div>
                <div class="a-main-nav__menu">
                    <ul class="a-main-nav__list">
                        <li class="a-main-nav__list-item"><a href="/groups">Главная</a></li>
                    </ul>
                    <ul class="a-main-nav__list">
                        <li class="a-main-nav__list-item"><a href="/#examples">как это работает</a></li>
                        <li class="a-main-nav__list-item"><a href="/#about">о проекте </a></li>
                        <li class="a-main-nav__list-item"><a href="/#questions">контакты </a></li>
                        <li class="a-main-nav__list-item"><a href="/registration">регистрация</a></li>
                    </ul>
                </div>
                <ul class="a-share-nav">
                    <li class="a-share-nav__item"><a class="icon-facebook" href="#"></a></li>
                    <li class="a-share-nav__item"><a class="icon-youtubel" href="#"></a></li>
                    <li class="a-share-nav__item"><a class="icon-instagram" href="#"></a></li>
                </ul>
            </nav>
            <? include_once(F_PATH_SYS."tpl/inc/header_interface.tpl.php") ?>
            <main class="a-body a-page__main">
                <div class="a-center">
                    <div class="a-crumbs"><a href="/groups">Inspiratura<span class="icon"><span></span></span></a><span>Найти курс</span></div>
                    <form id="group" method="post">
                        <div class="a-form">
                            <div>
                                <h2 class="a-tit">Найти курс</h2>
                                <div class="a-form-row__fild" style="display: inline-block; width: 80%;">
                                      <input id="name" value="<?= $word ?>" type="text" class="a-input" style="padding-right: 67px; display: inline-block;"/>
                                      <label class="slide input-tool" ><i class="glyphicon glyphicon-chevron-down"></i></label>
                                </div>
                                <button type="submit" class="a-btn a-btn--green find_course" style="display: inline-block;">Найти</button>
                            </div>
                            <div style="margin-top: -4px;">
                                <div class="block_content" style="background: #cccbcb;width: 80%;height: auto;-moz-border-radius: 0 0 5px 5px;border-radius: 0 0 5px 5px;display: none;">
                                    <div class="a-form-row" style="padding: 10px;">
                                        <div class="a-form-row__fild" style="width: 100%;">
                                            <label class="a-form-lb2">Автор</label>
                                            <input id="author" value="<?= $author ?>" type="text" class="a-input" style="width: 100%"/>
                                        </div>
                                    </div>
                                    <div class="a-form-row" style="padding: 10px">
                                        <div class="a-form-row__fild" style="width: 100%;">
                                            <label class="a-form-lb2">Отображать</label>
                                            <select id="bought_filter" class="b-select js-select" data-placeholder=" " name="type" style="width: 100%">
                                                <option value="0" <?= empty($bought_filter) ? 'selected' : 0?>>Все</option>
                                                <option value="1" <?= $bought_filter == 1 ? 'selected' : 0?>>Только не приобретённые</option>
                                                <option value="2" <?= $bought_filter == 2 ? 'selected' : 0?>>Только приобретённые</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="a-form-row dep_filter_bought" style="padding: 10px">
                                        <div class="a-form-row__fild" style="width: 100%;">
                                            <label class="a-form-lb2">Цена</label>
                                            <select id="cost_filter" class="b-select js-select" data-placeholder=" " name="type" style="width: 100%">
                                                <option value="0" >Выберите тип сортировки</option>
                                                <option value="1" <?= $cost_filter == 1 ? 'selected' : 0?>>По возрастанию</option>
                                                <option value="2" <?= $cost_filter == 2 ? 'selected' : 0?>>По убыванию</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <nav class="a-signals-nav cf">
                        <ul>
                            <? foreach ($finded as $f): ?>
                                <li class="a-signals-nav__item">
                                    <a class="link link--bitnex" style="text-decoration: none;padding: 5px;" href="/course_view?group_id=<?= $f['id']?>">
                                        <div class="vyrovnyat" style="width:100%;height:55%;">
                                            <div class="course-image-style" style="background: url('<?= $f["img"] ?>')">
                                                <? if(empty($f["img"])): ?>
                                                    <div class="bg-circle bg-yellow">
                                                        <i class="glyphicon glyphicon-search"></i>
                                                    </div>
                                                <? endif; ?>
                                            </div>
                                        </div>
                                        <div class="vyrovnyat" style="width:100%;height:33%;padding: 5px;">
                                            <div><?= $f["name"] ?></div>
                                        </div>
                                        <div class="vyrovnyat_description" style="width:100%;height:35%;">
                                            <div>Цена: <?= $f["cost"] ?></div>
                                        </div>
                                    </a>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    </nav>
                </div>

            </main>
        </div>
        <script src="js/vendor.js?v=1557472175815"></script>
        <script src="js/main.js?v=1557472175815"></script>
    </body>
</html>