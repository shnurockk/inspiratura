<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));


//Ошибка при многомерной ссылке

if (isset($F_URL) and count($F_URL) > 1)
    die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\Fianta;
use Fianta\Core\Converter;
use Fianta\Core\DB;

$F_PAGE_GEN['title'] = "Восстановление пароля";

$F_PAGE_GEN['description'] = "BTC";

$F_PAGE_GEN['keywords'] = "BTC";

$F_PAGE_GEN['robots'] = 'none';


if (isset($_GET["secret"])) {
    $uid = filter_input(INPUT_GET, 'uid', FILTER_VALIDATE_INT);
    $secret = filter_input(INPUT_GET, 'secret', FILTER_DEFAULT);
    $e_q = DB::con()->query("SELECT `fio`,`id`,`updated`,`salt` FROM `".F_DB_PREFIX."users` WHERE `pass`=".DB::quote(trim($secret))) or die(Fianta::err(__FILE__, __LINE__));
    $ids = $e_q->fetch(PDO::FETCH_ASSOC);
    if (empty($ids)) {
        echo "Ошибка! Пользователь не найден!";
    } else {
        $uid = $ids["id"];
        $updated = $ids["updated"];
        $fio = $ids["fio"];
        $salt = $ids["salt"];
        $now = date("Y-m-d H:i:s");
        $unow = Converter::toUnixDate($now);
        $check_sec = $unow - $updated;
        if ($check_sec >= 3600) {
            echo "Ошибка! Ссылка просрочена!";
        } else {
            include_once(F_PATH_SYS."tpl/remind_password_form.tpl.php");
        }
    }
} else {
    include_once(F_PATH_SYS."tpl/inc/remind_password.tpl.php");
}
