<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\User;
use Fianta\Sys\UserInfo;

//Ошибка при многомерной ссылке

if (isset($F_URL) and count($F_URL) > 1)
    die(include_once(F_PATH_SYS.'pages/404.php'));
$F_PAGE_GEN['title'] = "Профиль";

$F_PAGE_GEN['description'] = "BTC";

$F_PAGE_GEN['keywords'] = "BTC";

$F_PAGE_GEN['robots'] = 'none';
if (F_LOGGED) {

    $uid = User::get()->id;

    $uinfo = UserInfo::getbyID($uid);
    $pcode = $uinfo->pcode;
    $email = $uinfo->email;
    $email2 = $uinfo->email2;
    $fio = $uinfo->fio;
    $phone = $uinfo->login;
    $t_uid = $uinfo->t_uid;
    $t_name = $uinfo->t_name;
    $role = $uinfo->role_id;
    $confirm_email = $uinfo->confrim_email;

    $conf = new \Fianta\Sys\Insp();
    $t_admin = $conf->getTelegram($uid);

    //Подключаем шаблон
    include_once(F_PATH_SYS."tpl/profile_edit.tpl.php");
} else {
    include_once(F_PATH_SYS."tpl/inc/login.tpl.php");
}