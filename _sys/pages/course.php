<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\User;
use Fianta\Sys\Insp;
use Fianta\Sys\UserInfo;

//Ошибка при многомерной ссылке
if (isset($F_URL) and count($F_URL) > 1)
    die(include_once(F_PATH_SYS.'pages/404.php'));

$F_PAGE_GEN['title'] = "Изучение курса";

$F_PAGE_GEN['description'] = "";

$F_PAGE_GEN['keywords'] = "";

$F_PAGE_GEN['robots'] = 'none';

if (F_LOGGED) {
    $insp = new Insp();

    $uid = User::get()->id;
    $id = filter_input(INPUT_GET, 'id');

    $course = $insp->getUserCourse($uid, $id);
    $progressCount = $insp->getCourseAllProgressCount($uid, $id);
    $progress = $insp->getCourseAllProgress($uid, $id);

    if(empty($course)){
        die(include_once(F_PATH_SYS.'pages/404.php'));
    }

    if(isset($_SESSION['COURSE_'.$id]['LAST'])) {
        $last_view = $_SESSION['COURSE_'.$id]['LAST'];
    }
    //die(print_r($last_view, true));

    $total_sections = count($course['sections']);
    $total_blocks = 0;
    foreach ($course['sections'] as $s){
        $total_blocks += count($s['blocks']);
    }

    $block_count = 0;
    include_once(F_PATH_SYS."tpl/course.tpl.php");
} else {
    include_once(F_PATH_SYS."tpl/inc/login.tpl.php");
}