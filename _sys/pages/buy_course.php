<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));


$response = file_get_contents('php://input');
$date = date("d.m.Y H:i:s");
if (!empty($response)) {
    $data = json_decode($response, true);
    $buy_info = explode("_", $data["order_id"]);
    $query = \Fianta\Core\DB::con()->query('INSERT INTO `'.F_DB_PREFIX.'users_course_bought` (uid,course_id,boughtdate) VALUES ('.\Fianta\Core\DB::quote($buy_info[0]).','.\Fianta\Core\DB::quote($buy_info[1]).','.strtotime($data["date"]).')');
    $str = print_r($data, TRUE);
    file_put_contents(F_PATH_SYS."course_buys_log.log", $date.": Успешная покупка:\n".$str."\n", FILE_APPEND);
} else {
    file_put_contents(F_PATH_SYS."course_buys_log.log", $date.": Нет данных о покупке\n", FILE_APPEND);
    die(include_once(F_PATH_SYS.'pages/404.php'));
}