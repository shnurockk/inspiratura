<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));


//Ошибка при многомерной ссылке

if (isset($F_URL) and count($F_URL) > 1)
    die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\Fianta;
use Fianta\Core\DB;

$F_PAGE_GEN['title'] = "Регистрация";

$F_PAGE_GEN['description'] = "BTC";

$F_PAGE_GEN['keywords'] = "BTC";

$F_PAGE_GEN['robots'] = 'none';

if (!isset($_GET["confrim_email"]) and ! isset($_GET["confrim_new_email"])) {
    $phone = "";
    $email = "";
    $name = "";

    $client_id_fb = '309273509704456'; // Client ID
    $client_secret_fb = '76e5925898946b93fb9ff4081ca953eb'; // Client secret
    $redirect_uri_fb = 'https://youneedcare.com/registration'; // Redirect URIs

    $url_fb = 'https://www.facebook.com/dialog/oauth';

    $params_fb = array(
        'client_id' => $client_id_fb,
        'redirect_uri' => $redirect_uri_fb,
        'response_type' => 'code',
        'scope' => 'email'
    );

    $link_fb = '<a href="'.$url_fb.'?'.urldecode(http_build_query($params_fb)).'"><img class="auth_icon" src="css/images/fb_auth.png"/></a>';

    if (isset($_GET['code']) and ! isset($_GET['scope'])) {
        $result_fb = false;

        $params_fb = array(
            'client_id' => $client_id_fb,
            'redirect_uri' => $redirect_uri_fb,
            'client_secret' => $client_secret_fb,
            'code' => $_GET['code']
        );

        $url_fb = 'https://graph.facebook.com/oauth/access_token';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url_fb);
        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params_fb)));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $res = curl_exec($curl);
        curl_close($curl);

        $tokenInfo = json_decode($res, true);

        if (is_array($tokenInfo) && count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
            $params = array('access_token' => $tokenInfo['access_token'], 'fields' => "email,name");

            $userInfo = json_decode(file_get_contents('https://graph.facebook.com/me'.'?'.urldecode(http_build_query($params))), true);

            if (isset($userInfo['id'])) {
                $userInfo = $userInfo;
                $result_fb = true;
            }

            if ($result_fb) {
                $name = $userInfo["name"];
                $email = $userInfo["email"];
            }
        }
    }

    $client_id = '1047617067762-t9kbp9oduav3072rn02p9i3vi7oobucv.apps.googleusercontent.com'; // Client ID
    $client_secret = 'l5F_uw0kxTOUJCBUegts0Vst'; // Client secret
    $redirect_uri = 'https://youneedcare.com/registration'; // Redirect URIs

    $url = 'https://accounts.google.com/o/oauth2/auth';

    $params = array(
        'redirect_uri' => $redirect_uri,
        'response_type' => 'code',
        'client_id' => $client_id,
        'scope' => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
    );

    $link_google = '<a href="'.$url.'?'.urldecode(http_build_query($params)).'"><img class="auth_icon" src="css/images/google_auth.png"/></a>';

    if (isset($_GET['code']) and isset($_GET['scope']) and mb_stripos($_GET['scope'], 'www.googleapis.com') !== false) {
        $result = false;

        $params = array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'redirect_uri' => $redirect_uri,
            'grant_type' => 'authorization_code',
            'code' => $_GET['code']
        );

        $url = 'https://accounts.google.com/o/oauth2/token';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);
        $tokenInfo = json_decode($result, true);

        if (isset($tokenInfo['access_token'])) {
            $params['access_token'] = $tokenInfo['access_token'];

            $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo'.'?'.urldecode(http_build_query($params))), true);
            if (isset($userInfo['id'])) {
                $userInfo = $userInfo;
                $result = true;
            }
        }

        if ($result and ! isset($tokenInfo["error"])) {
//        echo "<pre>";
//        print_r($tokenInfo);
//        echo "</pre>";
            $name = $userInfo["name"];
            $email = $userInfo["email"];
        }
    }


//Подключаем шаблон

    include_once(F_PATH_SYS."tpl/registration.tpl.php");
} else if (isset($_GET["confrim_email"])) {
    $uid = filter_input(INPUT_GET, 'uid', FILTER_DEFAULT);
    $confrim_email_q = DB::con()->query("SELECT `confrim_email` FROM `".F_DB_PREFIX."users` WHERE `id`=".DB::quote($uid)) or die(Fianta::err(__FILE__, __LINE__));
    $confrim_email = $confrim_email_q->fetch(PDO::FETCH_ASSOC);
    $confrim_email = $confrim_email["confrim_email"];
    $link_confrim = $_GET["confrim_email"];
    if ($confrim_email != 0 and $link_confrim == $confrim_email) {
        DB::con()->query("UPDATE `".F_DB_PREFIX."users` SET `confrim_email`=0 WHERE `id`=".DB::quote($uid)) or die(Fianta::err(__FILE__, __LINE__));
        unset($_SESSION["user"]);
        echo '<script>window.onload = function(){window.setTimeout(function() {
          window.location.href = "/welcome";
        }, 3000)};</script>';
        echo "Почта успешно подтверждена!<br/>";
        echo "Через 3 секунды вы будете перенаправленны!";
    } elseif ($confrim_email == 0) {
        echo '<script>window.onload = function(){window.setTimeout(function() {
          window.location.href = "/welcome";
        }, 3000)};</script>';
        echo "Почта уже была подтверждена!<br/>";
        echo "Через 3 секунды вы будете перенаправленны!";
    } elseif ($link_confrim != $confrim_email) {
        echo "Ошибка! Повторите попытку отправив письмо еще раз!";
    }
} else if (isset($_GET["confrim_new_email"])) {
    $uid = filter_input(INPUT_GET, 'uid', FILTER_DEFAULT);
    $confrim_email_q = DB::con()->query("SELECT `email2`,`updated` FROM `".F_DB_PREFIX."users` WHERE `id`=".DB::quote($uid)) or die(Fianta::err(__FILE__, __LINE__));
    $confrim_email = $confrim_email_q->fetch(PDO::FETCH_ASSOC);
    $email2 = $confrim_email["email2"];
    $updated = $confrim_email["updated"];
    $link_updated = $_GET["confrim_new_email"];
    if (!empty($email2) and $link_updated == $updated) {
        DB::con()->query("UPDATE `".F_DB_PREFIX."users` SET `email`=".DB::quote($email2).",`email2`='' WHERE `id`=".DB::quote($uid)) or die(Fianta::err(__FILE__, __LINE__));
        echo '<script>window.onload = function(){window.setTimeout(function() {
          window.location.href = "/welcome";
        }, 3000)};</script>';
        echo "Почта успешно подтверждена!<br/>";
        echo "Через 3 секунды вы будете перенаправленны!";
    } elseif (empty($email2)) {
        echo '<script>window.onload = function(){window.setTimeout(function() {
          window.location.href = "/welcome";
        }, 3000)};</script>';
        echo "Почта уже была подтверждена!<br/>";
        echo "Через 3 секунды вы будете перенаправленны!";
    } elseif ($link_updated != $updated) {
        echo "Ошибка! Повторите попытку отправив письмо еще раз!";
    }
}