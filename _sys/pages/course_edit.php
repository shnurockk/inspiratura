<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\User;
use Fianta\Sys\Insp;
use Fianta\Sys\UserInfo;

//Ошибка при многомерной ссылке
if (isset($F_URL) and count($F_URL) > 1)
    die(include_once(F_PATH_SYS.'pages/404.php'));

$F_PAGE_GEN['title'] = "Редактирование курса";

$F_PAGE_GEN['description'] = "";

$F_PAGE_GEN['keywords'] = "";

$F_PAGE_GEN['robots'] = 'none';

$F_PAGE_GEN['return_page'] = '/groups';

$F_PAGE_GEN['return_page_desc'] = 'НАЗАД К КУРСАМ';

if (F_LOGGED) {
    $insp = new Insp();

    $stage = filter_input(INPUT_GET, 'stage');
    $r = filter_input(INPUT_GET, 'r');
    $group_id = filter_input(INPUT_GET, 'group_id');
    $clean = filter_input(INPUT_GET, 'clean');

    $uid = User::get()->id;

    $course = [];
    if(!empty($group_id)) {
        if($clean == true){
            unset($_SESSION['course'][$group_id]);
        }

        if(!isset($_SESSION['course'][$group_id])) {
            $course = $insp->getCourses($uid, $group_id, ['all']);
            $course = reset($course);

            if (!empty($course)) {
                $_SESSION['course'][$group_id] = $course;
            }
        }
        else{
            $course = $_SESSION['course'][$group_id];
        }
    }
    else{
            die(include_once(F_PATH_SYS.'pages/404.php'));
    }
    $uinfo = UserInfo::getbyID($uid);
    $pcode = $uinfo->pcode;
    $email = $uinfo->email;
    $email2 = $uinfo->email2;
    $fio = $uinfo->fio;
    $phone = $uinfo->login;
    $t_uid = $uinfo->t_uid;
    $t_name = $uinfo->t_name;
    $role = $uinfo->role_id;
    $confirm_email = $uinfo->confrim_email;

    if($stage == 'cost') {
        include_once(F_PATH_SYS."tpl/teacher/curse_constructor/cost.tpl.php");
    }
    else if($stage == 'section'){
        include_once(F_PATH_SYS."tpl/teacher/curse_constructor/section.tpl.php");
    }
    else{
        include_once(F_PATH_SYS."tpl/teacher/curse_constructor/info.tpl.php");
    }
}
else{
    include_once(F_PATH_SYS."tpl/inc/login.tpl.php");
}