<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\User;
use Fianta\Sys\Insp;

//Ошибка при многомерной ссылке

if (isset($F_URL) and count($F_URL) > 1)
    die(include_once(F_PATH_SYS.'pages/404.php'));


//Заполняем переменные шаблона

$F_PAGE_GEN['title'] = "Найти курс";

$F_PAGE_GEN['description'] = "Confpulse";

$F_PAGE_GEN['keywords'] = "Confpulse";

$F_PAGE_GEN['robots'] = 'none';

if (F_LOGGED) {
    $uid = User::get()->id;
    $word = filter_input(INPUT_GET, 'find');
    $author = filter_input(INPUT_GET, 'author');
    $cost_filter = filter_input(INPUT_GET, 'cost_filter');
    $bought_filter = filter_input(INPUT_GET, 'bought_filter');
    if(!empty($cost_filter) && !isset($bought_filter)){
        $bought_filter = 0;
    }
    $insp = new Insp();

    $finded = $insp->findCourse($word, $author, $cost_filter, $bought_filter);
    //Подключаем шаблон

    include_once(F_PATH_SYS."tpl/find_course.tpl.php");
} else {
    include_once(F_PATH_SYS."tpl/inc/login.tpl.php");
}