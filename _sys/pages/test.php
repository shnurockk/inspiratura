<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\Email;
use Fianta\Core\Fianta;
use Fianta\Core\DB;
use Fianta\Core\User;
use Fianta\Sys\Insp;
use Fianta\Sys\UserInfo;
use Telegram\Bot\Api;
use Viber\Client;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Fianta\Sys\Emotions;
use Fianta\Sys\FaceID;
use Char0n\FFMpegPHP\Movie;

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

echo "<pre>";

$response = file_get_contents('php://input');
if (empty($response)) {
    echo strtotime("2020-04-01T17:26:17.5875256+03:00");
} else {
    $data = json_decode($response, true);

    file_put_contents(F_PATH_SYS."test.txt", $data, FILE_APPEND);
}