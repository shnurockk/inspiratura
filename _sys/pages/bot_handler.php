<?php

use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Fianta\Sys\Insp;

$telegram = new Api('967935120:AAGM-cl20ZeesA8qZMXd251zldA5eAkGpA8'); //Устанавливаем токен, полученный у BotFather
$result = $telegram->getWebhookUpdates(); //Передаем в переменную $result полную информацию о сообщении пользователя
//if ($result["callback_query"]["from"]["id"] == 255752568) {
//    ob_start();
//    print_r($result["callback_query"]);
//    $ss = ob_get_clean();
//    $telegram->sendMessage(['chat_id' => 255752568, 'text' => $ss]);
//    exit();
//}
if (isset($result["message"])) {
    $mes_id = $result["message"]["message_id"];
    if (isset($result["message"]["text"])) {
        $text = $result["message"]["text"]; //Текст сообщения
    }
    $chat_id = $result["message"]["chat"]["id"]; //Уникальный идентификатор пользователя
    $name = $result["message"]["from"]["first_name"]; //Юзернейм пользователя
    if (isset($result["message"]["reply_to_message"]) and ! empty($result["message"]["reply_to_message"])) {
        $reply_text = $result["message"]["reply_to_message"]["text"];
    } else {
        $reply_text = '';
    }
    if (isset($result["message"]["contact"]["phone_number"])) {
        $phone = $result["message"]["contact"]["phone_number"];
    }
    if (isset($result["message"]["photo"])) {
        foreach ($result["message"]["photo"] as $p) {
            $photo = $p;
        }
        $photo_caption = "";
        if (isset($result["message"]["caption"])) {
            $photo_caption = $result["message"]["caption"];
        }
    }
} elseif (isset($result["callback_query"])) {
    $mes_id = $result["callback_query"]["message"]["message_id"];
    $text = $result["callback_query"]["message"]["text"]; //Текст сообщения
    $chat_id = $result["callback_query"]["from"]["id"]; //Уникальный идентификатор пользователя
    $name = $result["callback_query"]["from"]["first_name"]; //Юзернейм пользователя
    $callback_data = $result["callback_query"]["data"]; //Данные подписанные в нажатой кнопке
    $callback_id = $result["callback_query"]["id"];
}
$conf = new Insp();
if (isset($text) and ! isset($callback_data)) {

    if ($text == "/start") {
        $reply = "Добро пожаловать! Для начала авторизируйтесь используя кнопку ниже.";

        $keyboard[0][0]["text"] = "Авторизация";
        $keyboard[0][0]["request_contact"] = true;

        $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'one_time_keyboard' => true, 'resize_keyboard' => true]);
        $telegram->sendMessage(['chat_id' => $chat_id, 'text' => $reply, 'reply_markup' => $reply_markup]);
        $keyboard = [];
        $keyboard[0][0]["text"] = "Начать урок";
        $keyboard[0][0]["callback_data"] = "start_lesson_31";
        $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
        $telegram->sendMessage(['chat_id' => $chat_id, 'reply_markup' => $reply_markup, 'text' => "Или же попробуйте пройти тестовый урок!"]);
        exit();
    }
    if ($text != "/start") {
        $user = $conf->checkUser($chat_id);
        if ($user) {
            //$telegram->sendMessage(['chat_id' => $chat_id, 'text' => "При отправке мне сообщений будет обработка команд или продолжение урока(повторно отправляем слово с вариантами ответов)"]);
            $keyboard[0][0]["text"] = "Начать урок";
            $keyboard[0][0]["callback_data"] = "start_lessons";
            $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
            $telegram->sendMessage(['chat_id' => $chat_id, 'reply_markup' => $reply_markup, 'text' => "Можно начать урок"]);
            exit();
        } else {
            $reply = "Добро пожаловать! Для начала авторизируйтесь используя кнопку ниже.";

            $keyboard[0][0]["text"] = "Авторизация";
            $keyboard[0][0]["request_contact"] = true;

            $reply_markup = $telegram->replyKeyboardMarkup(['keyboard' => $keyboard, 'one_time_keyboard' => true, 'resize_keyboard' => true]);
            $telegram->sendMessage(['chat_id' => $chat_id, 'text' => $reply, 'reply_markup' => $reply_markup]);
        }
    }
}
if (isset($phone)) {
    $user = $conf->checkUser($chat_id);
    $reply_markup = $telegram->replyKeyboardHide();
    if ($user) {
        $conf->updateTelegram($phone, $chat_id);
        $reply_markup = $telegram->replyKeyboardHide();
        $telegram->sendMessage(['chat_id' => $chat_id, 'reply_markup' => $reply_markup, 'text' => "Поздравляю, Вы авторизированы!"]);
        $keyboard[0][0]["text"] = "Начать урок";
        $keyboard[0][0]["callback_data"] = "start_lessons";
        $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
        $telegram->sendMessage(['chat_id' => $chat_id, 'reply_markup' => $reply_markup, 'text' => "Можно начать урок"]);
    } else {
        $conf->addUser($phone, $chat_id, $name);
        $reply_markup = $telegram->replyKeyboardHide();
        $reply = "Поздравляю, Вы авторизированы! Для старта урока нужно связать телеграм и вебсервис (https://inspiratura.com/profile_telegram_edit)";
        $telegram->sendMessage(['chat_id' => $chat_id, 'reply_markup' => $reply_markup, 'text' => $reply]);
        //$telegram->sendMessage(['chat_id' => $chat_id, 'reply_markup' => $reply_markup, 'text' => "Успешно авторизировались! Для продолжения введите код подключения.", 'parse_mode' => 'Markdown']);
    }
}

if (isset($callback_data)) {
    if (mb_stripos($callback_data, 'start_lessons') !== false) {
        $groups = $conf->getGroupByTUser($chat_id);
        $text = "Выберите урок(группу слов):\n";
        if (!empty($groups)) {
            foreach ($groups as $i => $b) {
                $keyboard[$i][0]["text"] = $b["name"];
                $keyboard[$i][0]["callback_data"] = "start_lesson_".$b["id"];
            }
        } else {
            $text .= "Нет доступных личных уроков. Добавить можно перейдя по ссылке:\nhttps://inspiratura.com/groups";
        }

        $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
        $updating_prams = [
            'chat_id' => $chat_id,
            'text' => $text,
            'reply_markup' => $reply_markup,
            'parse_mode' => 'Markdown'
        ];
        $telegram->sendMessage($updating_prams);
        $shared_groups = $conf->getSharedGroupByTUser($chat_id);
        $text = "Интересующие вас уроки:\n";
        if (!empty($shared_groups)) {
            $keyboard = [];
            foreach ($shared_groups as $i => $b) {
                $keyboard[$i][0]["text"] = $b["name"];
                $keyboard[$i][0]["callback_data"] = "start_lesson_".$b["id"];
            }
        } else {
            $text .= "Нет доступных интересующих вас уроков. Уроки доступны по ссылкам-приглашениям.";
        }
        $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
        $updating_prams = [
            'chat_id' => $chat_id,
            'text' => $text,
            'reply_markup' => $reply_markup,
            'parse_mode' => 'Markdown'
        ];
        $telegram->sendMessage($updating_prams);
        exit();
    }
    if (mb_stripos($callback_data, 'start_lesson_') !== false) {
        $group_id = str_replace('start_lesson_', '', $callback_data);
        $stat_id = $conf->addNewStat($group_id, $chat_id);
        $conf->userGroupConnect($group_id, $stat_id, $chat_id);

        $g = $conf->getGroupById($group_id);
        $reply = "Выберите перевод слова/фразы:\n*".$g["words"][0]["word"]."*\n";
        $answers = $conf->getAnswers($g["words"], $g["words"][0]["translate"]);
        ksort($answers);
        $keyboard = [];
        foreach ($answers as $i => $b) {
            $n = $i + 1;
            $hash = hash('sha256', $b);
            $reply .= $n." - ".$b."\n";
            $keyboard[0][$i]["text"] = $n;
            $keyboard[0][$i]["callback_data"] = "a_0_".substr($hash, 0, 40);
        }
        $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
        $updating_prams = [
            'chat_id' => $chat_id,
            'message_id' => $mes_id,
            'text' => $reply,
            'reply_markup' => $reply_markup,
            'parse_mode' => 'Markdown'
        ];
        $telegram->editMessageText($updating_prams);
        exit();
    }
    if (mb_stripos($callback_data, 'repeat_lesson_') !== false) {
        $group_id = str_replace('repeat_lesson_', '', $callback_data);
        $shared = $conf->checkGroupForShared($group_id, $chat_id);
        $last_stat = $conf->setLastStat($chat_id, $group_id, $shared);
        $stat_id = $conf->addNewStat($group_id, $chat_id);
        $conf->userGroupConnect($group_id, $stat_id, $chat_id);
        $g = $conf->getGroupById($group_id);
        $ls_answers = $conf->getStatById($last_stat, true);
        $reply = "Выберите перевод слова/фразы(повтор):\n*".$ls_answers["stat"][0]["word"]."*\n";
        $answers = $conf->getAnswers($g["words"], $ls_answers["stat"][0]["translate"]);
        ksort($answers);
        $keyboard = [];
        foreach ($answers as $i => $b) {
            $n = $i + 1;
            $hash = hash('sha256', $b);
            $reply .= $n." - ".$b."\n";
            $keyboard[0][$i]["text"] = $n;
            $keyboard[0][$i]["callback_data"] = "r_0_".substr($hash, 0, 40);
        }
//        ob_start();
//        print_r($keyboard);
//        $r = ob_get_clean();
//        $telegram->sendMessage(['chat_id' => $chat_id, 'text' => $r]);
//        exit();
        $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
        $updating_prams = [
            'chat_id' => $chat_id,
            'message_id' => $mes_id,
            'text' => $reply,
            'reply_markup' => $reply_markup,
            'parse_mode' => 'Markdown'
        ];
        $telegram->editMessageText($updating_prams);
        exit();
    }
    if (mb_stripos($callback_data, 'a_') !== false) {
        $data = explode("_", $callback_data);
        $stage = $data[1] + 1;
        $answer_hash = $data[2];
        $user = $conf->checkUser($chat_id);
        $group_id = $user["group_id"];
        $stat_id = $user["stat_id"];
        $g = $conf->getGroupById($group_id);
        $right_answer_hash = hash('sha256', $g["words"][$data[1]]["translate"]);
        $right_answer_hash = substr($right_answer_hash, 0, 40);
        if ($right_answer_hash == $answer_hash) {
            $text = "Правильный ответ!";
            $conf->addStatAnswer($stat_id, $g["words"][$data[1]]["id"], 1);
        } else {
            $text = "Вы ошиблись. Вот правильный ответ:\n*".$g["words"][$data[1]]["translate"]."*\n";
            $conf->addStatAnswer($stat_id, $g["words"][$data[1]]["id"], 0);
        }
        if ($stage >= count($g["words"])) {
            $text .= "\nУрок завершен.";
            $conf->setStage(0, $chat_id);
            $conf->userGroupConnect(0, 0, $chat_id);
            unset($keyboard);
            $keyboard = [];
            $keyboard[0][0]["text"] = "Смотреть видео";
            $keyboard[0][0]["url"] = "https://youtu.be/ZWls3Zt9C2M?t=2569";
            $keyboard[1][0]["text"] = "Статистика";
            $keyboard[1][0]["url"] = "https://inspiratura.com/stat?id=".$stat_id;
            $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
            $updating_prams = [
                'chat_id' => $chat_id,
                'message_id' => $mes_id,
                'reply_markup' => $reply_markup,
                'text' => $text
            ];
            $telegram->editMessageText($updating_prams);
            unset($keyboard);
            $stat = $conf->getStatById($stat_id);
            if ($stat["good_answers"] < count($stat["stat"])) {
                $keyboard[0][0]["text"] = "1 час";
                $keyboard[0][0]["callback_data"] = "repeatlesson_1_".$group_id;
                $keyboard[0][1]["text"] = "6 часов";
                $keyboard[0][1]["callback_data"] = "repeatlesson_6_".$group_id;
                $keyboard[0][2]["text"] = "12 часов";
                $keyboard[0][2]["callback_data"] = "repeatlesson_12_".$group_id;
                $keyboard[0][3]["text"] = "24 часа";
                $keyboard[0][3]["callback_data"] = "repeatlesson_24_".$group_id;
                $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
                $telegram->sendMessage(['chat_id' => $chat_id, 'reply_markup' => $reply_markup, 'text' => "Не все ответы были правильными :(\nДавайте повторим урок! Правильные переводы спрашивать уже не буду...\nЧерез какое время повторим?"]);
            } else {
                $telegram->sendMessage(['chat_id' => $chat_id, 'text' => "Урок пройден на отлично! Чтоб начать новый - напишите мне любое сообщение."]);
            }
            exit();
        }
//        ob_start();
//        print_r($answer_hash);
//        print_r($right_answer_hash);
//        print_r($data[1]);
//        print_r($trans[$data[1]]);
//        $r = ob_get_clean();
//        $telegram->sendMessage(['chat_id' => $chat_id, 'text' => $r]);
//        exit();

        $keyboard[0][0]["text"] = "Смотреть видео";
        $keyboard[0][0]["url"] = "https://youtu.be/ZWls3Zt9C2M?t=2569";
        $keyboard[1][0]["text"] = "Учить дальше";
        $keyboard[1][0]["callback_data"] = "next_step_".$stage;
        $keyboard[2][0]["text"] = "Статистика";
        $keyboard[2][0]["url"] = "https://inspiratura.com/stat?id=".$stat_id;
        $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
        $updating_prams = [
            'chat_id' => $chat_id,
            'message_id' => $mes_id,
            'text' => $text,
            'reply_markup' => $reply_markup,
            'parse_mode' => 'Markdown'
        ];
        $telegram->editMessageText($updating_prams);
        exit();
    }
    if (mb_stripos($callback_data, 'r_') !== false) {
        $data = explode("_", $callback_data);
        $stage = $data[1] + 1;
        $answer_hash = $data[2];
        $user = $conf->checkUser($chat_id);
        $group_id = $user["group_id"];
        $stat_id = $user["stat_id"];
        $g = $conf->getGroupById($group_id);
        $shared = $conf->checkGroupForShared($group_id, $chat_id);
        if ($shared) {
            $last_stat = $conf->getSharedLastStat($chat_id, $group_id);
        } else {
            $last_stat = $g["last_stat"];
        }
        $ls_answers = $conf->getStatById($last_stat, true);
        $right_answer_hash = hash('sha256', $ls_answers["stat"][$data[1]]["translate"]);
        $right_answer_hash = substr($right_answer_hash, 0, 40);
        if ($right_answer_hash == $answer_hash) {
            $text = "Правильный ответ!(повтор)";
            $conf->addStatAnswer($stat_id, $ls_answers["stat"][$data[1]]["word_id"], 1);
        } else {
            $text = "Вы ошиблись(повтор). Вот правильный ответ:\n*".$g["words"][$data[1]]["translate"]."*\n";
            $conf->addStatAnswer($stat_id, $ls_answers["stat"][$data[1]]["word_id"], 0);
        }
        if ($stage >= count($ls_answers["stat"])) {
            $text .= "\nПовторение завершено.";
            $conf->setStage(0, $chat_id);
            $conf->userGroupConnect(0, 0, $chat_id);
            unset($keyboard);
            $keyboard = [];
            $keyboard[0][0]["text"] = "Смотреть видео";
            $keyboard[0][0]["url"] = "https://youtu.be/ZWls3Zt9C2M?t=2569";
            $keyboard[1][0]["text"] = "Статистика";
            $keyboard[1][0]["url"] = "https://inspiratura.com/stat?id=".$stat_id;
            $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
            $updating_prams = [
                'chat_id' => $chat_id,
                'message_id' => $mes_id,
                'reply_markup' => $reply_markup,
                'text' => $text
            ];
            $telegram->editMessageText($updating_prams);
            unset($keyboard);
            $stat = $conf->getStatById($stat_id);
            if ($stat["good_answers"] < count($stat["stat"])) {
                $keyboard[0][0]["text"] = "1 час";
                $keyboard[0][0]["callback_data"] = "repeatlesson_1_".$group_id;
                $keyboard[0][1]["text"] = "6 часов";
                $keyboard[0][1]["callback_data"] = "repeatlesson_6_".$group_id;
                $keyboard[0][2]["text"] = "12 часов";
                $keyboard[0][2]["callback_data"] = "repeatlesson_12_".$group_id;
                $keyboard[0][3]["text"] = "24 часа";
                $keyboard[0][3]["callback_data"] = "repeatlesson_24_".$group_id;
                $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
                $telegram->sendMessage(['chat_id' => $chat_id, 'reply_markup' => $reply_markup, 'text' => "Не все ответы были правильными :(\nДавайте повторим урок! Правильные переводы спрашивать уже не буду...\nЧерез какое время повторим?"]);
            } else {
                $telegram->sendMessage(['chat_id' => $chat_id, 'text' => "Урок пройден на отлично! Чтоб начать новый - напишите мне любое сообщение."]);
            }
            exit();
        }
//        ob_start();
//        print_r($answer_hash);
//        print_r($right_answer_hash);
//        print_r($data[1]);
//        print_r($trans[$data[1]]);
//        $r = ob_get_clean();
//        $telegram->sendMessage(['chat_id' => $chat_id, 'text' => $r]);
//        exit();

        $keyboard[0][0]["text"] = "Смотреть видео";
        $keyboard[0][0]["url"] = "https://youtu.be/ZWls3Zt9C2M?t=2569";
        $keyboard[1][0]["text"] = "Учить дальше";
        $keyboard[1][0]["callback_data"] = "next_steprepeat_".$stage;
        $keyboard[2][0]["text"] = "Статистика";
        $keyboard[2][0]["url"] = "https://inspiratura.com/stat?id=".$stat_id;
        $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
        $updating_prams = [
            'chat_id' => $chat_id,
            'message_id' => $mes_id,
            'text' => $text,
            'reply_markup' => $reply_markup,
            'parse_mode' => 'Markdown'
        ];
        $telegram->editMessageText($updating_prams);
        exit();
    }
    if (mb_stripos($callback_data, 'repeatlesson_') !== false) {
        $data = explode("_", $callback_data);
        $hours = $data[1];
        $group_id = $data[2];
        $shared = $conf->checkGroupForShared($group_id, $chat_id);
        $conf->setRepeat($group_id, $hours, $chat_id, $shared);
        $updating_prams = [
            'chat_id' => $chat_id,
            'message_id' => $mes_id,
            'text' => 'Отлично! Жду с нетерпением :)',
            'parse_mode' => 'Markdown'
        ];
        $telegram->editMessageText($updating_prams);
    }
    if (mb_stripos($callback_data, 'next_step_') !== false) {
        $stage = str_replace('next_step_', '', $callback_data);
        $group_id = $conf->checkUserGroupConnect($chat_id);
        $g = $conf->getGroupById($group_id);
        $text = "Двигаемся дальше! Выберите перевод слова/фразы:\n*".$g["words"][$stage]["word"]."*\n";
        $answers = $conf->getAnswers($g["words"], $g["words"][$stage]["translate"]);
        ksort($answers);

        $conf->setStage($stage, $chat_id);
        $keyboard = [];
        foreach ($answers as $i => $b) {
            $n = $i + 1;
            $hash = hash('sha256', $b);
            $text .= $n." - ".$b."\n";
            $keyboard[0][$i]["text"] = $n;
            $keyboard[0][$i]["callback_data"] = "a_".$stage."_".substr($hash, 0, 40);
        }
        $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
        $updating_prams = [
            'chat_id' => $chat_id,
            'message_id' => $mes_id,
            'text' => $text,
            'reply_markup' => $reply_markup,
            'parse_mode' => 'Markdown'
        ];
        $telegram->editMessageText($updating_prams);
        exit();
    }
    if (mb_stripos($callback_data, 'next_steprepeat_') !== false) {
        $stage = str_replace('next_steprepeat_', '', $callback_data);
        $group_id = $conf->checkUserGroupConnect($chat_id);
        $g = $conf->getGroupById($group_id);
        $last_stat = $g["last_stat"];
        $ls_answers = $conf->getStatById($last_stat, true);
        $text = "Двигаемся дальше! Выберите перевод слова/фразы(повтор):\n*".$ls_answers["stat"][$stage]["word"]."*\n";
        $answers = $conf->getAnswers($g["words"], $ls_answers["stat"][$stage]["translate"]);
        ksort($answers);

        $conf->setStage($stage, $chat_id);
        $keyboard = [];
        foreach ($answers as $i => $b) {
            $n = $i + 1;
            $hash = hash('sha256', $b);
            $text .= $n." - ".$b."\n";
            $keyboard[0][$i]["text"] = $n;
            $keyboard[0][$i]["callback_data"] = "r_".$stage."_".substr($hash, 0, 40);
        }
        $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
        $updating_prams = [
            'chat_id' => $chat_id,
            'message_id' => $mes_id,
            'text' => $text,
            'reply_markup' => $reply_markup,
            'parse_mode' => 'Markdown'
        ];
        $telegram->editMessageText($updating_prams);
        exit();
    }
}