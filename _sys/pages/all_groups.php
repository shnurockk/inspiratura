<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\User;
use Fianta\Sys\Insp;

//Ошибка при многомерной ссылке

if (isset($F_URL) and count($F_URL) > 1)
    die(include_once(F_PATH_SYS.'pages/404.php'));


//Заполняем переменные шаблона

$F_PAGE_GEN['title'] = "Группы";

$F_PAGE_GEN['description'] = "Confpulse";

$F_PAGE_GEN['keywords'] = "Confpulse";

$F_PAGE_GEN['robots'] = 'none';

if (F_LOGGED) {
    $uid = User::get()->id;
    //Подключаем шаблон
    $insp = new Insp();
    $groups = $insp->getGroups($uid);
    include_once(F_PATH_SYS."tpl/all_groups.tpl.php");
} else {
    include_once(F_PATH_SYS."tpl/inc/login.tpl.php");
}