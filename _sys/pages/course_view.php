<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\User;
use Fianta\Sys\Insp;
use Fianta\Sys\UserInfo;

//Ошибка при многомерной ссылке
if (isset($F_URL) and count($F_URL) > 1)
    die(include_once(F_PATH_SYS.'pages/404.php'));

$F_PAGE_GEN['title'] = "Просмотр курса";

$F_PAGE_GEN['description'] = "";

$F_PAGE_GEN['keywords'] = "";

$F_PAGE_GEN['robots'] = 'none';

if (F_LOGGED) {
    $insp = new Insp();

    $uid = User::get()->id;
    $group_id = filter_input(INPUT_GET, 'group_id');
    $buy_result = filter_input(INPUT_GET, 'buy_result');
    
    $course = [];
    if (!empty($group_id)) {
        $course = $insp->getCourse($group_id, ['all']);
        $author = UserInfo::getbyID($course['uid']);
    }

    if(empty($group_id) || empty($course)) {
        die(include_once(F_PATH_SYS.'pages/404.php'));
    }

    $total_sections = count($course['sections']);
    $total_blocks = 0;
    foreach ($course['sections'] as $s){
        $total_blocks += count($s['blocks']);
    }

    $boughtCourse = $insp->getUserBoughtCourse($uid, $group_id);

    include_once(F_PATH_SYS."tpl/course_view.tpl.php");
} else {
    include_once(F_PATH_SYS."tpl/inc/login.tpl.php");
}