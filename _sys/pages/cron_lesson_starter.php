<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\Fianta;
use Fianta\Core\DB;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramResponseException;

$dt = date("d.m.Y H:i:s");
$d = new DateTime($dt);
//$d->modify("+5 hour");
$to = $d->getTimestamp();

//$insp = new Insp();
//$stat = $insp->getStatById(8, TRUE);
//$g = $insp->getGroupById(9);
//Fianta::debug($to);

$q = DB::con()->query("SELECT g.*,u.`t_uid` FROM `".F_DB_PREFIX."groups` g "
        ."JOIN `".F_DB_PREFIX."telegram_users` u ON u.`uid`=g.`uid` WHERE g.`start_date`>0 AND g.`start_date`<=".DB::quote($to)." AND g.`started`=0") or die(Fianta::err(__FILE__, __LINE__));
$groups = $q->fetchAll(PDO::FETCH_ASSOC);

if (!empty($groups)) {
    $telegram = new Api('967935120:AAGM-cl20ZeesA8qZMXd251zldA5eAkGpA8');
    foreach ($groups as $g) {
        if (isset($g["t_uid"]) and ! empty($g["t_uid"])) {
            $keyboard[0][0]["text"] = "Начать";
            $keyboard[0][0]["callback_data"] = "start_lesson_".$g["id"];
            $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
            try {
                $telegram->sendMessage(['chat_id' => $g["t_uid"], 'text' => "Пора обучаться! Урок ".$g["name"].". По готовности нажмите кнопку \"Начать\"", 'parse_mode' => 'Markdown', 'reply_markup' => $reply_markup]);
                DB::con()->query("UPDATE `".F_DB_PREFIX."groups` SET `started`=1 WHERE `id`=".DB::quote($g["id"])) or die(Fianta::err(__FILE__, __LINE__));
            } catch (TelegramResponseException $e) {
                continue;
            }
        }
    }
}

$q2 = DB::con()->query("SELECT g.*,u.`t_uid` FROM `".F_DB_PREFIX."groups` g "
        ."JOIN `".F_DB_PREFIX."telegram_users` u ON u.`uid`=g.`uid` WHERE g.`repeat_date`>0 AND g.`repeat_date`<=".DB::quote($to)." AND g.`repeated`=0") or die(Fianta::err(__FILE__, __LINE__));
$repeats = $q2->fetchAll(PDO::FETCH_ASSOC);
//Fianta::debug($repeats);
if (!empty($repeats)) {
    $telegram = new Api('967935120:AAGM-cl20ZeesA8qZMXd251zldA5eAkGpA8');
    foreach ($repeats as $g) {
        if (isset($g["t_uid"]) and ! empty($g["t_uid"])) {
            $keyboard[0][0]["text"] = "Начать";
            $keyboard[0][0]["callback_data"] = "repeat_lesson_".$g["id"];
            $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
            try {
                $telegram->sendMessage(['chat_id' => $g["t_uid"], 'text' => "Пора повторять урок ".$g["name"]."! По готовности нажмите кнопку \"Начать\"", 'parse_mode' => 'Markdown', 'reply_markup' => $reply_markup]);
                DB::con()->query("UPDATE `".F_DB_PREFIX."groups` SET `repeated`=1 WHERE `id`=".DB::quote($g["id"])) or die(Fianta::err(__FILE__, __LINE__));
            } catch (TelegramResponseException $e) {
                continue;
            }
        }
    }
}

$q3 = DB::con()->query("SELECT g.`id`,g.`name`,u.`t_uid`,u.`uid` FROM `".F_DB_PREFIX."groups` g "
        ."JOIN `".F_DB_PREFIX."shared_groups` s ON s.`group_id`=g.`id` JOIN `".F_DB_PREFIX."telegram_users` u ON u.`uid`=s.`uid` WHERE s.`repeat_date`>0 AND s.`repeat_date`<=".DB::quote($to)." AND s.`repeated`=0") or die(Fianta::err(__FILE__, __LINE__));
$shared_repeats = $q3->fetchAll(PDO::FETCH_ASSOC);

if (!empty($shared_repeats)) {
    $telegram = new Api('967935120:AAGM-cl20ZeesA8qZMXd251zldA5eAkGpA8');
    foreach ($shared_repeats as $g) {
        if (isset($g["t_uid"]) and ! empty($g["t_uid"])) {
            $keyboard[0][0]["text"] = "Начать";
            $keyboard[0][0]["callback_data"] = "repeat_lesson_".$g["id"];
            $reply_markup = $telegram->replyKeyboardMarkup(['inline_keyboard' => $keyboard]);
            try {
                $telegram->sendMessage(['chat_id' => $g["t_uid"], 'text' => "Пора повторять урок ".$g["name"]."! По готовности нажмите кнопку \"Начать\"", 'parse_mode' => 'Markdown', 'reply_markup' => $reply_markup]);
                DB::con()->query("UPDATE `".F_DB_PREFIX."shared_groups` SET `repeated`=1 WHERE `group_id`=".DB::quote($g["id"])." AND `uid`=".DB::quote($g["uid"])) or die(Fianta::err(__FILE__, __LINE__));
            } catch (TelegramResponseException $e) {
                continue;
            }
        }
    }
}

