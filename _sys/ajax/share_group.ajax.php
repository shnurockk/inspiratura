<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\AjaxResponse;
use Fianta\Core\Fianta;
use Fianta\Core\DB;

if (!F_LOGGED) {

    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$id = filter_input(INPUT_POST, 'id');

DB::con()->query("UPDATE `".F_DB_PREFIX."groups` SET `shared`=1 WHERE `id`=".DB::quote($id)) or die(Fianta::err(__FILE__, __LINE__));

$result = new AjaxResponse("success", "Ссылка на урок скопирована в буфер обмена (<a href='https://inspiratura.com/share_group?id=$id' terget='_blank'>https://inspiratura.com/share_group?id=$id</a>)");
exit($result->json());
