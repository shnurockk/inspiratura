<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\User;
use Fianta\Core\AjaxResponse;
use Fianta\Sys\Insp;
use Fianta\Core\Fianta;
use Fianta\Core\DB;
use Fianta\Core\QueryBuilder;

if (!F_LOGGED) {

    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$data = [];
$id = filter_input(INPUT_POST, 'id');
$insp = new Insp();
$group = $insp->getGroupById($id);
//$res = print_r($group["words"], true);

$uid = User::get()->id;
$params = [
    "uid" => $uid,
    "name" => $group["name"]." (скопирован)",
    "start_date" => 0,
    "started" => 0
];
$qb = new QueryBuilder($params);
DB::con()->query("INSERT INTO `".F_DB_PREFIX."groups` (".$qb->insert_fields.") VALUES (".$qb->insert_values.")") or die(Fianta::err(__FILE__, __LINE__));
$group_id = DB::con()->lastInsertId();
$q = "INSERT INTO `".F_DB_PREFIX."words` (`group_id`,`word`,`translate`) VALUES ";
foreach ($group["words"] as $w) {
    $q .= "(".$group_id.",'".$w["word"]."','".$w["translate"]."'),";
}
$query = substr($q, 0, -1).";";
DB::con()->query($query) or die(Fianta::err(__FILE__, __LINE__));

$result = new AjaxResponse("success", $group_id);
exit($result->json());
