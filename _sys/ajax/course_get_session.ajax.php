<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\AjaxResponse;
use Fianta\Core\DB;

if (!F_LOGGED) {
    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$group_id = filter_input(INPUT_POST, 'group_id');

if (!isset($group_id)) {
    $result = new AjaxResponse("error", "Идентификатор группы не указан!");
    exit($result->json());
}
$stage = filter_input(INPUT_POST, 'stage');
if(!isset($_SESSION['course'][$group_id][$stage])){
    $result = new AjaxResponse("error", "Данные не найденны!");
    exit($result->json());
}

$stage_index = filter_input(INPUT_POST, 'stage_index');

$key = filter_input(INPUT_POST, 'key');
$index = filter_input(INPUT_POST, 'index');

if(!isset($_SESSION['course'][$group_id][$stage][$stage_index])){
    $result = new AjaxResponse("error", "Данные с таким индексом не найденны!");
    exit($result->json());
}

$data = null;
if(isset($_SESSION['course'][$group_id][$stage][$stage_index][$key][$index])) {
    $data = $_SESSION['course'][$group_id][$stage][$stage_index][$key][$index];
} else if(isset($_SESSION['course'][$group_id][$stage][$stage_index][$key])){
    $data = $_SESSION['course'][$group_id][$stage][$stage_index][$key];
} else if(isset($_SESSION['course'][$group_id][$stage][$stage_index][$index])){
    $data = $_SESSION['course'][$group_id][$stage][$stage_index][$index];
} else {
    $data = $_SESSION['course'][$group_id][$stage][$stage_index];
}

$result = new AjaxResponse("success", "ОК", $data);
exit($result->json());