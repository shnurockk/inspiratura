<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\AjaxResponse;
use Fianta\Sys\Translate;

if (!F_LOGGED) {

    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$data = [];
$sdata = filter_input(INPUT_POST, 'data');

parse_str($sdata, $data);

$translated = [];
$words = [];
if (!empty($data["words"])) {
    //$arr = explode("\n", str_replace("\r", "", $data["words"]));
    $arr = array_diff($data["words"], array(' ', '', '\r', NULL, false));
    foreach ($arr as $v) {
        //$words[] = trim(str_replace(" ", "", $v));
        $words[] = trim($v);
    }

    $translated = Translate::translate("ru", "en", $words);
}
$data["text"] = $translated;
//ob_start();
//echo '<pre>';
//print_r($translated);
//$r = ob_get_clean();
//$result = new AjaxResponse("error", $r);
//exit($result->json());
//$str = implode("\n", $translated);
$result = new AjaxResponse("success", "", $data);
exit($result->json());
