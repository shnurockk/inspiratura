<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\User;
use Fianta\Core\AjaxResponse;
use Fianta\Sys\Insp;

if(!F_LOGGED){
    $response = new AjaxResponse('error', 'Нету доступа.');
    exit($response->json());
}

// 0 - Ученик, 1 - Преподаватель
$user_mode = filter_input(INPUT_POST, 'user_mode');
if($user_mode == 1){
    $_SESSION['user_mode'] = $user_mode;
    $message = 'Переход в режим \'Преподавателя\'.';
}
else if(isset($_SESSION['user_mode'])){
    unset($_SESSION['user_mode']);
    $message = 'Переход в режим \'Ученика\'.';
}
else{
    $response = new AjaxResponse('error', 'Не возможно выполнить данное действие.');
    exit($response->json());
}

$response = new AjaxResponse('success', $message);
exit($response->json());
