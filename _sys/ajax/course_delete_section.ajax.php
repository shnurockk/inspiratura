<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\AjaxResponse;
use Fianta\Core\DB;

if (!F_LOGGED) {
    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$index = filter_input(INPUT_POST, 'index');
if(!isset($index)){
    $result = new AjaxResponse("error", "Индекс не указан");
    exit($result->json());
}

$group_id = filter_input(INPUT_POST, 'group_id');
if(empty($group_id)){
    $result = new AjaxResponse("error", "Идентификатор группы не указан");
    exit($result->json());
}
$section_index = filter_input(INPUT_POST, 'section_index');
if(isset($section_index)){
    $name = $_SESSION['course'][$group_id]['sections'][$section_index]['blocks'][$index]['name'];
    $_SESSION['course'][$group_id]['sections'][$section_index]['blocks'][$index]['deleted'] = 1;
} else {
    $name = $_SESSION['course'][$group_id]['sections'][$index]['name'];
    $_SESSION['course'][$group_id]['sections'][$index]['deleted'] = 1;
}


$result = new AjaxResponse("success", "'<b>$name</b>', успешно удалён!");
exit($result->json());