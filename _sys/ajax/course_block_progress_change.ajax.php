<?php

use Fianta\Core\AjaxResponse;

if (!F_LOGGED) {
    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$id = filter_input(INPUT_POST, 'id');
if(empty($id)){
    $result = new AjaxResponse("error", "Не указан идентификатор.");
    exit($result->json());
}

$section_id = filter_input(INPUT_POST, 'section_id');
if(empty($section_id)){
    $result = new AjaxResponse("error", "Не указан раздел.");
    exit($result->json());
}

$block_id = filter_input(INPUT_POST, 'block_id');
if(empty($block_id)){
    $result = new AjaxResponse("error", "Не указан блок.");
    exit($result->json());
}

$done = filter_input(INPUT_POST, 'done');
if(empty($block_id)){
    $result = new AjaxResponse("error", "Не указан параметр 'done'.");
    exit($result->json());
}

$uid = \Fianta\Core\User::get()->id;
$insp = new \Fianta\Sys\Insp();
$result = $insp->setCourseProgress($uid, $id, $section_id, $block_id, $done);
if($result === false){
    $result = new AjaxResponse("error", "Не удалось обработать запрос.");
    exit($result->json());
}
$progress = $insp->getCourseProgressCount($uid, $id, $section_id);

$result = new AjaxResponse("success", "OK", $progress);
exit($result->json());