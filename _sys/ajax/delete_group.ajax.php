<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\AjaxResponse;
use Fianta\Core\Fianta;
use Fianta\Core\DB;
use Fianta\Core\User;

if (!F_LOGGED) {

    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$id = filter_input(INPUT_POST, 'id');
$sub = filter_input(INPUT_POST, 'sub');
if ($sub == 1) {
    $uid = User::get()->id;
    DB::con()->query("DELETE FROM `".F_DB_PREFIX."shared_groups` WHERE `group_id`=".DB::quote($id)." AND `uid`=".DB::quote($uid)) or die(Fianta::err(__FILE__, __LINE__));
} else {
    DB::con()->query("DELETE FROM `".F_DB_PREFIX."groups` WHERE `id`=".DB::quote($id)) or die(Fianta::err(__FILE__, __LINE__));
}
$result = new AjaxResponse("success", "Урок удален!");
exit($result->json());
