<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\User;
use Fianta\Core\AjaxResponse;
use Fianta\Sys\Insp;

if (!F_LOGGED) {

    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$uid = User::get()->id;
$conf = new Insp();
$user = $conf->unauthTelegram($uid);

$result = new AjaxResponse("success", "Бот успешно привязан!");
exit($result->json());

