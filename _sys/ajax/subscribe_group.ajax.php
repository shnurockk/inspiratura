<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\AjaxResponse;
use Fianta\Core\Fianta;
use Fianta\Core\DB;

if (!F_LOGGED) {

    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$data = [];
$id = filter_input(INPUT_POST, 'id');
$uid = filter_input(INPUT_POST, 'uid');

DB::con()->query("INSERT INTO `".F_DB_PREFIX."shared_groups` (`uid`,`group_id`) VALUES (".DB::quote($uid).", ".DB::quote($id).") ON DUPLICATE KEY UPDATE `group_id`=".DB::quote($id)) or die(Fianta::err(__FILE__, __LINE__));

$result = new AjaxResponse("success", "Урок добавлен в список интересующих");
exit($result->json());
