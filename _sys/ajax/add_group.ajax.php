<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\User;
use Fianta\Core\AjaxResponse;
use Fianta\Sys\Insp;

if (!F_LOGGED) {

    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$data = [];
$sdata = filter_input(INPUT_POST, 'data');

parse_str($sdata, $data);


$words = [];
$translated = [];
if (!empty($data["words"])) {
    //$arr = explode("\n", str_replace("\r", "", $data["words"]));
    $arr = array_diff($data["words"], array(' ', '', '\r', NULL, false));
    foreach ($arr as $v) {
        //$words[] = trim(str_replace(" ", "", $v));
        $words[] = trim($v);
    }
}
if (!empty($data["translate"])) {
    //$arr2 = explode("\n", str_replace("\r", "", $data["translate"]));
    $arr2 = array_diff($data["translate"], array(' ', '', '\r', NULL, false));
    foreach ($arr2 as $v) {
        //$words[] = trim(str_replace(" ", "", $v));
        $translated[] = trim($v);
    }
}
//ob_start();
//echo "<pre>";
//print_r($translated);
//print_r($words);
//echo "</pre>";
//$r = ob_get_clean();
//$result = new AjaxResponse("error", $r);
//exit($result->json());
if (empty($data["name"])) {
    $result = new AjaxResponse("error", "Не заполнено название группы!");
    exit($result->json());
}
if (empty($words)) {
    $result = new AjaxResponse("error", "Не заполнены слова/фразы (рус)");
    exit($result->json());
}
if (empty($translated)) {
    $result = new AjaxResponse("error", "Не заполнены переводы");
    exit($result->json());
}
if (count($words) < 5) {
    $result = new AjaxResponse("error", "Необходимо минимум 5 слов/фраз!");
    exit($result->json());
}
if (count($translated) != count($words)) {
    $result = new AjaxResponse("error", "Количество слов/фраз должно быть равно количеству переводов!");
    exit($result->json());
}
$date = 0;
$started = 0;
$opened = 0;
if (isset($data["opened"])) {
    $opened = 1;
}
if (!empty($data["start_date"])) {
    $date = strtotime($data["start_date"]);
}
if (!$date) {
    $started = 1;
}
$blocks["words"] = $words;
$blocks["translate"] = $translated;
$uid = User::get()->id;
$group_params = [
    "uid" => $uid,
    "name" => $data["name"],
    "start_date" => $date,
    "started" => $started,
    "opened" => $opened
];
$insp = new Insp();
$insp->addGroup($group_params, $blocks);
$result = new AjaxResponse("success", "Успешно сохранено!");
exit($result->json());


