<?php
defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\AjaxResponse;
use Fianta\Core\DB;

if (!F_LOGGED) {
    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$id = filter_input(INPUT_POST, 'id');
if(empty($id)){
    $result = new AjaxResponse("error", "Неуказан идентификатор");
    exit($result->json());
}
$uid = \Fianta\Core\User::get()->id;

$query = DB::con()->query('DELETE FROM `'.F_DB_PREFIX.'groups_course` WHERE `id`='.DB::quote($id).' AND `uid`='.DB::quote($uid));
if($query === false){
    $result = new AjaxResponse("error", "Не удалось удалить указаный курс!");
    exit($result->json());
}

$result = new AjaxResponse("success", "Успешно удалён!");
exit($result->json());