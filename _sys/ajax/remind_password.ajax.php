<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\AjaxResponse;
use Fianta\Core\Fianta;
use Fianta\Core\Converter;
use Fianta\Core\DB;
use Fianta\Core\Email;

if (!filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL)) {
    $result = new AjaxResponse("error", "Некорректный Email");
    exit($result->json());
} else {
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
}

$e_q = DB::con()->query("SELECT `id`,`updated`,`pass` FROM `".F_DB_PREFIX."users` WHERE `email`=".DB::quote(trim($email))) or die(Fianta::err(__FILE__, __LINE__));
$ids = $e_q->fetch(PDO::FETCH_ASSOC);
if (empty($ids)) {
    $result = new AjaxResponse("error", "Пользователь с данным Email не найден!");
    exit($result->json());
}
$uid = $ids["id"];
$updated = $ids["updated"];
$secret = $ids["pass"];

$now = date("Y-m-d H:i:s");
$unow = Converter::toUnixDate($now);
$check_sec = $unow - $updated;
if ($check_sec < 60) {
    $wait = 60 - $check_sec;
    $result = new AjaxResponse("error", "Email уже отправлялся! Повторная попытка доступна через: ".$wait." сек.");
    exit($result->json());
}

$link = 'http://'.$_SERVER['SERVER_NAME'].'/remind_password?secret='.trim($secret);

ob_start();
include(F_PATH_SYS."tpl/emails/remind_password.tpl.php");
$msg = ob_get_clean();
//$headers = 'MIME-Version: 1.0'."\r\n";
//$headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
//$res = mail(trim($email), "Восстановление пароля", $msg, $headers);
$e_mail = new Email($email, "Восстановление пароля", $msg);
$res = $e_mail->mailSend();

if ($res != 1) {
    $result = new AjaxResponse("error", "Ошибка отправки: ".$res);
    exit($result->json());
}
$salt = mt_rand(100000, 999999);
DB::con()->query("UPDATE `".F_DB_PREFIX."users` SET `updated` = ".DB::quote($unow).", `salt`=".DB::quote($salt)." WHERE `id`=".DB::quote($uid)) or die(Fianta::err(__FILE__, __LINE__));

$result = new AjaxResponse("success", 'Email отправлен на почту! Если письмо не пришло, проверьте папку "спам".');
exit($result->json());
