<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\User;
use Fianta\Core\AjaxResponse;
use Fianta\Sys\Insp;

if (!F_LOGGED) {

    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$uid = User::get()->id;
$first_name = filter_input(INPUT_POST, 'first_name');
$user_id = filter_input(INPUT_POST, 'user_id');
$conf = new Insp();
$user = $conf->checkUser($user_id);
if ($user) {
    $conf->authTelegram($uid, $user_id);
} else {
    $conf->addUser("", $user_id, $first_name);
    $conf->authTelegram($uid, $user_id);
}
$result = new AjaxResponse("success", "Бот успешно привязан!");
exit($result->json());

