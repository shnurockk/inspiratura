<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\AjaxResponse;
use Fianta\Core\Fianta;
use Fianta\Core\Converter;
use Fianta\Core\DB;
use Fianta\Core\Email;

$uid = filter_input(INPUT_POST, 'uid', FILTER_DEFAULT);

$upd_q = DB::con()->query("SELECT `updated`,`email`,`confrim_email` FROM `".F_DB_PREFIX."users` WHERE `id`=".DB::quote($uid)) or die(Fianta::err(__FILE__, __LINE__));
$updated = $upd_q->fetch(PDO::FETCH_ASSOC);
$email = $updated["email"];
$confrim = $updated["confrim_email"];
$updated = $updated["updated"];

$now = date("Y-m-d H:i:s");
$unow = Converter::toUnixDate($now);
$check_sec = $unow - $updated;
if ($check_sec < 60) {
    $wait = 60 - $check_sec;
    $result = new AjaxResponse("error", "Email уже отправлялся! Повторная попытка доступна через: ".$wait." сек.");
    exit($result->json());
}

$link = 'http://'.$_SERVER['SERVER_NAME'].'/registration?confrim_email='.trim($confrim)."&uid=".$uid;

ob_start();
include(F_PATH_SYS."tpl/emails/mail_confirmation.tpl.php");
$msg = ob_get_clean();
$e_mail = new Email($email, "Подтверждение почты", $msg);
$res = $e_mail->mailSend();
//$headers = 'MIME-Version: 1.0'."\r\n";
//$headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
//$res = mail(trim($email), "Подтверждение почты", $msg, $headers);

if ($res != 1) {
    $result = new AjaxResponse("error", "Ошибка отправки: ".$res);
    exit($result->json());
}

DB::con()->query("UPDATE `".F_DB_PREFIX."users` SET `updated` = ".DB::quote($unow)." WHERE `id`=".DB::quote($uid)) or die(Fianta::err(__FILE__, __LINE__));

$result = new AjaxResponse("success", 'Email отправлен на почту! Если письмо не пришло, проверьте папку "спам".');
exit($result->json());
