<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\AjaxResponse;

if (!F_LOGGED) {
    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$uid = \Fianta\Core\User::get()->id;

$group_id = filter_input(INPUT_POST, 'id');

$data = null;
if (isset($_SESSION['course'][$group_id])) {
    $data = $_SESSION['course'][$group_id];
}

if (empty($data)) {
    $result = new AjaxResponse("error", "Не возможно выполнить данное действие. Попробуйте обновить страницу. Если данная ошибка повториться обратитесь к администратору.");
    exit($result->json());
}

$resp = [];
$err = [];
$err_page = '';

if (!isset($data["name"]) || empty($data["name"])) {
    $err_page = 'Информация о курсе';
}

if (!isset($data["learns"]) || isEmpty($data["learns"])) {
    $result = new AjaxResponse("error", "Необходимо указать что студенты будут изучать на вашем курсе!");
    exit($result->json());
} else {
    $i = -1;
    $err_1 = [];
    $err_1['err_i'] = '';
    foreach ($data['learns'] as $l) {
        ++$i;
        if (!empty($l['desc'])) {
            continue;
        }

        if($l['deleted'] == 1) {
            continue;
        }
        $err_1['err_i'] .= $i.',';
    }

    if (!empty($err_1['err_i'])) {
        $err_1['err_section'] = 'learns';
        $err_page = '<b>Информация о курсе</b>';
        $err['err_stage'] = 'info';
        $err['info'][] = $err_1;
    }
}

if (!isset($data["requirements"]) || isEmpty($data["requirements"])) {
    $result = new AjaxResponse("error", "Необходимо указать хотя бы одно требование!");
    exit($result->json());
} else {
    $i = -1;
    $err_2 = [];
    $err_2['err_i'] = '';
    foreach ($data['requirements'] as $req) {
        ++$i;

        if (!empty($req['desc'])) {
            continue;
        }

        if($req['deleted'] == 1) {
            continue;
        }

        $err_2['err_i'] .= $i.',';
    }

    if (!empty($err_2['err_i'])) {
        $err_2['err_section'] = 'requirements';
        $err_page = '<b>Информация о курсе</b>';
        $err['err_stage'] = 'info';
        $err['info'][] = $err_2;
    }
}

if (!isset($data["audiences"]) || isEmpty($data["audiences"])) {

    $result = new AjaxResponse("error", "Необходимо указать хотя бы одну целивую аудиторию!");
    exit($result->json());
} else {
    $i = -1;
    $err_3 = [];
    $err_3['err_i'] = '';
    foreach ($data['audiences'] as $a) {
        ++$i;
        if (!empty($a['desc'])) {
            continue;
        }

        if($a['deleted'] == 1) {
            continue;
        }

        $err_3['err_i'] .= $i.',';
    }

    if (!empty($err_3['err_i'])) {
        $err_3['err_section'] = 'audiences';
        $err_page = '<b>Информация о курсе</b>';
        $err['err_stage'] = 'info';
        $err['info'][] = $err_3;
    }
}

if (!isset($data["cost"]) || empty($data["cost"])) {
    if (!empty($err_page)) {
        $err_page .= ', ';
    }

    $err_page .= '<b>Стоимость курса</b>';
    $err['err_stage'] = 'cost';
    $err['cost'][] = ['err_i' => '0', 'err_section' => 'cost'];
}

if(!empty($err_page)){
    $err_page = 'Незаполненны поля на страницах: \''.$err_page.'\'';
}

$is_empty = true;
$empty_blocks = '';
if(isset($data["sections"])) {

    foreach ($data["sections"] as $k => $section) {
        if($section['deleted'] == 1){
            continue;
        }

        $is_empty = false;
        $is_empty_b = true;
        if(!isset($section['blocks'])
        || empty($section['blocks'])){
            $is_empty_b = true;
        } else {
            foreach ($section['blocks'] as $block){
                if($block['deleted'] == 0){
                    $is_empty_b = false;
                    break;
                }
            }
        }

        if($is_empty_b) {
            if(!empty($empty_blocks)){
                $empty_blocks .= ',';
            }

            $empty_blocks .= "<b>$section[name]</b>";
        }
    }
}
if ($is_empty) {
    if (!empty($err_page)) {
        $err_page .= '<br />';
    }

    $err_page .= 'Необходимо создать хотя бы один раздел!';
    $err['err_stage'] = 'section';
}

if(!empty($empty_blocks) && !$is_empty){
    if (!empty($err_page)) {
        $err_page .= '<br />';
    }

    $err_page .= 'Необходимо создать хотя бы один блок для разделов: '. $empty_blocks;
    $err['err_stage'] = 'section';
}

if (!empty($err)) {
    $result = new AjaxResponse("error", $err_page, $err);
    exit($result->json());
}

$group_id = isset($data['id']) ? $data['id'] : null;
$image = isset($data['img']) ? $data['img'] : null;
$query = \Fianta\Core\DB::con()->query('INSERT INTO `'.F_DB_PREFIX.'groups_course` (`id`,`uid`,`name`,`img`) VALUES('.\Fianta\Core\DB::quote($group_id).','.\Fianta\Core\DB::quote($uid).','.\Fianta\Core\DB::quote($data['name']).','.\Fianta\Core\DB::quote($image).') ON DUPLICATE KEY UPDATE `name` = '.\Fianta\Core\DB::quote($data['name']).', `img`='.\Fianta\Core\DB::quote($image));
$group_id = empty($data['id']) ? \Fianta\Core\DB::con()->lastInsertId() : $group_id;

foreach ($data['learns'] as $k => $l) {
    $block_id = isset($l['id']) ? $l['id'] : null;
    if($l['deleted'] == 1 && $block_id !== null) {
        $query = \Fianta\Core\DB::con()->query('DELETE FROM `'.F_DB_PREFIX.'groups_course_learn` WHERE `id`='.\Fianta\Core\DB::quote($block_id).' AND `group_id`='.\Fianta\Core\DB::quote($group_id));
    } else if($l['deleted'] == 0) {
        $query = \Fianta\Core\DB::con()->query('INSERT INTO `'.F_DB_PREFIX.'groups_course_learn`(`id`,`group_id`,`desc`,`index`) VALUES ('.\Fianta\Core\DB::quote($block_id).','.\Fianta\Core\DB::quote($group_id).','.\Fianta\Core\DB::quote($l['desc']).','.\Fianta\Core\DB::quote($k).') ON DUPLICATE KEY UPDATE `desc` = '.\Fianta\Core\DB::quote($l['desc']).', `index` = '.\Fianta\Core\DB::quote($k));
    }
}

foreach ($data['requirements'] as $k => $r) {
    $block_id = isset($r['id']) ? $r['id'] : null;
    if($r['deleted'] == 1 && $block_id !== null) {
        $query = \Fianta\Core\DB::con()->query('DELETE FROM `'.F_DB_PREFIX.'groups_course_requirements` WHERE `id`='.\Fianta\Core\DB::quote($block_id).' AND `group_id`='.\Fianta\Core\DB::quote($group_id));

    } else if($r['deleted'] == 0) {
        $query = \Fianta\Core\DB::con()->query('INSERT INTO `'.F_DB_PREFIX.'groups_course_requirements`(`id`,`group_id`,`desc`,`index`) VALUES ('.\Fianta\Core\DB::quote($block_id).','.\Fianta\Core\DB::quote($group_id).','.\Fianta\Core\DB::quote($r['desc']).','.\Fianta\Core\DB::quote($k).') ON DUPLICATE KEY UPDATE `desc` = '.\Fianta\Core\DB::quote($r['desc']).', `index` = '.\Fianta\Core\DB::quote($k) );
    }
}

foreach ($data['audiences'] as $k => $a) {
    $block_id = isset($a['id']) ? $a['id'] : null;
    if($a['deleted'] == 1 && $block_id !== null) {
        $query = \Fianta\Core\DB::con()->query('DELETE FROM `'.F_DB_PREFIX.'groups_course_audiences` WHERE `id`='.\Fianta\Core\DB::quote($block_id).' AND `group_id`='.\Fianta\Core\DB::quote($group_id));
    } else if($a['deleted'] == 0) {
        $query = \Fianta\Core\DB::con()->query('INSERT INTO `'.F_DB_PREFIX.'groups_course_audiences`(`id`,`group_id`,`desc`,`index`) VALUES ('.\Fianta\Core\DB::quote($block_id).','.\Fianta\Core\DB::quote($group_id).','.\Fianta\Core\DB::quote($a['desc']).','.\Fianta\Core\DB::quote($k).') ON DUPLICATE KEY UPDATE `desc` = '.\Fianta\Core\DB::quote($a['desc']).', `index` = '.\Fianta\Core\DB::quote($k));
    }
}

foreach ($data['sections'] as $k => $a) {
    $block_id = isset($a['id']) ? $a['id'] : null;
    if($a['deleted'] == 1 && $block_id !== null) {
        $query = \Fianta\Core\DB::con()->query('DELETE FROM `'.F_DB_PREFIX.'groups_course_sections` WHERE `id`='.\Fianta\Core\DB::quote($block_id).' AND `group_id`='.\Fianta\Core\DB::quote($group_id));
    } else if($a['deleted'] == 0) {
        $query = \Fianta\Core\DB::con()->query('INSERT INTO `'.F_DB_PREFIX.'groups_course_sections`(`id`,`group_id`,`name`,`desc`,`index`) VALUES ('.\Fianta\Core\DB::quote($block_id).','.\Fianta\Core\DB::quote($group_id).','.\Fianta\Core\DB::quote($a['name']).','.\Fianta\Core\DB::quote($a['desc']).','.\Fianta\Core\DB::quote($k).') ON DUPLICATE KEY UPDATE `desc` = '.\Fianta\Core\DB::quote($a['desc']).', `name` = '.\Fianta\Core\DB::quote($a['name']).', `index` = '.\Fianta\Core\DB::quote($k));

        $section_id = $block_id == null ?  \Fianta\Core\DB::con()->lastInsertId() : $block_id;
        foreach ($a['blocks'] as $k_b => $block){
            $block_id = isset($block['id']) && $block['id'] > 0 ? $block['id'] : null;

            if($block['deleted'] == 1 && $block_id !== null) {
                $query = \Fianta\Core\DB::con()->query('DELETE FROM `'.F_DB_PREFIX.'groups_course_section_blocks` WHERE `id`='.\Fianta\Core\DB::quote($block_id).' AND `section_id`='.\Fianta\Core\DB::quote($section_id));
            } else if($block['deleted'] == 0) {
                $query = \Fianta\Core\DB::con()->query('INSERT INTO `'.F_DB_PREFIX.'groups_course_section_blocks`(`id`,`section_id`,`name`,`attach`,`index`) VALUES ('.\Fianta\Core\DB::quote($block_id).','.\Fianta\Core\DB::quote($section_id).','.\Fianta\Core\DB::quote($block['name']).','.\Fianta\Core\DB::quote($block['attach']).','.\Fianta\Core\DB::quote($k_b).') ON DUPLICATE KEY UPDATE `attach` = '.\Fianta\Core\DB::quote($block['attach']).', `name` = '.\Fianta\Core\DB::quote($block['name']).', `index` ='. \Fianta\Core\DB::quote($k_b));
            }
        }
    }

}


$query = \Fianta\Core\DB::con()->query('UPDATE `'.F_DB_PREFIX.'groups_course` SET `cost`='.\Fianta\Core\DB::quote($data['cost']).' WHERE `id`='.\Fianta\Core\DB::quote($group_id));

unset($_SESSION['course'][$group_id]);

$result = new AjaxResponse("success", 'Успешно сохранён в базе! Возвращаем на страницу с уроками.', $resp);
exit($result->json());


function isEmpty($arr){
    if(empty($arr) || empty($arr[0])){
        return true;
    }

    foreach ($arr as $item){
        if(isset($item['deleted']) && empty($item['deleted'])){
            return false;
        }
    }

    return true;
}
