<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\AjaxResponse;

if (!F_LOGGED) {
    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$id = filter_input(INPUT_POST, 'id');
if(empty($id)){
    $result = new AjaxResponse("error", "Не указан идентификатор.");
    exit($result->json());
}

$section_id = filter_input(INPUT_POST, 'section_id');
if(empty($section_id)){
    $result = new AjaxResponse("error", "Не указан раздел.");
    exit($result->json());
}

$block_id = filter_input(INPUT_POST, 'block_id');
if(empty($block_id)){
    $result = new AjaxResponse("error", "Не указан блок.");
    exit($result->json());
}

$uid = \Fianta\Core\User::get()->id;
$insp = new \Fianta\Sys\Insp();
$attach = $insp->getUserBlockAttach($uid, $id, $section_id, $block_id);
if($attach === false){
    $result = new AjaxResponse("error", "Не найдено.");
    exit($result->json());
}

$_SESSION['COURSE_'.$id]['LAST'] = ['section_id' => $section_id, 'block_id' => $block_id];

$result = new AjaxResponse("success", "ОК", $attach);
exit($result->json());
