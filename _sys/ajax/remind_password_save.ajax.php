<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\AjaxResponse;
use Fianta\Core\Auth;
use Fianta\Core\Fianta;
use Fianta\Core\DB;

$data = [];
$sdata = filter_input(INPUT_POST, 'form_data');
parse_str($sdata, $data);

if (empty($data["pass"]) or empty($data["pass2"])) {
    $result = new AjaxResponse("error", "Не заполнено поле: Пароль и Подтверждение пароля");
    exit($result->json());
}
if (empty($data["salt"])) {
    $result = new AjaxResponse("error", "Непредвиденная ошибка! Повторите попытку!");
    exit($result->json());
}
if (empty($data["secret"])) {
    $result = new AjaxResponse("error", "Непредвиденная ошибка! Повторите попытку!");
    exit($result->json());
}

$error = Auth::passValidation($data["pass"], $data["pass2"]);
if (!empty($error)) {
    $result = new AjaxResponse("error", $error);
    exit($result->json());
}
$salt = filter_var($data["salt"], FILTER_VALIDATE_INT);
$secret = filter_var($data["secret"], FILTER_DEFAULT);
$new_pass = Auth::crypt_pass(trim($data["pass"]), $salt);

DB::con()->query("UPDATE `".F_DB_PREFIX."users` SET `pass` = ".DB::quote($new_pass)." WHERE `pass`=".DB::quote($secret)." AND `salt`=".DB::quote($salt)) or die(Fianta::err(__FILE__, __LINE__));
Auth::logout();
$result = new AjaxResponse("success", "Новый пароль сохранен!");
exit($result->json());
