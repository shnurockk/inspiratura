<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\AjaxResponse;
use Fianta\Core\DB;

if (!F_LOGGED) {
    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$group_id = filter_input(INPUT_POST, 'group_id');
if(empty($group_id)){
    $result = new AjaxResponse("error", "Идентификатор группы не указан");
    exit($result->json());
}

$section_index = filter_input(INPUT_POST, 'section_index');

$from = filter_input(INPUT_POST, 'from_index');
$to = filter_input(INPUT_POST, 'to_index');

if(isset($section_index)) {
    $array = $_SESSION['course'][$group_id]['sections'][$section_index]['blocks'];
} else {
    $array = $_SESSION['course'][$group_id]['sections'];
}

$from_i = $array[$from];
$to_i = $array[$to];
$array[$to] = $from_i;
$array[$from] = $to_i;

if(isset($section_index)){
    $_SESSION['course'][$group_id]['sections'][$section_index]['blocks'] = $array;
} else {
    $_SESSION['course'][$group_id]['sections'] = $array;
}


$name = $_SESSION['course'][$group_id]['name'];

$result = new AjaxResponse("success", "Раздел '$name' обновлён!");
exit($result->json());
