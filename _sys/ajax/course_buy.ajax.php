<?php

use Fianta\Core\AjaxResponse;

//Эмуляция оплаты
if (!F_LOGGED) {
    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}

$group_id = filter_input(INPUT_POST, 'group_id');
if (empty($group_id)) {
    $result = new AjaxResponse("error", "Идентификатор курсов не указан.");
    exit($result->json());
}

$insp = new \Fianta\Sys\Insp();
$course = $insp->getCourse($group_id);
if (empty($course)) {
    $result = new AjaxResponse("error", "Курсы с идентификатором '$group_id' небыла найдена!");
    exit($result->json());
}

$uid = \Fianta\Core\User::get()->id;

//TODO Процедура оплаты

$q = \Fianta\Core\DB::con()->query('SELECT COUNT(*) FROM `'.F_DB_PREFIX.'users_course_bought` WHERE `uid`='.\Fianta\Core\DB::quote($uid).' AND `course_id`='.\Fianta\Core\DB::quote($group_id));
$count = $q->fetchColumn();
if ($count > 0) {
    $result = new AjaxResponse("error", "Вы уже приобрели '<b>".$course['name']."</b>'!");
    exit($result->json());
}

$date = getdate();

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://api.easypay.ua/api/system/createApp");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, true);  //Post Fields
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$headers = [
    'PartnerKey: easypay-test',
    'locale: ua'
];
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$server_output = curl_exec($ch);
curl_close($ch);
$data = json_decode($server_output, TRUE); // Создали сессию, получили данные appId pageId
if (!is_null($data["error"]) and ! empty($data["error"])) {
    $result = new AjaxResponse("error", "Ошибка формирования платежа, попробуйте позже!(".$data["error"].")");
    exit($result->json());
}

$body = [
    "order" => [
        "serviceKey" => "MERCHANT-TEST",
        "orderId" => $uid."_".$group_id."_".$date[0],
        "description" => "Оплата курса обучения: ".$course["name"],
        "amount" => $course["cost"],
        "additionalItems" => [
            "Merchant.UrlNotify" => "https://inspiratura.com/buy_course"
        ]
    ],
    "urls" => [
        "success" => "https://inspiratura.com/course_view?group_id=".$group_id."&buy_result=success",
        "failed" => "https://inspiratura.com/course_view?group_id=".$group_id."&buy_result=failed"
    ]
];
$json = json_encode($body);
$sign = base64_encode(hash('sha256', ("test".$json), true));

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://api.easypay.ua/api/merchant/createOrder");
curl_setopt($ch, CURLOPT_POST, 1);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$Ordheaders = [
    'appId: '.$data["appId"],
    'pageId: '.$data["pageId"],
    'sign: '.$sign,
    'PartnerKey: easypay-test',
    'Content-Type: application/json',
    'locale: ua'
];


curl_setopt($ch, CURLOPT_POSTFIELDS, $json);  //Post Fields
curl_setopt($ch, CURLOPT_HTTPHEADER, $Ordheaders);

$Ordres = curl_exec($ch);

curl_close($ch);

$Orddata = json_decode($Ordres, TRUE);
if (!is_null($Orddata["error"]) and ! empty($Orddata["error"])) {
    $result = new AjaxResponse("error", "Ошибка формирования платежа, попробуйте позже!(".$Orddata["error"].")");
    exit($result->json());
}

$result = new AjaxResponse("success", $Orddata["forwardUrl"]);
exit($result->json());

//$query = \Fianta\Core\DB::con()->query('INSERT INTO `'.F_DB_PREFIX.'users_course_bought` (uid,course_id,boughtdate) VALUES ('.\Fianta\Core\DB::quote($uid).','.\Fianta\Core\DB::quote($group_id).','.strtotime(date(DATE_RFC2822)).')');

//$result = new AjaxResponse("success", "Вы успешно приобрели '<b>".$course['name']."</b>'!");
//exit($result->json());
