<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));
/* * ***********************************************************************************************
  FIANTA FRAMEWORK BY FEDOR YASTREBOV
  Запрещено использование данного программного кода или его части в любых целях без согласия автора.
  COPYRIGHT © 2013-2017 WEB: http://fianta.com EMAIL: adm@fianta.com
 * ************************************************************************************************ */

use Fianta\Core\AjaxResponse;
use Fianta\Core\Auth;
use Fianta\Sys\Reg;
use Fianta\Core\Fianta;
use Fianta\Core\DB;
use Fianta\Core\Email;

$data = [];
$sdata = filter_input(INPUT_POST, 'form_data');
parse_str($sdata, $data);

//Валидация
if (!isset($data["premissons"])) {
    $result = new AjaxResponse("error", "Необходимо дать согласие на обработку данных!");
    exit($result->json());
}
/*if (!isset($data["name"]) or empty($data["name"])) {
    $result = new AjaxResponse("error", "Не заполнено поле: Имя");
    exit($result->json());
}*/
if (!isset($data["email"]) or empty($data["email"])) {
    $result = new AjaxResponse("error", "Не заполнено поле: Email");
    exit($result->json());
} else {
    if (!filter_var($data["email"], FILTER_VALIDATE_EMAIL)) {
        $result = new AjaxResponse("error", "Некорректный Email");
        exit($result->json());
    }
    $email = filter_var($data["email"], FILTER_VALIDATE_EMAIL);
}
/*$login = preg_replace("/[^0-9]/", '', $data["phone"]);
if (substr($login, 0, 3) != 380 or strlen($login) != 12) {
    $result = new AjaxResponse("error", "Некорректно указан номер телефона");
    exit($result->json());
}*/


if (empty($data["pass"]) or empty($data["pass2"])) {
    $result = new AjaxResponse("error", "Не заполнено поле: Пароль и Подтверждение пароля");
    exit($result->json());
}
$current_time = strtotime(date("Y-m-d H:i:s"));
$fields = [
    "fio" => '',
    "login" => null,
    "email" => trim($email),
    "confrim_email" => $current_time,
    "role_id" => 2,
    "updated" => $current_time,
    "user_id" => 0
];

//Ренистрация
$reg = new Reg($fields, $data["pass"], $data["pass2"]);
$r = $reg->execute();
if (!empty($r)) {
    $result = new AjaxResponse("error", $r);
    exit($result->json());
}
$uid = $reg->getuid();

$upd_q = DB::con()->query("SELECT `email`,`confrim_email` FROM `".F_DB_PREFIX."users` WHERE `id`=".DB::quote($uid)) or die(Fianta::err(__FILE__, __LINE__));
$updated = $upd_q->fetch(PDO::FETCH_ASSOC);
$email = $updated["email"];
$confrim = $updated["confrim_email"];


$link = F_SSL_URL.'/registration?confrim_email='.trim($confrim)."&uid=".$uid;

ob_start();
include(F_PATH_SYS."tpl/emails/mail_confirmation.tpl.php");
$msg = ob_get_clean();
$e_mail = new Email($email, "Подтверждение почты", $msg);
$res = $e_mail->mailSend();

$now = date("Y-m-d H:i:s");
$unow = strtotime($now);

if ($res != 1) {
    $result = new AjaxResponse("error", "Ошибка отправки письма подтверждения: ".$res." (Повторно письмо можно отправить во время авторизации)");
    exit($result->json());
}


DB::con()->query("UPDATE `".F_DB_PREFIX."users` SET `updated` = ".DB::quote($unow)." WHERE `id`=".DB::quote($uid)) or die(Fianta::err(__FILE__, __LINE__));

$err = Auth::login(trim($email), $data["pass"]);
if (!empty($err)) {
    $result = new AjaxResponse("error", $err);

    exit($result->json());
} else {
    $result = new AjaxResponse("success", "Регистрация прошла успешно! Письмо с подтверждением отправлено на почту!\n\r");
    exit($result->json());
}
