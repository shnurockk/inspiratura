<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use Fianta\Core\AjaxResponse;
use Fianta\Core\DB;

if (!F_LOGGED) {
    $result = new AjaxResponse("error", "Доступ отсутствует");
    exit($result->json());
}
$stage = filter_input(INPUT_POST, 'stage');
$data = [];
$sdata = filter_input(INPUT_POST, 'data');
parse_str($sdata, $data);

if (empty($data)) {
    $result = new AjaxResponse("error", "Нету входящих данных!");
    exit($result->json());
}

$group_id = filter_input(INPUT_POST, 'group_id');
$group_id = empty($group_id) ? sha1(date(DATE_COOKIE).'='.\Fianta\Core\User::get()->id.'='.rand(0, 10000)) : $group_id;
if (!isset($group_id)) {
    $result = new AjaxResponse("error", "Идентификатор группы не указан!");
    exit($result->json());
}

if ($stage === 'info') {
    if (isset($data["name"])) {
        if (empty($data["name"])) {
            $result = new AjaxResponse("error", "Не заполнено название курса!");
            exit($result->json());
        }

        if (!isset($_SESSION['course'][$group_id]['name'])) {
            $_SESSION['course'][$group_id]['requirements'] = [['id' => null, 'desc' => '', 'deleted' => 0]];
            $_SESSION['course'][$group_id]['learns'] = [['id' => null, 'desc' => '', 'deleted' => 0]];
            $_SESSION['course'][$group_id]['audiences'] = [['id' => null, 'desc' => '', 'deleted' => 0]];
            $_SESSION['course'][$group_id]['audiences'] = [['id' => null, 'desc' => '', 'deleted' => 0]];
            $_SESSION['course'][$group_id]['sections'] = [];
            $_SESSION['course'][$group_id]['img'] = '';
        } else if (!isset($_SESSION['course'][$group_id]['id'])) {
            $response['next'] = 'cost';
        }

        $_SESSION['course'][$group_id]['name'] = $data["name"];
        $response['group_id'] = $group_id;
    }

    if(isset($data["image_name"])){
        $_SESSION['course'][$group_id]['img'] = $data['image_name'];
    }

    if (isset($data["requirements"])) {
        if (!isset($_SESSION['course'][$group_id]['requirements'])) {
            $_SESSION['course'][$group_id]['requirements'] = [];
        }
        foreach ($data["requirements"] as $k => $requirement) {
            if(!isset($_SESSION['course'][$group_id]['requirements'][$k])){
                $_SESSION['course'][$group_id]['requirements'][] = [];
            }
            $_SESSION['course'][$group_id]['requirements'][$k]['desc'] =  $requirement;
            $_SESSION['course'][$group_id]['requirements'][$k]['deleted'] = isset($data['requirement_deletes'][$k]) ? $data['requirement_deletes'][$k] : 0;
        }
    }
    if (isset($data["learns"])) {
        if (!isset($_SESSION['course'][$group_id]['learns'])) {
            $_SESSION['course'][$group_id]['learns'] = [];
        }
        foreach ($data["learns"] as $k => $l) {
            if(!isset($_SESSION['course'][$group_id]['learns'][$k])){
                $_SESSION['course'][$group_id]['learns'][] = [];
            }
            $_SESSION['course'][$group_id]['learns'][$k]['desc'] =  $l;
            $_SESSION['course'][$group_id]['learns'][$k]['deleted'] = isset($data['learn_deletes'][$k]) ? $data['learn_deletes'][$k] : 0;
        }
    }

    if (isset($data["audiences"])) {
        if (!isset($_SESSION['course'][$group_id]['audiences'])) {
            $_SESSION['course'][$group_id]['audiences'] = [];
        }
        foreach ($data["audiences"] as $k => $a) {
            if(!isset($_SESSION['course'][$group_id]['audiences'][$k])){
                $_SESSION['course'][$group_id]['audiences'][] = [];
            }

            $_SESSION['course'][$group_id]['audiences'][$k]['desc'] =  $a;
            $_SESSION['course'][$group_id]['audiences'][$k]['deleted'] = isset($data['audience_deletes'][$k]) ? $data['audience_deletes'][$k] : 0;
        }
    }
}

if ($stage === 'cost') {
    if (!isset($_SESSION['course'][$group_id]['cost'])) {
        $response['next'] = 'section';
    }
    if (isset($data["cost"])) {
        $_SESSION['course'][$group_id]['cost'] = $data["cost"];
    }
}

if ($stage === 'section') {
    if (!isset($_SESSION['course'][$group_id]['sections'])) {
        $_SESSION['course'][$group_id]['sections'] = [];
    }

    if (isset($data["section_index"])) {

        if (!isset($_SESSION['course'][$group_id]['sections'][$data["section_index"]])) {
            $result = new AjaxResponse("error", "Раздел с индексом '$data[section_index]' не найден!");
            exit($result->json());
        }

        if (!isset($_SESSION['course'][$group_id]['sections'][$data["section_index"]]['blocks'])) {
            $_SESSION['course'][$group_id]['sections'][$data["section_index"]]['blocks'] = [];
        }

        $index = count($_SESSION['course'][$group_id]['sections'][$data["section_index"]]['blocks']);
        if (!isset($data["index"]) || $data["index"] == '') {
            $data["index"] = $index;
        }

        $_SESSION['course'][$group_id]['sections'][$data["section_index"]]['blocks'][$data["index"]]['name'] = $data['name'];
        $_SESSION['course'][$group_id]['sections'][$data["section_index"]]['blocks'][$data["index"]]['attach'] = $data["attach"];
        $_SESSION['course'][$group_id]['sections'][$data["section_index"]]['blocks'][$data["index"]]['deleted'] = !isset($data["deleted"]) ? 0 : $data['deleted'];
        $_SESSION['course'][$group_id]['sections'][$data["section_index"]]['blocks'][$data["index"]]['index'] = $data['index'];
    } else {
        if (isset($data["name"]) && isset($data["desc"])) {
            $index = count($_SESSION['course'][$group_id]['sections']);
            if (!isset($data["index"]) || $data["index"] == '') {
                $data["index"] = $index;
            }

            $_SESSION['course'][$group_id]['sections'][$data["index"]]['name'] = $data['name'];
            $_SESSION['course'][$group_id]['sections'][$data["index"]]['desc'] = $data['desc'];
            $_SESSION['course'][$group_id]['sections'][$data["index"]]['deleted'] = !isset($data['deleted']) ? 0 : $data['deleted'];
            $_SESSION['course'][$group_id]['sections'][$data["index"]]['index'] = $data['index'];
        }
    }
}

$name = $_SESSION['course'][$group_id]['name'];
$response['course'][$group_id] = $_SESSION['course'][$group_id];
$response['group_id'] = $group_id;
$result = new AjaxResponse("success", "Курс '<b>$name</b>' успешно обновлен!", $response);
exit($result->json());