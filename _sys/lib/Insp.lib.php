<?php

namespace Fianta\Sys;

use Fianta\Core\Fianta;
use Fianta\Core\DB;
use Fianta\Core\QueryBuilder;
use Fianta\Core\User;
use PDO;

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

/*
 * FIANTA FRAMEWORK BY FEDOR YASTREBOV
 * Запрещено использование данного программного кода или его части в любых целях без согласия автора
 * COPYRIGHT © 2013-2017 
 * http://fianta.com 
 * EMAIL: adm@fianta.com
 */

class Insp {

    public function __construct() {
        
    }

    public function authTelegram($uid, $t_uid) {
        $ps = DB::con()->prepare("UPDATE `".F_DB_PREFIX."telegram_users` SET `uid`=:uid WHERE `t_uid`=:t_uid") or die(Fianta::err(__FILE__, __LINE__));
        $ps->bindParam(":uid", $uid);
        $ps->bindParam(":t_uid", $t_uid);
        $ps->execute();
    }

    public function updateTelegram($phone, $t_uid) {
        $ps = DB::con()->prepare("UPDATE `".F_DB_PREFIX."telegram_users` SET `phone`=:phone WHERE `t_uid`=:t_uid") or die(Fianta::err(__FILE__, __LINE__));
        $ps->bindParam(":phone", $phone);
        $ps->bindParam(":t_uid", $t_uid);
        $ps->execute();
    }

    public function getTelegram($uid) {
        $q = DB::con()->query("SELECT * FROM `".F_DB_PREFIX."telegram_users` WHERE `uid`=".DB::quote($uid)) or die(Fianta::err(__FILE__, __LINE__));
        $res = $q->fetch(PDO::FETCH_ASSOC);
        return $res;
    }

    public function addUser($phone, $t_uid, $name) {
        $ps = DB::con()->prepare("INSERT INTO `".F_DB_PREFIX."telegram_users` (`t_uid`,`phone`,`name`) VALUES (:t_uid, :phone, :name) ON DUPLICATE KEY UPDATE `name`=:name, `phone`=:phone") or die(Fianta::err(__FILE__, __LINE__));
        $ps->bindParam(":t_uid", $t_uid);
        $ps->bindParam(":name", $name);
        $ps->bindParam(":phone", $phone);
        $ps->execute();
    }

    public function checkUser($t_uid) {
        $q = DB::con()->query("SELECT * FROM `".F_DB_PREFIX."telegram_users` WHERE `t_uid`=".DB::quote($t_uid)) or die(Fianta::err(__FILE__, __LINE__));
        $res = $q->fetch(PDO::FETCH_ASSOC);
        if (empty($res)) {
            return false;
        }
        return $res;
    }

    public function userGroupConnect($group_id, $stat_id, $t_uid) {
        DB::con()->query("UPDATE `".F_DB_PREFIX."telegram_users` SET `group_id`=".DB::quote($group_id).",`stage`=0,`stat_id`=".DB::quote($stat_id)." WHERE `t_uid`=".DB::quote($t_uid)) or die(Fianta::err(__FILE__, __LINE__));
    }

    public function setStage($stage, $t_uid) {
        DB::con()->query("UPDATE `".F_DB_PREFIX."telegram_users` SET `stage`=".DB::quote($stage)." WHERE `t_uid`=".DB::quote($t_uid)) or die(Fianta::err(__FILE__, __LINE__));
    }

    public function checkUserGroupConnect($t_uid) {
        $q = DB::con()->query("SELECT `group_id` FROM `".F_DB_PREFIX."telegram_users` WHERE `t_uid`=".DB::quote($t_uid)) or die(Fianta::err(__FILE__, __LINE__));
        $res = $q->fetch(PDO::FETCH_ASSOC);
        if (empty($res) or ( isset($res["group_id"]) and $res["group_id"] == 0)) {
            return false;
        }
        return $res["group_id"];
    }

    public function unauthTelegram($uid) {
        $ps = DB::con()->prepare("UPDATE `".F_DB_PREFIX."telegram_users` SET `uid`='' WHERE `uid`=:uid") or die(Fianta::err(__FILE__, __LINE__));
        $ps->bindParam(":uid", $uid);
        $ps->execute();
    }

    public function addGroup($params, $blocks) {
        $qb = new QueryBuilder($params);
        DB::con()->query("INSERT INTO `".F_DB_PREFIX."groups` (".$qb->insert_fields.") VALUES (".$qb->insert_values.")") or die(Fianta::err(__FILE__, __LINE__));
        $group_id = DB::con()->lastInsertId();
        $ps = DB::con()->prepare("INSERT INTO `".F_DB_PREFIX."words` (`group_id`,`word`,`translate`) VALUES (:group_id, :word, :translate)") or die(Fianta::err(__FILE__, __LINE__));
        foreach ($blocks["words"] as $i => $w) {
            $ps->bindParam(":group_id", $group_id);
            $ps->bindParam(":word", $w);
            $ps->bindParam(":translate", $blocks["translate"][$i]);
            $ps->execute();
        }
    }

    public function getGroups($uid) {
        $res = [];
        $q = DB::con()->query("SELECT * FROM `".F_DB_PREFIX."groups` WHERE `uid`!=".DB::quote($uid)." AND `opened`=1") or die(Fianta::err(__FILE__, __LINE__));
        while ($rw = $q->fetch(PDO::FETCH_ASSOC)) {
            $q2 = DB::con()->query("SELECT * FROM `".F_DB_PREFIX."words` WHERE `group_id`=".DB::quote($rw["id"])) or die(Fianta::err(__FILE__, __LINE__));
            $words = [];
            $translate = [];
            while ($rw2 = $q2->fetch(PDO::FETCH_ASSOC)) {
                $words[] = $rw2["word"];
                $translate[] = $rw2["translate"];
            }
            $res[] = [
                "id" => $rw["id"],
                "words" => $words,
                "translate" => $translate,
                "name" => $rw["name"],
            ];
        }
        return $res;
    }

    public function getGroupByUser($uid) {
        $res = [];
        $q = DB::con()->query("SELECT * FROM `".F_DB_PREFIX."groups` WHERE `uid`=".DB::quote($uid)) or die(Fianta::err(__FILE__, __LINE__));
        while ($rw = $q->fetch(PDO::FETCH_ASSOC)) {
            $q2 = DB::con()->query("SELECT * FROM `".F_DB_PREFIX."words` WHERE `group_id`=".DB::quote($rw["id"])) or die(Fianta::err(__FILE__, __LINE__));
            $words = [];
            $translate = [];
            while ($rw2 = $q2->fetch(PDO::FETCH_ASSOC)) {
                $words[] = $rw2["word"];
                $translate[] = $rw2["translate"];
            }
            $res[] = [
                "id" => $rw["id"],
                "words" => $words,
                "translate" => $translate,
                "name" => $rw["name"],
            ];
        }
        return $res;
    }

    public function getSubGroupByUser($uid) {
        $res = [];
        $q = DB::con()->query("SELECT g.`id`,g.`name` FROM `".F_DB_PREFIX."groups` g JOIN `".F_DB_PREFIX."shared_groups` s ON s.`group_id`=g.`id` WHERE s.`uid`=".DB::quote($uid)) or die(Fianta::err(__FILE__, __LINE__));
        while ($rw = $q->fetch(PDO::FETCH_ASSOC)) {
            $q2 = DB::con()->query("SELECT * FROM `".F_DB_PREFIX."words` WHERE `group_id`=".DB::quote($rw["id"])) or die(Fianta::err(__FILE__, __LINE__));
            $words = [];
            $translate = [];
            while ($rw2 = $q2->fetch(PDO::FETCH_ASSOC)) {
                $words[] = $rw2["word"];
                $translate[] = $rw2["translate"];
            }
            $res[] = [
                "id" => $rw["id"],
                "words" => $words,
                "translate" => $translate,
                "name" => $rw["name"],
            ];
        }
        return $res;
    }

    public function getGroupByTUser($t_uid) {
        $q = DB::con()->query("SELECT g.`id`,g.`name` FROM `".F_DB_PREFIX."groups` g"
                ." JOIN `".F_DB_PREFIX."telegram_users` t ON t.`uid`=g.`uid`  WHERE t.`t_uid`=".DB::quote($t_uid)) or die(Fianta::err(__FILE__, __LINE__));
        $res = $q->fetchAll(PDO::FETCH_ASSOC);
        return $res;
    }

    public function getSharedGroupByTUser($t_uid) {
        $q = DB::con()->query("SELECT g.`id`,g.`name` FROM `".F_DB_PREFIX."groups` g"
                ." JOIN `".F_DB_PREFIX."shared_groups` s ON s.`group_id`=g.`id` JOIN `".F_DB_PREFIX."telegram_users` t ON t.`uid`=s.`uid` WHERE t.`t_uid`=".DB::quote($t_uid)) or die(Fianta::err(__FILE__, __LINE__));
        $res = $q->fetchAll(PDO::FETCH_ASSOC);
        return $res;
    }

    public function checkGroupForShared($group_id, $t_uid) {
        $user = $this->checkUser($t_uid);
        $q = DB::con()->query("SELECT `uid` FROM `".F_DB_PREFIX."groups` WHERE `id`=".DB::quote($group_id)) or die(Fianta::err(__FILE__, __LINE__));
        $res = $q->fetch(PDO::FETCH_ASSOC);
        if (!empty($res) and $res["uid"] == $user["uid"]) {
            return false;
        }
        return true;
    }

    public function getGroupById($id) {
        $q = DB::con()->query("SELECT * FROM `".F_DB_PREFIX."groups` WHERE `id`=".DB::quote($id)) or die(Fianta::err(__FILE__, __LINE__));
        $res = $q->fetch(PDO::FETCH_ASSOC);
        $q2 = DB::con()->query("SELECT * FROM `".F_DB_PREFIX."words` WHERE `group_id`=".DB::quote($id)." ORDER BY `id` ASC") or die(Fianta::err(__FILE__, __LINE__));
        while ($rw = $q2->fetch(PDO::FETCH_ASSOC)) {
            $res["words"][] = [
                "id" => $rw["id"],
                "word" => $rw["word"],
                "translate" => $rw["translate"],
            ];
        }
        return $res;
    }

    public function editGroup($id, $params, $blocks, $new_blocks, $all_blocks) {
        $qb = new QueryBuilder($params);
        DB::con()->query("UPDATE `".F_DB_PREFIX."groups` SET ".$qb->update_string." WHERE `id`=".DB::quote($id)) or die(Fianta::err(__FILE__, __LINE__));
        $ps = DB::con()->prepare("UPDATE `".F_DB_PREFIX."words` SET `word`=:word,`translate`=:translate WHERE `id`=:id") or die(Fianta::err(__FILE__, __LINE__));
        foreach ($blocks as $b) {
            $now_blocks[] = $b["id"];
            $ps->bindParam(":id", $b["id"]);
            $ps->bindParam(":word", $b["word"]);
            $ps->bindParam(":translate", $b["translate"]);
            $ps->execute();
        }
        $del_blocks = array_diff($all_blocks, $now_blocks);
        //return $del_blocks;
        if (!empty($del_blocks)) {
            DB::con()->query("DELETE FROM `".F_DB_PREFIX."words` WHERE `id` IN (".implode(",", $del_blocks).")") or die(Fianta::err(__FILE__, __LINE__));
        }
        if (!empty($new_blocks)) {
            $ps2 = DB::con()->prepare("INSERT INTO `".F_DB_PREFIX."words` (`group_id`,`word`,`translate`) VALUES (:group_id, :word, :translate)") or die(Fianta::err(__FILE__, __LINE__));
            foreach ($new_blocks as $b) {
                $ps2->bindParam(":group_id", $id);
                $ps2->bindParam(":word", $b["word"]);
                $ps2->bindParam(":translate", $b["translate"]);

                $ps2->execute();
            }
        }
    }

    public function getAnswers($words, $right_answer) {
        $res = [];
        $trans = [];
        foreach ($words as $w) {
            $trans[] = $w["translate"];
        }
        $count = count($trans) - 1;
        $r_index = random_int(0, 3);
        $res[$r_index] = $right_answer;
        $n = 0;
        while ($n <= 3) {
            if (isset($res[$n])) {
                $n++;
                continue;
            }
            $index = random_int(0, $count);
            if (!isset($trans[$index])) {
                continue;
            }
            $answer = $trans[$index];
            if (!in_array($answer, $res)) {
                $res[$n] = $answer;
                unset($trans[$index]);
                $count = $count - 1;
                $n++;
            }
        }
        return $res;
    }

    public function addNewStat($group_id, $t_uid) {
        $now = strtotime(date("d.m.Y H:i:s"));
        $ps2 = DB::con()->prepare("INSERT INTO `".F_DB_PREFIX."stat` (`group_id`,`t_uid`,`date`) VALUES (:group_id, :t_uid, :date)") or die(Fianta::err(__FILE__, __LINE__));
        $ps2->bindParam(":group_id", $group_id);
        $ps2->bindParam(":t_uid", $t_uid);
        $ps2->bindParam(":date", $now);
        $ps2->execute();
        return DB::con()->lastInsertId();
    }

    public function getStatById($id, $only_wrong = false) {
        $words = [];
        $q = DB::con()->query("SELECT s.*,g.`name` FROM `".F_DB_PREFIX."stat` s"
                ." JOIN `".F_DB_PREFIX."groups` g ON g.`id`=s.`group_id`  WHERE s.`id`=".DB::quote($id)) or die(Fianta::err(__FILE__, __LINE__));
        $stat = $q->fetch(PDO::FETCH_ASSOC);
        if (empty($stat)) {
            return false;
        }
        $q2 = DB::con()->query("SELECT * FROM `".F_DB_PREFIX."words` WHERE `group_id`=".DB::quote($stat["group_id"])) or die(Fianta::err(__FILE__, __LINE__));
        while ($rw = $q2->fetch(PDO::FETCH_ASSOC)) {
            $words[$rw["id"]]["word"] = $rw["word"];
            $words[$rw["id"]]["translate"] = $rw["translate"];
        }
        $q3 = DB::con()->query("SELECT * FROM `".F_DB_PREFIX."stat_answers` WHERE `stat_id`=".DB::quote($stat["id"])) or die(Fianta::err(__FILE__, __LINE__));
        $answers = $q3->fetchAll(PDO::FETCH_ASSOC);
        $n = 0;
        foreach ($answers as $a) {
            if ($a["Y"] == 1) {
                $n++;
            }
            if ($only_wrong and $a["Y"] == 0) {
                $stat["stat"][] = [
                    "word" => $words[$a["word_id"]]["word"],
                    "translate" => $words[$a["word_id"]]["translate"],
                    "word_id" => $a["word_id"],
                    "val" => $a["Y"]
                ];
            } elseif (!$only_wrong) {
                $stat["stat"][] = [
                    "word" => $words[$a["word_id"]]["word"],
                    "translate" => $words[$a["word_id"]]["translate"],
                    "word_id" => $a["word_id"],
                    "val" => $a["Y"]
                ];
            }
        }
        $stat["good_answers"] = $n;
        return $stat;
    }

    public function getStatByGroup($group_id) {
        $q = DB::con()->query("SELECT * FROM `".F_DB_PREFIX."stat` WHERE `group_id`=".DB::quote($group_id)." ORDER BY `date` DESC") or die(Fianta::err(__FILE__, __LINE__));
        $stats = $q->fetchAll(PDO::FETCH_ASSOC);
        return $stats;
    }

    public function getSharedLastStat($t_uid, $group_id) {
        $user = $this->checkUser($t_uid);
        $q = DB::con()->query("SELECT `last_stat` FROM `".F_DB_PREFIX."shared_groups` WHERE `uid`=".DB::quote($user["uid"])." AND `group_id`=".DB::quote($group_id)) or die(Fianta::err(__FILE__, __LINE__));
        $stat = $q->fetch(PDO::FETCH_ASSOC);
        return $stat["last_stat"];
    }

    public function setLastStat($t_uid, $group_id, $shared = false) {
        $q = DB::con()->query("SELECT `id` FROM `".F_DB_PREFIX."stat` WHERE `t_uid`=".DB::quote($t_uid)." AND `group_id`=".DB::quote($group_id)." ORDER BY `date` DESC LIMIT 1") or die(Fianta::err(__FILE__, __LINE__));
        $stat = $q->fetch(PDO::FETCH_ASSOC);
        if ($shared) {
            $user = $this->checkUser($t_uid);
            DB::con()->query("UPDATE `".F_DB_PREFIX."shared_groups` SET `last_stat`=".DB::quote($stat["id"])." WHERE `group_id`=".DB::quote($group_id)." AND `uid`=".DB::quote($user["uid"])) or die(Fianta::err(__FILE__, __LINE__));
        } else {
            DB::con()->query("UPDATE `".F_DB_PREFIX."groups` SET `last_stat`=".DB::quote($stat["id"])." WHERE `id`=".DB::quote($group_id)) or die(Fianta::err(__FILE__, __LINE__));
        }
        return $stat["id"];
    }

    public function addStatAnswer($stat_id, $word_id, $val) {
        $ps2 = DB::con()->prepare("INSERT INTO `".F_DB_PREFIX."stat_answers` (`stat_id`,`word_id`,`Y`) VALUES (:stat_id, :word_id, :val)") or die(Fianta::err(__FILE__, __LINE__));
        $ps2->bindParam(":stat_id", $stat_id);
        $ps2->bindParam(":word_id", $word_id);
        $ps2->bindParam(":val", $val);
        $ps2->execute();
    }

    public function setRepeat($group_id, $hours, $t_uid, $shared = false) {
        $dt = date("d.m.Y H:i:s");
        $d = new \DateTime($dt);
        $d->modify("+".$hours." hour");
        $to = $d->getTimestamp();
        if ($shared) {
            $user = $this->checkUser($t_uid);
            DB::con()->query("UPDATE `".F_DB_PREFIX."shared_groups` SET `repeat_date`=".DB::quote($to).", `repeated`=0 WHERE `group_id`=".DB::quote($group_id)." AND `uid`=".DB::quote($user["uid"])) or die(Fianta::err(__FILE__, __LINE__));
        } else {
            DB::con()->query("UPDATE `".F_DB_PREFIX."groups` SET `repeat_date`=".DB::quote($to).", `repeated`=0 WHERE `id`=".DB::quote($group_id)) or die(Fianta::err(__FILE__, __LINE__));
        }
    }

    public function findCourse($word, $author, $filter_cost, $filter_bought) {
        if(!empty($author)){
            if(($pos = mb_strpos($author, 'id:')) !== false) {
                $author = substr($author, 3);
                $author = DB::quote($author);
                $author = "`g`.`uid`=$author AND";
            } else {
                $author = "`u`.`fio` LIKE '%".$author."%' AND";
            }
        }
        $order = "";
        if($filter_cost > 0){
            if($filter_cost < 2){
                $order = 'ASC';
            } else if($filter_cost == 2) {
                $order = 'DESC';
            }

            if(!empty($order)) {
                $order = "ORDER BY `g`.`cost` $order";
            }
        }

        $bought_join = '';
        $bought_condition = '';
        if($filter_bought <= 2) {
            if ($filter_bought <= 1) {
                $bought_join = 'LEFT ';
            }

            $uid = User::get()->id;

            $bought_join .= "JOIN `".F_DB_PREFIX."users_course_bought` `ucb` ON `ucb`.`uid`=$uid AND `ucb`.`course_id`=`g`.`id`";
            if($filter_bought == 1){
                $bought_condition = '`ucb`.`id` IS NULL AND';
            }
        }

        $q = DB::con()->query("SELECT `g`.*,`u`.`fio` FROM `".F_DB_PREFIX."groups_course` `g`"
            ." JOIN `".F_DB_PREFIX."users` `u` ON `g`.`uid`=`u`.`id`"
            ." $bought_join"
            ." WHERE $bought_condition $author `g`.`name` LIKE '%".$word."%' $order") or die(Fianta::err(__FILE__, __LINE__));
        $c = $q->fetchAll(PDO::FETCH_ASSOC);
        return $c;
    }

    public function getCourses($user_id, $group_id = null, $joinParams = []){
        $condition = '';
        if(!empty($group_id)) {
            $condition = '`id`='.DB::quote($group_id).' AND ';
        }

        $q = DB::con()->query('SELECT * FROM `'.F_DB_PREFIX.'groups_course` WHERE '.$condition.'`uid`='.DB::quote($user_id))  or die(Fianta::err(__FILE__, __LINE__));
        $result = $q->fetchAll(PDO::FETCH_ASSOC);
        if(empty($result) || !isset($result[0]['id'])){
            return false;
        }


        if(!empty($joinParams)){
            $new_result = [];
            foreach ($result as $g) {
                if (in_array('learn', $joinParams) || in_array('all', $joinParams)) {
                    $q = DB::con()->query('SELECT `id`,`desc` FROM `'.F_DB_PREFIX.'groups_course_learn` WHERE `group_id`='.DB::quote($g['id']).' ORDER BY `index`')  or die(Fianta::err(__FILE__, __LINE__));
                    $learns = $q->fetchAll(PDO::FETCH_ASSOC);
                    $g['learns'] = $learns;
                    foreach ($g['learns'] as $k => $l){
                        $g['learns'][$k]['deleted'] = 0;
                    }
                }

                if (in_array('section', $joinParams) || in_array('attachment', $joinParams) || in_array('all', $joinParams)) {
                    $q = DB::con()->query('SELECT `id`,`name`,`desc` FROM `'.F_DB_PREFIX.'groups_course_sections` WHERE `group_id`='.DB::quote($g['id']).' ORDER BY `index`')  or die(Fianta::err(__FILE__, __LINE__));
                    $sections = $q->fetchAll(PDO::FETCH_ASSOC);
                    $g['sections'] = $sections;
                    foreach ($sections as $i => $section) {
                        $g['sections'][$i]['deleted'] = 0;

                        $q = DB::con()->query('SELECT `id`,`name`,`attach`,`time` FROM `'.F_DB_PREFIX.'groups_course_section_blocks` WHERE `section_id`='.DB::quote($section['id']).' ORDER BY `index`')  or die(Fianta::err(__FILE__, __LINE__));
                        $blocks = $q->fetchAll(PDO::FETCH_ASSOC);
                        $g['sections'][$i]['blocks'] = $blocks;

                        foreach ($g['sections'][$i]['blocks'] as $k => $attachment){
                            $g['sections'][$i]['blocks'][$k]['deleted'] = 0;
                        }
                    }
                }

                if (in_array('requirements', $joinParams) || in_array('all', $joinParams)) {
                    $q = DB::con()->query('SELECT `id`,`desc` FROM `'.F_DB_PREFIX.'groups_course_requirements` WHERE `group_id`='.DB::quote($g['id']).' ORDER BY `index`') or die(Fianta::err(__FILE__, __LINE__));
                    $requirements = $q->fetchAll(PDO::FETCH_ASSOC);
                    $g['requirements'] = $requirements;
                    foreach ($g['requirements'] as $k => $requirement){
                        $g['requirements'][$k]['deleted'] = 0;
                    }
                }

                if (in_array('audiences', $joinParams) || in_array('all', $joinParams)) {
                    $q = DB::con()->query('SELECT `id`,`desc` FROM `'.F_DB_PREFIX.'groups_course_audiences` WHERE `group_id`='.DB::quote($g['id']).' ORDER BY `index`') or die(Fianta::err(__FILE__, __LINE__));
                    $audiences = $q->fetchAll(PDO::FETCH_ASSOC);
                    $g['audiences'] = $audiences;
                    foreach ($g['audiences'] as $k => $audience){
                        $g['audiences'][$k]['deleted'] = 0;
                    }
                }
                $new_result[] = $g;
            }
            return $new_result;
        }
        return $result;
    }

    public function getCourse($group_id = null, $joinParams = [])
    {
        $q = DB::con()->query('SELECT * FROM `'.F_DB_PREFIX.'groups_course` WHERE `id`='.DB::quote($group_id)) or die(Fianta::err(__FILE__, __LINE__));
        $result = $q->fetch(PDO::FETCH_ASSOC);
        if (empty($result) || !isset($result['id'])) {
            return false;
        }

        $g = $result;

        if (!empty($joinParams)) {
            if (in_array('learn', $joinParams) || in_array('all', $joinParams)) {
                $q = DB::con()->query('SELECT `id`,`desc` FROM `'.F_DB_PREFIX.'groups_course_learn` WHERE `group_id`='.DB::quote($g['id']).' ORDER BY `index`') or die(Fianta::err(__FILE__, __LINE__));
                $learns = $q->fetchAll(PDO::FETCH_ASSOC);
                $g['learns'] = $learns;
                foreach ($g['learns'] as $k => $l) {
                    $g['learns'][$k]['deleted'] = 0;
                }
            }

            if (in_array('section', $joinParams) || in_array('attachment', $joinParams) || in_array('all', $joinParams)) {
                $q = DB::con()->query('SELECT `id`,`name`,`desc` FROM `'.F_DB_PREFIX.'groups_course_sections` WHERE `group_id`='.DB::quote($g['id']).' ORDER BY `index`') or die(Fianta::err(__FILE__, __LINE__));
                $sections = $q->fetchAll(PDO::FETCH_ASSOC);
                $g['sections'] = $sections;
                foreach ($sections as $i => $section) {
                    $g['sections'][$i]['deleted'] = 0;

                    $q = DB::con()->query('SELECT `id`,`name`,`attach`,`time` FROM `'.F_DB_PREFIX.'groups_course_section_blocks` WHERE `section_id`='.DB::quote($section['id']).' ORDER BY `index`') or die(Fianta::err(__FILE__, __LINE__));
                    $blocks = $q->fetchAll(PDO::FETCH_ASSOC);
                    $g['sections'][$i]['blocks'] = $blocks;

                    foreach ($g['sections'][$i]['blocks'] as $k => $attachment) {
                        $g['sections'][$i]['blocks'][$k]['deleted'] = 0;
                    }
                }
            }

            if (in_array('requirements', $joinParams) || in_array('all', $joinParams)) {
                $q = DB::con()->query('SELECT `id`,`desc` FROM `'.F_DB_PREFIX.'groups_course_requirements` WHERE `group_id`='.DB::quote($g['id']).' ORDER BY `index`') or die(Fianta::err(__FILE__, __LINE__));
                $requirements = $q->fetchAll(PDO::FETCH_ASSOC);
                $g['requirements'] = $requirements;
                foreach ($g['requirements'] as $k => $requirement) {
                    $g['requirements'][$k]['deleted'] = 0;
                }
            }

            if (in_array('audiences', $joinParams) || in_array('all', $joinParams)) {
                $q = DB::con()->query('SELECT `id`,`desc` FROM `'.F_DB_PREFIX.'groups_course_audiences` WHERE `group_id`='.DB::quote($g['id']).' ORDER BY `index`') or die(Fianta::err(__FILE__, __LINE__));
                $audiences = $q->fetchAll(PDO::FETCH_ASSOC);
                $g['audiences'] = $audiences;
                foreach ($g['audiences'] as $k => $audience) {
                    $g['audiences'][$k]['deleted'] = 0;
                }
            }
        }

        return $g;
    }

    /**
     * Возвращает купленный курс пользователя
     * @param $uid
     * @param $id
     * @return bool|mixed
     */
    public function getUserCourse($uid, $id){
        $q = DB::con()->query('SELECT gc.* FROM `'.F_DB_PREFIX.'users_course_bought` u JOIN `'.F_DB_PREFIX.'groups_course` gc ON gc.`id`=u.`course_id` WHERE u.`id`='.DB::quote($id).' AND u.`uid`='.DB::quote($uid))  or die(Fianta::err(__FILE__, __LINE__));
        $result = $q->fetch(PDO::FETCH_ASSOC);
        if(!isset($result['id']) || empty($result)){
            return false;
        }

        $g = $result;

        $q = DB::con()->query('SELECT `id`,`name`,`desc` FROM `'.F_DB_PREFIX.'groups_course_sections` WHERE `group_id`='.DB::quote($g['id']).' ORDER BY `index`')  or die(Fianta::err(__FILE__, __LINE__));
        $sections = $q->fetchAll(PDO::FETCH_ASSOC);
        $g['sections'] = $sections;

        foreach ($sections as $i => $section) {
            $q = DB::con()->query('SELECT `id`,`name`,`attach`,`time` FROM `'.F_DB_PREFIX.'groups_course_section_blocks` WHERE `section_id`='.DB::quote($section['id']).' ORDER BY `index`')  or die(Fianta::err(__FILE__, __LINE__));
            $blocks = $q->fetchAll(PDO::FETCH_ASSOC);
            $g['sections'][$i]['blocks'] = $blocks;
        }

        return $g;
    }

    public function getUserBlockAttach($uid, $id, $section_id, $block_id){
        $q = DB::con()->query('SELECT gcsb.`attach` FROM `'.F_DB_PREFIX.'users_course_bought` u JOIN `'.F_DB_PREFIX.'groups_course_sections` gcs ON gcs.`group_id`=u.`course_id` AND gcs.`id`='.DB::quote($section_id).' JOIN `'.F_DB_PREFIX.'groups_course_section_blocks` gcsb ON gcsb.`section_id`=gcs.`id` AND gcsb.`id`='.DB::quote($block_id).' WHERE u.`id`='.DB::quote($id).' AND u.`uid`='.DB::quote($uid))  or die(Fianta::err(__FILE__, __LINE__));
        $result = $q->fetch(PDO::FETCH_ASSOC);
        if(!isset($result['attach'])){
            return false;
        }

        return $result;
    }

    public function setCourseProgress($uid, $id, $section_id, $block_id, $done = 0){
        $q = DB::con()->query('SELECT COUNT(gcsb.`id`) FROM `'.F_DB_PREFIX.'users_course_bought` u JOIN `'.F_DB_PREFIX.'groups_course_sections` gcs ON gcs.`group_id`=u.`course_id` AND gcs.`id`='.DB::quote($section_id).' JOIN `'.F_DB_PREFIX.'groups_course_section_blocks` gcsb ON gcsb.`section_id`=gcs.`id` AND gcsb.`id`='.DB::quote($block_id).' WHERE u.`id`='.DB::quote($id).' AND u.`uid`='.DB::quote($uid))  or die(Fianta::err(__FILE__, __LINE__));
        $result = $q->fetch();
        if(!isset($result[0]) || empty($result[0])){
            return false;
        }

        if($done == 0){
            $q = DB::con()->query('DELETE FROM `'.F_DB_PREFIX.'users_course_progress` WHERE `bought_id`='.DB::quote($id).' AND `section_id`='.DB::quote($section_id).' AND `block_id`='.DB::quote($block_id))  or die(Fianta::err(__FILE__, __LINE__));
            return true;
        } elseif($done == 1) {
            $q = DB::con()->query('INSERT INTO `'.F_DB_PREFIX.'users_course_progress` (`bought_id`,`section_id`,`block_id`) VALUES ('.DB::quote($id).','.DB::quote($section_id).','.DB::quote($block_id).') ON DUPLICATE KEY UPDATE `bought_id`='.DB::quote($id).', `section_id`='.DB::quote($section_id).', `block_id`='.DB::quote($block_id))  or die(Fianta::err(__FILE__, __LINE__));
            return true;
        }

        return false;
    }

    public function getCourseProgressCount($uid, $id, $section_id){
        $q = DB::con()->query('SELECT COUNT(ucp.`bought_id`) as current, COUNT(gcsb.`id`) as total FROM `'.F_DB_PREFIX.'users_course_bought` u RIGHT JOIN `'.F_DB_PREFIX.'groups_course_sections` gcs ON gcs.`group_id`=u.`course_id` AND gcs.`id`='.DB::quote($section_id).' INNER JOIN `'.F_DB_PREFIX.'groups_course_section_blocks` gcsb ON gcsb.`section_id`=gcs.`id` LEFT JOIN `'.F_DB_PREFIX.'users_course_progress` ucp ON ucp.`bought_id`=u.`id` AND ucp.`section_id`=gcs.`id` AND ucp.`block_id`=gcsb.`id` WHERE u.`id`='.DB::quote($id).' AND u.`uid`='.DB::quote($uid))  or die(Fianta::err(__FILE__, __LINE__));
        $result = $q->fetch(PDO::FETCH_ASSOC);
        if(!isset($result['total']) || empty($result['total'])){
            return false;
        }

        return $result;
    }

    public function getCourseAllProgressCount($uid, $id){
        $q = DB::con()->query('SELECT gcs.`id` as section_id, COUNT(ucp.`bought_id`) as current, COUNT(gcsb.`id`) as total FROM `'.F_DB_PREFIX.'users_course_bought` u RIGHT JOIN `'.F_DB_PREFIX.'groups_course_sections` gcs ON gcs.`group_id`=u.`course_id` INNER JOIN `'.F_DB_PREFIX.'groups_course_section_blocks` gcsb ON gcsb.`section_id`=gcs.`id` LEFT JOIN `'.F_DB_PREFIX.'users_course_progress` ucp ON ucp.`bought_id`=u.`id` AND ucp.`section_id`=gcs.`id` AND ucp.`block_id`=gcsb.`id` WHERE u.`id`='.DB::quote($id).' AND u.`uid`='.DB::quote($uid).' GROUP BY gcs.`id`')  or die(Fianta::err(__FILE__, __LINE__));
        $result = $q->fetchAll(PDO::FETCH_ASSOC);
        if(!isset($result[0])){
            return false;
        }

        return $result;
    }

    public function getCourseAllProgress($uid, $id){
        $q = DB::con()->query('SELECT gcs.`id`, ucp.`block_id` FROM `'.F_DB_PREFIX.'users_course_bought` u LEFT JOIN `'.F_DB_PREFIX.'groups_course_sections` gcs ON gcs.`group_id`=u.`course_id` INNER JOIN `'.F_DB_PREFIX.'groups_course_section_blocks` gcsb ON gcsb.`section_id`=gcs.`id` RIGHT JOIN `'.F_DB_PREFIX.'users_course_progress` ucp ON ucp.`bought_id`=u.`id` AND ucp.`section_id`=gcs.`id` AND ucp.`block_id`=gcsb.`id` WHERE u.`id`='.DB::quote($id).' AND u.`uid`='.DB::quote($uid))  or die(Fianta::err(__FILE__, __LINE__));
        $result = $q->fetchAll(PDO::FETCH_COLUMN|PDO::FETCH_GROUP);
        if(empty($result)){
            return false;
        }

        return $result;
    }

    public function getUserBoughtCourse($uid, $course_id){
        $q = DB::con()->query('SELECT * FROM `'.F_DB_PREFIX.'users_course_bought` WHERE `uid`='.DB::quote($uid).' AND `course_id`='.DB::quote($course_id))  or die(Fianta::err(__FILE__, __LINE__));
        $result = $q->fetch(PDO::FETCH_ASSOC);
        if(!isset($result['id'])){
            return false;
        }

        return $result;
    }
}
