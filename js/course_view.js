$window = $(window);
$window.scroll(function() {
    var $boxFix = $('.js-pin-header');

    if ($window.scrollTop() > 200) {
        $boxFix.addClass('pin-header-show');
        $boxFix.removeClass('pin-header-hide');
    } else {
        $boxFix.addClass('pin-header-hide');
        $boxFix.removeClass('pin-header-show');
    }
});

$window.resize(function () {
    console.log($('body').width());
});