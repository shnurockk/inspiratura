$(function() {

    $('#admin_exit').on('click', '#admin_signout', function(e) {
        e.preventDefault();
       
       butn = $("#admin_exit").html();
        $("#admin_exit").html("<img src='/css/_admin/img/upload.gif' alt='Загрузка'>");

        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            handler: "auth_logout", 
        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch(err){
                $.sweetModal({
                    content: r,
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }
            if (result.status == "success") {
                url = $('#admin_url').attr("content");
                window.location.href = url;
            } else {
                $("#admin_exit").html(butn);
            }
        });
    });
    
});