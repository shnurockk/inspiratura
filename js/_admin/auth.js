function auth_act(auth_type, href) {
    href = href || $("#auth_href").val();
    $("#auth_err").css("display", "none");

    if (auth_type == "1") {
        
    } else {
        butn = $("#auth_login_load").html();
        $("#auth_login_load").html("<img src='css/img/upload.gif' alt='Загрузка'>");
        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            handler: "auth", 
            auth_type: auth_type
        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch(err){
                $.sweetModal({
                    content: r,
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }
            if (result.status == "success") {
                window.location.href = href;
            } 
        });
    }
}




function ar_err(err, div) {
    div.html(err);
    div.css("display", "block");
}
function show_info(mes, type) {
    if (type) {
        type_id = 'success';
    } else {
        type_id = 'error';
    }
    new PNotify({
        title: "Повідомлення",
        text: mes,
        type: type_id
    });
    $.post("/_ajax.php", {handler: "adm", action: "clear_notify",  token: window.f_token});
    
}