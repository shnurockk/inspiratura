'use strict';

/* eslint-disable no-unused-vars */

var $window = $(window);
var $document = $(document);
var $html = $(document.documentElement);
var $body = $(document.body);

// fixed header 
$window.scroll(function() {
	var $boxFix = $('.js-fix');
	if ($(window).scrollTop() > 25) {
		$boxFix.addClass('fix');
	} else {
		$boxFix.removeClass('fix');
	}
});

// btn menu
var $btnMenu = $('.js-btn-menu');
var $boxMenu = $('.js-box-menu');
$window.resize(function() {
	if ($window.width() > 990) {
		$btnMenu.removeClass('is-open');
		$boxMenu.css('display', 'block');
	} else {
		$boxMenu.css('display', 'none');
	}
});
$btnMenu.click(function() {
	$(this).toggleClass('is-open');
	$boxMenu.slideToggle(600);
});

// modal
$('.js-modal').fancybox({
	toolbar: false,
	smallBtn: false,
	animationEffect: 'zoom-in-out',
	touch: false,
	autoFocus: false,
	arrows: false,
	beforeLoad: function beforeLoad(instance, current) {
		fullpage_api.setAutoScrolling(false);
	},
	beforeClose: function beforeClose(instance, current) {
		fullpage_api.setAutoScrolling(true);
	}
});

// form styler
var $jsFormStyle = $('.js-select, .js-input');
$jsFormStyle.styler();

// full page
var $fullPage = $('#js-fullpage');
$fullPage.fullpage({
	licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
	sectionSelector: '.b-screen',
	navigation: false,
	scrollingSpeed: 800,
	scrollOverflow: true,
	onLeave: function onLeave(origin, destination, direction) {
		var $leavingSection = $('.js-fix');
		if (destination.index == 0 && direction == 'up') {
			$leavingSection.removeClass('fix');
		} else if (origin.index !== 1 && direction == 'down') {
			$leavingSection.addClass('fix');
		}
	}
});

// slider
var $jsSlickFor = $('.js-slider-for');
var $jsSlickNav = $('.js-slider-nav');
$jsSlickFor.slick({
	dots: false,
	arrows: false,
	infinite: true,
	speed: 500,
	slidesToShow: 1,
	slidesToScroll: 1,
	rows: 1,
	asNavFor: $jsSlickNav,
	fade: true
});
$jsSlickNav.slick({
	dots: false,
	arrows: false,
	infinite: true,
	speed: 500,
	slidesToShow: 4,
	slidesToScroll: 1,
	asNavFor: $jsSlickFor,
	focusOnSelect: true,
	vertical: true
});
var $jsSlickProd = $('.js-cases-slider');
$jsSlickProd.slick({
	slidesToShow: 2,
	autoplay: true,
	autoplaySpeed: 3000,
	arrows: false,
	dots: true,
	responsive: [{
		breakpoint: 767,
		settings: {
			slidesToShow: 1,
			fade: true
		}
	}]
});

//mask
var $jsMaskPhone = $('.js-mask-phone');
$jsMaskPhone.inputmask('+380(99)999-99-99');

// ADMIN PAGES
// btn menu
var $btnAMenu = $('.js-a-btn-menu');
$btnAMenu.click(function() {
	$(this).toggleClass('is-open');
	$('body').toggleClass('close-menu');
});
// tab
var $jsTab = $('.js-tab');
$jsTab.tabs();
