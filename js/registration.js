$(document).ready(function () {

    $("body").on("click", "#register", function (e) {
        e.preventDefault();
        form_data = $('#registration').serialize();

        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'registration',
            form_data: form_data

        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors',
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }

            if (result.status == "success") {
                $.sweetModal({
                    timeout: 2000,
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS,
                    onClose: function () {
                        location.href = '/welcome';
                    }
                });
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });
    });
    $("body").on("click", ".register_btn", function (e) {
        e.preventDefault();
        form_data = $('.b-registr__entry-form2').serialize();

        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'registration',
            form_data: form_data

        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors',
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }

            if (result.status == "success") {
                $.sweetModal({
                    timeout: 2000,
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS,
                    onClose: function () {
                        location.href = '/welcome';
                    }
                });
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });
    });

    $("body").on("click", ".question-send", function (e) {
        e.preventDefault();
        form_data = $('.question-form').serialize();

        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'questions',
            form_data: form_data

        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors' + r,
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }

            if (result.status == "success") {
                $.sweetModal({
                    timeout: 2000,
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS
                });
                $(".b-input").each(function (index) {
                    $(this).val("");
                });
                $(".b-textarea").val("");
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });
    });
    $("body").on("click", ".b-newsl-form__btn", function (e) {
        e.preventDefault();
        email = $('.b-newsl-form__input').val();

        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'emailsub',
            email: email

        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors' + r,
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }

            if (result.status == "success") {
                $.sweetModal({
                    timeout: 2000,
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS
                });
                $(".b-newsl-form__input").val("");
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });
    });
    $("body").on("click", "#remind_password_save", function (e) {
        e.preventDefault();
        form_data = $('#remind_password_form').serialize();

        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'remind_password_save',
            form_data: form_data

        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors',
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }

            if (result.status == "success") {
                $.sweetModal({
                    timeout: 2000,
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS,
                    onClose: function () {
                        location.href = '/profile_edit';
                    }
                });
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });
    });


});
