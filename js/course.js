$(function () {
    group_id = $('input#group_id').attr('value');
    stage = $('input#stage').val();
    changed =
    sessionStorage.setItem('course_changed_prevent_alert', 'false');
    err = window.sessionStorage.getItem('err');
    if(err != null) {
        err = JSON.parse(err);
        //console.log(err);
        if(err[stage] != null && Array.isArray(err[stage])) {
            for (var y = 0; y < err[stage].length; y++) {
                el = err[stage][y];
                //console.log(el);
                arr = el['err_i'].split(',');
                for (var i = 0; i < arr.length; i++) {
                    parent = $("#" + el['err_section'] + "_" + arr[i]);
                    if (parent.length === 0) continue;
                    //console.log('found');
                    $("#" + el['err_section'], parent).addClass('input-error');
                }
            }
        }
    }


    $("form").submit(function(e) {
        e.preventDefault();
    });

    $('#main_sections').on('click', 'a', function () {
        sessionStorage.setItem('course_changed_prevent_alert', 'true');
    });

    $('input').change(function() {
        window.sessionStorage.setItem('course_changed', 'true');
        console.log('changed!');
    });

    $(window).bind('beforeunload', function() {
        course_changed = window.sessionStorage.getItem('course_changed');
        if(course_changed === 'true' && sessionStorage.getItem('course_changed_prevent_alert') === 'false'){
            return "У вас есть не сохранённые изминения, продолжить?";
        }
    });
});