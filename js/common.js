function onTelegramAuth(user) {
    console.log(user);
    $.post("/_ajax.php", {
        token: $('#f_token').attr("content"),
        file: 'auth_telegram_admin',
        first_name: user.first_name,
        user_id: user.id,
    }, function (r) {
        try {
            result = jQuery.parseJSON(r);
        } catch (err) {
            $.sweetModal({
                content: 'Unexpected errors' + r,
                icon: $.sweetModal.ICON_ERROR
            });
            return false;
        }
        if (result.status == "success") {
            $.sweetModal({
                timeout: 1500,
                content: result.mes,
                icon: $.sweetModal.ICON_SUCCESS,
                onClose: function () {
                    location.reload();
                }
            });
        } else {
            $.sweetModal({
                content: result.mes,
                icon: $.sweetModal.ICON_ERROR
            });
        }
    });
}

function save_data_to_session(prevent_to_next) {
    $.post("/_ajax.php", {
        token: $('#f_token').attr("content"),
        file: 'course_save_session',
        data: data,
        stage: stage,
        group_id: group_id,
    }, function (r) {
        try {
            result = jQuery.parseJSON(r);
        } catch (err) {
            $.sweetModal({
                content: 'Unexpected errors: ' + r,
                icon: $.sweetModal.ICON_ERROR
            });
            return false;
        }
        if (result.status == "success") {
            $.sweetModal({
                timeout: 1500,
                content: result.mes,
                icon: $.sweetModal.ICON_SUCCESS,
                onClose: function () {
                    window.sessionStorage.setItem('course_changed_prevent_alert', 'true');

                    if(result.data.next !== undefined){
                        window.sessionStorage.removeItem('err');

                        if(!prevent_to_next) {
                            window.location.href = '/course_edit?stage=' + result.data.next + '&group_id=' + result.data.group_id;
                            return;
                        }
                    }

                    window.location.reload();
                }
            });
        } else {
            $.sweetModal({
                content: result.mes,
                icon: $.sweetModal.ICON_ERROR,
                onClose: function () {
                }
            });
        }
    });
}

$(document).ready(function () {
    $("header").on("click", "#a--head-search-btn", function (e) {
        e.preventDefault();
        var press = jQuery.Event("keypress");
        press.ctrlKey = false;
        press.which = 13;
        $("header").find('#a--head-search').trigger(press);
    });

    $("header").on("keypress", "#a--head-search", function (e) {
        if(e.which == 13) {
            find = $("#a--head-search").val();
            window.open('/find_course?find=' + find + '&cost_filter=2');
        }
    });

    $('.js-resize-text').each(function(){
        var el= $(this);
        var textLength = el.html().length;
        if (textLength > 50){
            el.css('font-size', '0.8em');
            //el.css('padding-right', '90px');
        } else if (textLength > 40) {
            el.css('font-size', '1.0em');
            //el.css('padding-right', '100px');
        } else if (textLength > 30) {
            el.css('font-size', '1.3em');
            //el.css('padding-right', '120px');
        } else if (textLength > 20) {
            el.css('font-size', '1.5em');
            //el.css('padding-right', '140px');
        }
    });

    $("body").on("click", ".unauth", function (e) {
        data = $("#group").serialize();
        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'unauth_telegram_admin',
        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors: ' + r,
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }
            if (result.status == "success") {
                $.sweetModal({
                    timeout: 1500,
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS,
                    onClose: function () {
                        window.location.reload();
                    }
                });
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });

    });
    $("body").on("click", ".btn-switch-mode", function () {
        mode = $(this).attr('data-target');
        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'user_mode',
            user_mode: mode
        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors: ' + r,
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }
            if (result.status == "success") {
                $.sweetModal({
                    timeout: 1500,
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS,
                    onClose: function () {
                        window.location.href = '/groups';
                    }
                });
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });

    });
    $("body").on("click", ".curse-info-save", function (e) {
        data = $('form').serialize();
        stage = $('input#stage').val();
        group_id = $('input#group_id').val();
        console.log(data);

        if($('form').length !== 0) {
            $.post("/_ajax.php", {
                token: $('#f_token').attr("content"),
                file: 'course_save_session',
                data: data,
                stage: stage,
                group_id: group_id,
            }, function (r) {
                try {
                    result = jQuery.parseJSON(r);
                } catch (err) {
                    $.sweetModal({
                        content: 'Unexpected errors: ' + r,
                        icon: $.sweetModal.ICON_ERROR
                    });
                    return false;
                }
                if (result.status == "success") {
                    save_to_database();
                } else {
                    $.sweetModal({
                        content: result.mes,
                        icon: $.sweetModal.ICON_ERROR,
                        onClose: function () {
                        }
                    });
                }
            });
        } else {
            save_to_database();
        }

        function save_to_database() {
            $.post("/_ajax.php", {
                token: $('#f_token').attr("content"),
                file: 'course_save',
                stage: stage,
                id: group_id,
            }, function (r) {
                try {
                    result = jQuery.parseJSON(r);
                } catch (err) {
                    $.sweetModal({
                        content: 'Unexpected errors: ' + r,
                        icon: $.sweetModal.ICON_ERROR
                    });
                    return false;
                }
                if (result.status == "success") {
                    $.sweetModal({
                        timeout: 1500,
                        content: result.mes,
                        icon: $.sweetModal.ICON_SUCCESS,
                        onClose: function () {
                            window.sessionStorage.setItem('course_changed', 'false');
                            window.sessionStorage.removeItem('err');
                            window.location.href = '/groups';
                        }
                    });
                } else {
                    $.sweetModal({
                        content: result.mes,
                        icon: $.sweetModal.ICON_ERROR,
                        onClose: function () {
                            if (result.data != null) {
                                sessionStorage.setItem('course_changed_prevent_alert', 'true');
                                window.sessionStorage.setItem('err', JSON.stringify(result.data));
                                window.location.href = '/course_edit?stage='+result.data.err_stage+'&group_id='+group_id;
                            }
                        }
                    });
                }
            });
        }
    });
    $("body").on("click", ".curse-info-save-session", function (e) {
        data = $('form').serialize();
        stage = $('input#stage').val();
        group_id = $('input#group_id').val();
        save_data_to_session(false);
    });

    $("body").on("click", ".add_group", function (e) {
        data = $("#group").serialize();
        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'add_group',
            data: data
        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors: ' + r,
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }
            if (result.status == "success") {
                $.sweetModal({
                    timeout: 1500,
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS,
                    onClose: function () {
                        window.location.reload();
                    }
                });
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });

    });
    $("body").on("click", ".edit_group", function (e) {
        data = $("#group").serialize();
        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'edit_group',
            data: data
        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors: ' + r,
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }
            if (result.status == "success") {
                $.sweetModal({
                    timeout: 1500,
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS,
                    onClose: function () {
                        window.location.reload();
                    }
                });
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });
    });
    $("body").on("click", ".del_group", function (e) {
        id = $(this).attr("data-id");
        sub = $(this).attr("data-sub");
        $.sweetModal.defaultSettings.confirm.yes.label = 'Удалить';
        $.sweetModal.defaultSettings.confirm.cancel.label = 'Отмена';
        $.sweetModal.confirm('Подтвердите удаление', function () {
            $.post("/_ajax.php", {
                token: $('#f_token').attr("content"),
                file: 'delete_group',
                id: id,
                sub: sub
            }, function (r) {
                try {
                    result = jQuery.parseJSON(r);
                } catch (err) {
                    $.sweetModal({
                        content: 'Unexpected errors: ' + r,
                        icon: $.sweetModal.ICON_ERROR
                    });
                    return false;
                }
                if (result.status == "success") {
                    $.sweetModal({
                        timeout: 2500,
                        content: result.mes,
                        icon: $.sweetModal.ICON_SUCCESS,
                        onClose: function () {
                            window.location.reload();
                        }
                    });
                } else {
                    $.sweetModal({
                        content: result.mes,
                        icon: $.sweetModal.ICON_ERROR
                    });
                }
            });
        });
    });
    $("body").on("click", ".copy_group", function (e) {
        id = $(this).attr("data-id");
        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'copy_group',
            id: id
        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors: ' + r,
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }
            if (result.status == "success") {
                $.sweetModal({
                    timeout: 2500,
                    content: "Урок успешно скопирован. Сейчас вы будете направлены на страницу редактирования",
                    icon: $.sweetModal.ICON_SUCCESS,
                    onClose: function () {
                        var url = "/edit_group?id=" + result.mes;
                        window.open(url, '_self');
                    }
                });
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });
    });
    $("body").on("click", ".share_group", function (e) {
        id = $(this).attr("data-id");
        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'share_group',
            id: id
        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors: ' + r,
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }
            if (result.status == "success") {
                var $tmp = $("<input>");
                var link = "https://inspiratura.com/share_group?id=" + id;
                $("body").append($tmp);
                $tmp.val(link).select();
                document.execCommand("copy");
                $tmp.remove();
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS
                });
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });
    });
    $("body").on("click", ".sub_group", function (e) {
        id = $(this).attr("data-id");
        uid = $(this).attr("data-uid");
        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'subscribe_group',
            id: id,
            uid: uid
        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors: ' + r,
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }
            if (result.status == "success") {
                $.sweetModal({
                    timeout: 2500,
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS,
                    onClose: function () {
                        window.location.reload();
                    }
                });
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });
    });

    $("body").on("click", "#save_profile", function (e) {
        e.preventDefault();
        form_data = $('#profile_edit').serialize();

        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'profile_edit',
            form_data: form_data

        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors',
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }

            if (result.status == "success") {
                $.sweetModal({
                    timeout: 2000,
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS,
                    onClose: function () {
                        location.reload();
                    }
                });
            } else if (result.status == "error_nomodal") {
                $(".a-form-row__valid").html('');
                $(result.data.field).html('<div class="a-form-valid a-form-valid--error">Недопустимый формат</div>');
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });
    });

    $("body").on("click", "#save_profile_pass", function (e) {
        e.preventDefault();
        form_data = $('#profile_edit_pass').serialize();

        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'profile_edit_pass',
            form_data: form_data

        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors',
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }

            if (result.status == "success") {
                $.sweetModal({
                    timeout: 2000,
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS,
                    onClose: function () {
                        location.reload();
                    }
                });
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });
    });
});