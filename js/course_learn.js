$(document).ready(function () {
    $('.slide').on('click', function () {
        glyphicon = $(this).parent().find('.glyphicon');
        block = $(this).parent().find('.block_content');
        if (block.first().is(":hidden")) {
            block.slideDown();
            glyphicon.removeClass('glyphicon-chevron-down');
            glyphicon.addClass('glyphicon-chevron-up');
        } else {
            block.slideUp();
            glyphicon.removeClass('glyphicon-chevron-up');
            glyphicon.addClass('glyphicon-chevron-down');
        }
    });

    prevChild = null;
    $('.child-container').on('click', function () {
        child = $(this).children('div:first-child');
        if (child.hasClass('a-selected')) return;

        id = $('#id').val();
        container = $(this);
        block_id = container.find('#block_id').val();
        section_id = container.closest('.a-channel__item').find('#section_id').val();
        spinner = `<div class="center-abs"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" style="top: 2px;"></span> Загружаем...</div>`;
        error = `<div class="center-abs"><span class="glyphicon glyphicon glyphicon-remove" style="top: 2px;padding-right: 5px;color: #e84752;"></span>Не удалось загрузить :(</div>`;

        $('.content').empty();
        $('.content').append(spinner);

        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'course_get_block_attach',
            id: id,
            section_id: section_id,
            block_id: block_id,
            cache: true
        }, function (r) {
            $('.content').empty();

            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors: ' + r,
                    icon: $.sweetModal.ICON_ERROR
                });

                $('.content').append(error);
                return false;
            }
            if (result.status == "success") {

                child.removeClass('row__block_hover');
                child.addClass('a-selected');
                if (prevChild !== null) {
                    prevChild.removeClass('a-selected');
                    prevChild.addClass('row__block_hover');
                }
                prevChild = child;

                $('.content').append(result.data.attach);
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });

                $('.content').append(error);
            }
        });
    });

    $(".child-container .js-input").click(function(e) {
        e.stopPropagation();
    });

    last_el = $('.child-container .last_view:first');
    if(last_el.length === 0) {
        last_el = $('.child-container:first');
    }
    last_el.trigger('click');
    last_el.closest('.a-channel__item').find('.slide:first').trigger('click');
    /*$.when($('.child-container').each(function () {
        is_check = $(this).find('input:first').prop("checked");
        if(is_check == true){
            last_el = $(this);
        }
    })).done(function () {
        last_el.trigger('click');
        last_el.closest('.a-channel__item').find('.slide:first').trigger('click');
    });*/

    $('input[type="checkbox"]').click(function(){
        id = $('#id').val();
        container = $(this).closest('.child-container');
        block_id = container.find('#block_id').val();
        section_id = container.closest('.a-channel__item').find('#section_id').val();
        is_check = $(this).prop("checked") == false ? 1 : 0;
        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            file: 'course_block_progress_change',
            id: id,
            section_id: section_id,
            block_id: block_id,
            done:is_check,
            cache: true
        }, function (r) {
            try {
                result = jQuery.parseJSON(r);
            } catch (err) {
                $.sweetModal({
                    content: 'Unexpected errors: ' + r,
                    icon: $.sweetModal.ICON_ERROR
                });
                return false;
            }
            if (result.status == "success") {
                parent = container.closest('.a-channel__item');
                count = parent.find('#count');

                count.empty();
                count.append(result.data.current);
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        });
    });
});