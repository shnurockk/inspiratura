<?php 

namespace Fianta\Core;

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));
/*
 * FIANTA FRAMEWORK BY FEDOR YASTREBOV
 * Запрещено использование данного программного кода или его части в любых целях без согласия автора
 * COPYRIGHT © 2013-2017 
 * http://fianta.com 
 * EMAIL: adm@fianta.com
*/
class Fianta {

    //Вывод ошибок
    static function err($fname, $pos) {
        return "CMS FIANTA ".FIANTA_VER." ERROR CODE: ".basename(strtoupper($fname))."_".$pos;
    }

    //Логирование ошибок
    static function log($filename, $data) {
        if (!file_exists(F_PATH_CORE."logs")) {
            mkdir(F_PATH_CORE."logs", 0755);
        }
        file_put_contents(F_PATH_CORE."logs/".$filename."_".date("Y_m_d_H_i_s").".log", $data);
    }
    static function debug($val) {        if (is_array($val) or is_object($val)) {            echo '<pre>';                 print_r($val);             echo '<pre>';             exit();        } else {            echo $val;             die();	        }    }		
}
