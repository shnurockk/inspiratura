<?php 

namespace Fianta\Core;

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

use DateTime;
use DateTimeZone;
/*
 * FIANTA FRAMEWORK BY FEDOR YASTREBOV
 * Запрещено использование данного программного кода или его части в любых целях без согласия автора
 * COPYRIGHT © 2013-2017 
 * http://fianta.com 
 * EMAIL: adm@fianta.com
*/

class Converter {

    static function fromUnixDate($date, $time = false, $second = false) 
    {
        if ($date == 0) {
            return "";
        }
        if ($time) {
            if ($second) {
                $d = date("d.m.Y H:i:s", $date);
            } else {
                $d = date("d.m.Y H:i", $date);
            }
        } else {
            $d = date("d.m.Y", $date);
        }
        return $d;
    }
	 
    static function toUnixDate($date) 
    {
        $d = strtotime($date);
        return $d;
    }
    
    static function GmtTimeToLocalTime($time, $format = "d.m.Y H:i") 
    {
        date_default_timezone_set('UTC');
        $new_date = new DateTime($time);
        $new_date->setTimeZone(new DateTimeZone('Europe/Kiev'));
        return $new_date->format($format);
    }
    
    static function mb_ucfirst($str) {
        $fc = mb_strtoupper(mb_substr($str, 0, 1));
        return $fc.mb_substr($str, 1);
    }
    	static function btc_rename($val) {        if (strpos($val,'BTC') != false) {			$name = str_replace('BTC','',$val);						$name = 'BTC-'.$name;						return $name; 			        } else {						return $val;                  }    }		
}
