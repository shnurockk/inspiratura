<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));
$action = filter_input(INPUT_POST, 'action');
if (!empty($action)) {
    switch ($action) {
        case "clear_notify":
            unset($_SESSION["MOD_RESULT"]);
            break;
    }
}