<?php defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));
/*************************************************************************************************
FIANTA FRAMEWORK BY FEDOR YASTREBOV
Запрещено использование данного программного кода или его части в любых целях без согласия автора. 
COPYRIGHT © 2013-2017 WEB: http://fianta.com EMAIL: adm@fianta.com
**************************************************************************************************/

use Fianta\Sys\Providers;
use Fianta\Core\Fianta;
use Fianta\Core\Select;

//Таблица
if (isset($F_URL[2])) {
    header('Location: /'.F_ADMIN_ALIAS.'/'.$F_MOD.F_URL_SUFIX);
}

$text = "";
$text_bot = "";
$filename = F_PATH_SYS."alert.txt";
$filename_bot = F_PATH_SYS."alert_bot.txt";
if (file_exists($filename)) {
    $text = file_get_contents($filename);
}
if (file_exists($filename_bot)) {
    $text_bot = file_get_contents($filename_bot);
} 

$mod_head_tpl = F_PATH_CORE.'cms/mod/'.$F_MOD.'/tpl/view/head.tpl.php';
$mod_tpl = F_PATH_CORE.'cms/mod/'.$F_MOD.'/tpl/view/index.tpl.php';
