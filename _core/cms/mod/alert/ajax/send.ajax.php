<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));
/* * ***********************************************************************************************
  FIANTA FRAMEWORK BY FEDOR YASTREBOV
  Запрещено использование данного программного кода или его части в любых целях без согласия автора.
  COPYRIGHT © 2013-2017 WEB: http://fianta.com EMAIL: adm@fianta.com
 * ************************************************************************************************ */

use Fianta\Core\AjaxResponse;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Fianta\Core\User;

$data = [];
$sdata = filter_input(INPUT_POST, 'data');
parse_str($sdata, $data);

//Проверка прав
if (User::get()->role_id != 1) {
    $result = new AjaxResponse("error", "Недостаточно прав для проведения операции");
    exit($result->json());
}
$telegram = new Api('711315757:AAG_C5oGyN7kuK1PZvw4k3kPu2jUASK00Ro');
try {
    $telegram->sendMessage(['chat_id' => "@crptinfo", 'text' => $data["text"], 'parse_mode' => 'Markdown']);
} catch (TelegramResponseException $e) {
    $errorData = $e->getResponseData();
    $telegram->sendMessage(['chat_id' => 255752568, 'text' => $errorData["description"], 'parse_mode' => 'Markdown']);
    $result = new AjaxResponse("error", "Ошибка отправки: ".$errorData["description"]);
    exit($result->json());
}

$result = new AjaxResponse("success", "Успешно отправлено");
exit($result->json());
