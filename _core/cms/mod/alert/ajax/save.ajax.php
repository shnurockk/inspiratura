<?php

defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));
/* * ***********************************************************************************************
  FIANTA FRAMEWORK BY FEDOR YASTREBOV
  Запрещено использование данного программного кода или его части в любых целях без согласия автора.
  COPYRIGHT © 2013-2017 WEB: http://fianta.com EMAIL: adm@fianta.com
 * ************************************************************************************************ */

use Fianta\Core\AjaxResponse;
use Fianta\Core\User;

$data = [];
$data_bot = [];
$sdata = filter_input(INPUT_POST, 'data');
$sdata_bot = filter_input(INPUT_POST, 'data_bot');
parse_str($sdata, $data);
parse_str($sdata_bot, $data_bot);

//Проверка прав
if (User::get()->role_id != 1) {
    $result = new AjaxResponse("error", "Недостаточно прав для проведения операции");
    exit($result->json());
}

$filename = F_PATH_SYS."alert.txt";
file_put_contents($filename, $data);
$filename_bot = F_PATH_SYS."alert_bot.txt";
file_put_contents($filename_bot, $data_bot);

$result = new AjaxResponse("success", "Успешно обновлено");
exit($result->json());
