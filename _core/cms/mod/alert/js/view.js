$(function() {

    $(".i_func").on("click", ".mod_edit_apply", function(e) {
        e.preventDefault();
        
        btns = $('.i_func').html();
        $('.i_func').html('<div id="mod_action_process"></div>');
        form_data = $('#mod_edit_form').serialize();
        form_data_bot = $('#mod_edit_form_bot').serialize();
        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            mod: $('#f_mod').attr("content"), 
            mod_file: "save",
            data: form_data,
            data_bot: form_data_bot
        }, function(r) {
            try {
                result = jQuery.parseJSON(r);
            } catch(err){
                $.sweetModal({
                    content: err,
                    icon: $.sweetModal.ICON_ERROR,
                    onClose: function() {
                        $('.i_func').html(btns);
                    }
                });
            }
            if (result.status == "success") {     
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS,
                    onClose: function() {
                        if (result.data != "") {
                            window.location.href = result.data;
                        } else {
                            $('.i_func').html(btns);
                        }
                    }
                });
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_WARNING,
                    onClose: function() {
                        $('.i_func').html(btns);
                    }
                });
            }
        });
    });
    
        $(".i_func").on("click", ".mod_send", function(e) {
        e.preventDefault();
        
        btns = $('.i_func').html();
        $('.i_func').html('<div id="mod_action_process"></div>');
        form_data = $('#mod_send_form').serialize();
        $.post("/_ajax.php", {
            token: $('#f_token').attr("content"),
            mod: $('#f_mod').attr("content"), 
            mod_file: "send",
            data: form_data
        }, function(r) {
            try {
                result = jQuery.parseJSON(r);
            } catch(err){
                $.sweetModal({
                    content: err,
                    icon: $.sweetModal.ICON_ERROR,
                    onClose: function() {
                        $('.i_func').html(btns);
                    }
                });
            }
            if (result.status == "success") {
                $("#send_text").val("");
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_SUCCESS,
                    onClose: function() {
                        if (result.data != "") {
                            window.location.href = result.data;
                        } else {
                            $('.i_func').html(btns);
                        }
                    }
                });
            } else {
                $.sweetModal({
                    content: result.mes,
                    icon: $.sweetModal.ICON_WARNING,
                    onClose: function() {
                        $('.i_func').html(btns);
                    }
                });
            }
        });
    });
    
});
