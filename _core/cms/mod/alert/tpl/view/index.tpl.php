<?
defined('FIANTA_ACC') or die(include_once(F_PATH_SYS.'pages/404.php'));

/* * ***********************************************************************************************
  FIANTA FRAMEWORK BY FEDOR YASTREBOV
  Запрещено использование данного программного кода или его части в любых целях без согласия автора.
  COPYRIGHT © 2013-2017 WEB: http://fianta.com EMAIL: adm@fianta.com
 * ************************************************************************************************ */
?>

<div class="mod_func">
    <div class="i_func">
        <a href="#js" class="mod_send">Отправить в телеграм</a>
        <a href="#js" class="mod_edit_apply">Сохранить</a>
    </div>
</div>
<div class="mod_edit">
    <form id="mod_edit_form" name="mod_edit_form" method="post" enctype="multipart/form-data">	
        <div class="mod_fields_thin">
            <div class="mod_capt">Сообщение для всех пользователей(Alert Верх)</div>
            <textarea style="padding:10px;width:100%;color:red" name="text" id="text"><?= $text ?></textarea>
        </div>
    </form>
    <form id="mod_edit_form_bot" name="mod_edit_form_bot" method="post" enctype="multipart/form-data">	
        <div class="mod_fields_thin">
            <div class="mod_capt">Сообщение для всех пользователей(Alert Низ)</div>
            <textarea style="padding:10px;width:100%;color:red" name="text" id="text"><?= $text_bot ?></textarea>
        </div>
    </form>
    <form id="mod_send_form" name="mod_send_form" method="post" enctype="multipart/form-data">	
        <div class="mod_fields_thin">
            <div class="mod_capt">Отправка уведомления в Телеграм(CRPTINFO)</div>
            <textarea style="padding:10px;width:100%;color:red" name="text" id="send_text"></textarea>
        </div>
    </form>
</div>
