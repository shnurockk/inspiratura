<?php

/* * ***********************************************************************************************
  FIANTA FRAMEWORK BY FEDOR YASTREBOV
  Запрещено использование данного программного кода или его части в любых целях без согласия автора.
  COPYRIGHT © 2013-2017 WEB: http://fianta.com EMAIL: adm@fianta.com
 * ************************************************************************************************ */

//FIANTRY ENTRY
//Редирект с www на без www
if ($_SERVER['HTTP_HOST'] == F_WWW) {
    header("Location: ".F_SSL_URL.$_SERVER["REQUEST_URI"], TRUE, 301);
    exit();
}
header('Content-Type: text/html; charset=utf-8');

//Инициализация ядра
require_once('init.php');

//Ajax токен 
use Fianta\Core\Sys;

Sys::token();

//Контроллер запросов
$url = filter_input(INPUT_GET, 'a');
if (!empty($url)) {
    $F_URL = explode("/", $url);
}

if (strpos($_SERVER['REQUEST_URI'], "/_fianta.php") === 0) {
    include_once(F_PATH_WWW."_404.php");
    return;
}

if (empty($url)) {
    include_once(F_PATH_SYS."pages/index.php");
} else {
    if (count($F_URL) == 1 or $F_URL[0] == "admin") {
        $user_mode = isset($_SESSION['user_mode']) ? $_SESSION['user_mode'] : 0;
        (file_exists(F_PATH_SYS."pages/".$F_URL[0].".php")) ? include_once(F_PATH_SYS."pages/".$F_URL[0].".php") : include_once(F_PATH_WWW."_404.php");
        if($F_URL[0] != "/profile_edit" && F_LOGGED){
            include_once(F_PATH_SYS."tpl/inc/prevent_access.tpl.php");
        }
    }
    elseif ($F_URL[0] == "upload") {
        header('Content-type: application/pdf');
        readfile(F_PATH_WWW.$F_URL[0]."/".$F_URL[1]);
    } elseif ($F_URL[0] != "admin") {
        (file_exists(F_PATH_SYS.$F_URL[0]."/".$F_URL[1].".php")) ? include_once(F_PATH_SYS.$F_URL[0]."/".$F_URL[1].".php") : include_once(F_PATH_WWW."_404.php");
    }
}